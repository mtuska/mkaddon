// RAWR!

function table.GetDifference(newtbl, oldtbl)
	local nilType = "_nil"
	local diffTable = {}
	for k,v in pairs(newtbl) do
		if (type(oldtbl[k]) == "function") then
			continue
		elseif (type(oldtbl[k])=="table"&&type(v)=="table") then
			local diffTable2 = table.GetDifference(newtbl[k], oldtbl[k])
			if (table.Count(diffTable2) <= 0) then
				continue
			end
			diffTable[k] = diffTable2
			continue -- difference in table
		elseif (oldtbl[k]==v) then
			continue -- no change
		end
		diffTable[k] = newtbl[k]
	end
	for k,v in pairs(oldtbl) do
		if (type(newtbl[k]) == "function") then
			continue
		elseif (type(newtbl[k])=="table"&&type(v)=="table") then
			local diffTable2 = table.GetDifference(newtbl[k], oldtbl[k])
			if (table.Count(diffTable2) <= 0) then
				continue
			end
			diffTable[k] = diffTable2
			continue -- difference in table
		elseif (newtbl[k]==v) then
			continue -- no change
		elseif (newtbl[k]==nil) then
			diffTable[k] = nilType
			continue -- new value is nil
		end
	end
	return diffTable
end

function table.RandomSeq(tbl)
	local rand = math.random(1, #tbl)
	return tbl[rand], rand
end

function table.pack(...)
	return {...}
end

function table.unpack(...)
	return unpack(...)
end

function util.WeaponFromDamage(dmg)
	local inf = dmg:GetInflictor()
	local wep = nil
	if IsValid(inf) then
		if inf:IsWeapon() or inf.Projectile then
			wep = inf
		elseif dmg:IsDamageType(DMG_DIRECT) or dmg:IsDamageType(DMG_CRUSH) then
			-- DMG_DIRECT is the player burning, no weapon involved
			-- DMG_CRUSH is physics or falling on someone
			wep = nil
		elseif inf:IsPlayer() then
			wep = inf:GetActiveWeapon()
			if not IsValid(wep) then
				-- this may have been a dying shot, in which case we need a
				-- workaround to find the weapon because it was dropped on death
				wep = IsValid(inf.dying_wep) and inf.dying_wep or nil
			end
		end
	end

	return wep
end

MK.util = MK.util or {}

-- http://rosettacode.org/wiki/Averages/Median#Lua
function MK.util.Median(numlist)
	if type(numlist) ~= 'table' then return numlist end
	table.sort(numlist)
	if #numlist %2 == 0 then return (numlist[#numlist/2] + numlist[#numlist/2+1]) / 2 end
	return numlist[math.ceil(#numlist/2)]
end

function MK.util.IsMonth(month)
	return tonumber(os.date('%m')) == month
end

function MK.util.GenPassword()
	local str = ""
	while string.len(str) < 30 do
		local int = math.random(97,122)
		local stradd = string.char(int)
		str = str .. stradd
	end
	str = "~_~"..str

	return str
end

function MK.util.GMIncludeDir(directory, state, gm)
	local gm = gm or GAMEMODE.FolderName
	MK.util.IncludeDir(gm.."/gamemode/"..directory, state)
end

function MK.util.IncludeGMDir(directory, state)
	MK.util.IncludeInternalDir(MK.folderName.."/gamemodes/"..GAMEMODE.FolderName.."/"..directory)
end

function MK.util.IncludeMapDir(directory, state)
	MK.util.IncludeInternalDir(MK.folderName.."/gamemodes/"..game.GetMap().."/"..directory)
end

function MK.util.IncludeInternalDir(directory, state, precallback, postcallback)
	MK.util.IncludeDir(MK.folderName.."/"..directory, state, precallback, postcallback)
end

function MK.util.IncludeDir(directory, state, precallback, postcallback)
	local directory2 = directory.."/*.lua"
	local files = file.Find(directory2, "LUA")
	table.sort(files, function(a, b) // shared files take priority in loading, they generally create the base arrays
		if (string.find(a, "sh_") && !string.find(b, "sh_")) then
			return true
		elseif (!string.find(a, "sh_") && string.find(b, "sh_")) then
			return false
		end
		return a:upper() < b:upper()
	end)
	for k, v in pairs(files) do
		MK.util.Include(directory.."/"..v, state, precallback, postcallback)
	end
	hook.Run("MK.IncludeDirectory", directory)
end

function MK.util.IncludeInternal(fileName, state, precallback, postcallback)
	MK.util.Include(MK.folderName.."/"..fileName, state, precallback, postcallback)
end

function MK.util.Include(filepath, state, precallback, postcallback)
	local args = string.Explode("/", filepath)
	local fileName = filepath
	if (table.Count(args) != 0) then
		fileName = args[table.Count(args)]
		fileName = string.Replace(fileName, ".lua", "")
		fileName = string.Replace(fileName, "sv_", "")
		fileName = string.Replace(fileName, "sh_", "")
		fileName = string.Replace(fileName, "cl_", "")
	end
	if (precallback&&isfunction(precallback)) then
		precallback(fileName, filepath)
	end

	if (state == "shared" or string.find(filepath, "sh_")) then
		AddCSLuaFile(filepath)
		include(filepath)
	elseif ((state == "server" or string.find(filepath, "sv_")) and SERVER) then
		include(filepath)
	elseif (state == "client" or string.find(filepath, "cl_")) then
		if (SERVER) then
			AddCSLuaFile(filepath)
		else
			include(filepath)
		end
	end

	if (postcallback&&isfunction(postcallback)) then
		postcallback(fileName, filepath)
	end
end

function MK.util.IncludeResourceDirectory(dir, gmname)
	local resourceDir = (gmname or GM.FolderName).."/content"
	local files, dirs = file.Find('gamemodes/'..resourceDir.."/"..dir.."/*", "MOD")
 	for k,v in pairs(files) do
		local fileName = dir.."/"..v
		if (!string.find(v, ".bz2") and !string.find(v, ".bat") and v != "info.txt" and !string.find(fileName, "-off")) then
			resource.AddFile(fileName)
		end
 	end
 	for k,v in pairs(dirs) do
 		local fileName = dir.."/"..v
 		if (file.IsDir(resourceDir.."/"..fileName, "LUA")) then
			MK.util.IncludeResourceDirectory(fileName)
		end
	end
end

function MK.util.GetTableDelta(a, b)
	local output = {}

	for k, v in pairs(a) do
		if (type(v) == "table" and type(b[k]) == "table") then
			local output2 = MK.util.GetTableDelta(v, b[k])

			for k2, v2 in pairs(output2) do
				output[k] = output[k] or {}
				output[k][k2] = v2
			end
		elseif (b[k] == nil or b[k] != v) then
			output[k] = v or "__nil"
		end
	end

	for k, v in pairs(b) do
		if (type(v) == "table" and type(a[k]) == "table") then
			local output2 = MK.util.GetTableDelta(a[k], v)

			for k2, v2 in pairs(output2) do
				output[k] = output[k] or {}
				output[k][k2] = v2
			end
		elseif (a[k] == nil) then
			output[k] = "__nil"
		end
	end

	return output
end

function MK.util.IsSimilarTable(a, b)
	return table.Count(MK.util.GetTableDelta(a, b)) == 0
end

function MK.util.ExtractArgs(text)
	local skip = 0
	local arguments = {}
	local curString = ""

	for i = 1, #text do
		if (i <= skip) then continue end

		local c = text:sub(i, i)

		if (c == "\"" or c == "'") then
			local match = text:sub(i):match("%b"..c..c)

			if (match) then
				curString = ""
				skip = i + #match
				arguments[#arguments + 1] = match:sub(2, -2)
			else
				curString = curString..c
			end
		elseif (c == " " and curString != "") then
			arguments[#arguments + 1] = curString
			curString = ""
		else
			if (c == " " and curString == "") then
				continue
			end

			curString = curString..c
		end
	end

	if (curString != "") then
		arguments[#arguments + 1] = curString
	end

	return arguments
end

function MK.util.StringMatches(a, b)
	if (a and b) then
		local a2, b2 = a:lower(), b:lower()

		-- Check if the actual letters match.
		if (a == b) then return true end
		if (a2 == b2) then return true end

		-- Be less strict and search.
		if (a:find(b)) then return true end
		if (a2:find(b2)) then return true end
	end

	return false
end

function MK.util.FindStringInTable(str, tbl, returnValue)
	local matches = {}
	for k,v in ipairs(tbl) do
		if (MK.util.StringMatches(v, str)) then
			if (returnValue) then
				table.insert(matches, v)
			else
				table.insert(matches, k)
			end
		end
	end
	return matches
end

function MK.util.FindPlayer(name)
	local matches = {}
	for k, v in ipairs(player.GetAll()) do
		if (MK.util.StringMatches(v:Name(), name)) then
			table.insert(matches, v)
		end
	end
	return matches
end

local timeData = {
	{"y", 60 * 60 * 24 * 365},
	{"mo", 60 * 60 * 24 * 30},
	{"w", 60 * 60 * 24 * 7},
	{"d", 60 * 60 * 24},
	{"h", 60 * 60},
	{"m", 60},
	{"s", 1}
}

function MK.util.GetTimeByString(data)
	if (!data) then
		return 0
	end

	data = string.lower(data)

	local time = 0

	for i = 1, #timeData do
		local info = timeData[i]

		data = string.gsub(data, "(%d+)"..info[1], function(match)
			local amount = tonumber(match)

			if (amount) then
				time = time + (amount * info[2])
			end

			return ""
		end)
	end

	local seconds = tonumber(string.match(data, "(%d+)")) or 0

	time = time + seconds

	return math.max(time, 0)
end

function MK.util.GetTimeleftByString(secs)
	local str = ""

	local secsInYear = 60 * 60 * 24 * 365
	if secs > secsInYear then
		local years = math.floor( secs / secsInYear )
		secs = secs % secsInYear
		str = string.format( "%s%i year%s ", str, years, (years > 1 and "s" or "") )
	end

	local secsInWeek = 60 * 60 * 24 * 7
	if secs > secsInWeek then
		local weeks = math.floor( secs / secsInWeek )
		secs = secs % secsInWeek
		str = string.format( "%s%i week%s ", str, weeks, (weeks > 1 and "s" or "") )
	end

	local secsInDay = 60 * 60 * 24
	if secs > secsInDay then
		local days = math.floor( secs / secsInDay )
		secs = secs % secsInDay
		str = string.format( "%s%i day%s ", str, days, (days > 1 and "s" or "") )
	end

	local secsInHour = 60 * 60
	if secs > secsInHour then
		local hours = math.floor( secs / secsInHour )
		secs = secs % secsInHour
		str = string.format( "%s%i hour%s ", str, hours, (hours > 1 and "s" or "") )
	end

	local secsInMinute = 60
	if secs > secsInMinute then
		local minutes = math.ceil( secs / secsInMinute )
		secs = secs % secsInMinute
		if minutes >= 60 then minutes = 59 end
		str = string.format( "%s%i minute%s ", str, minutes, (minutes > 1 and "s" or "") )
	end

	if secs > 0 then
		local minutes = math.ceil( secs / secsInMinute )
		if minutes >= 60 then minutes = 59 end
		str = string.format( "%s%i second%s ", str, secs, (secs > 1 and "s" or "") )
	end

	return str:Trim()
end

function MK.util.Console( ply, msg )
	if (CLIENT or (ply and not ply:IsValid())) then
		Msg( msg .. "\n" )
		return
	end

	if (ply) then
		ply:PrintMessage( HUD_PRINTCONSOLE, msg .. "\n" )
	else
		local players = player.GetAll()
		for _, player in ipairs( players ) do
			player:PrintMessage( HUD_PRINTCONSOLE, msg .. "\n" )
		end
	end
end

local function sortProb(a, b)
	return a[1] > b[1]
end
function MK.util.PickWeighted(tbl)
	table.sort(tbl, sortProb)

	local range = 0
	for k,v in RandomPairs(tbl) do
		range = range + v
	end

	local pick = math.Rand(1, range)

	local ct = 0
	for k,v in RandomPairs(tbl) do
		if pick < ct + v then
			return k
		end
		ct = ct + v
	end
end
