// RAWR!

NPC.name = "Cheesenut"
-- This sets the model for the NPC.
NPC.model = "models/player/group01/male_05.mdl"
-- This is for player models that support player colors. The values range from 0-1.
NPC.color = Vector(1, 0, 0)
NPC.team = TEAM_MAYOR

-- Uncomment to make the NPC sit.
--NPC.sequence = "sit"

if (CLIENT) then
    function NPC:onStart()
        if (ply:Team() == self.team) then
            self:addText("Hello Mayor " .. LocalPlayer():Name() .. "! What can I do for you today?")

            self:addOption("I think it's time for me to resign...", function()
                self:addText("Good riddance!")

                self:send("Team")

                self:addLeave("Of course!")
            end)

            self:addOption("Maybe you could take a step into my office, and close the blinds...", function()
                self:addText("What? No, what's wrong with you!")

                self:addLeave("Well, thought I'd try.")
            end)

            self:addLeave("Nevermind.")
            return
        end

        self:addText("Hello, how can I help you?")
        if (VOTING) then
            self:addOption("I'd like to join the election for Mayor", function()
                self:addText("Oh, alright, that'd be "..(GAMEMODE.Config.currency or "$")..tostring(VOTING.CandidateCost))

                self:addOption("Here you go!", function()
                    self:addText("Alright, I've added you to the ballot.")
                    LocalPlayer():ConCommand("mayor_vote_enter")
                    self:addLeave("Thanks")
                end)
                self:addLeave("Oh...")
            end)
        end

        self:addOption("Could you tell me about the surrounding area?", function()
            self:addText("How about you just use your eyes instead?")

            self:addLeave("Thanks for nothing.")
        end)

        self:addLeave("Nevermind.")
    end
else

end