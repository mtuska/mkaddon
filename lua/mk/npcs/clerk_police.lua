// RAWR!

NPC.name = "Cheesenut"
-- This sets the model for the NPC.
NPC.model = "models/player/group01/male_05.mdl"
-- This is for player models that support player colors. The values range from 0-1.
NPC.color = Vector(1, 0, 0)
NPC.team = TEAM_POLICE

-- Uncomment to make the NPC sit.
--NPC.sequence = "sit"

if (CLIENT) then
    function NPC:onStart()
        if (ply:Team() == self.team) then
            self:addText("Hello Patrolman! How could I help you?")

            -- You can have as many options as you need.
            self:addOption("I'd like to go off duty.", function()
                self:addText("Alright, thanks for your work officer!")

                self:send("Team")

                self:addLeave("Of course!")
            end)

            self:addLeave("I'm good.")
            return
        end

        self:addText("Hello there, would you like to be a patrolman?")

        self:addOption("Sure, I'll join", function()
            self:addText("Alright, do good work out there patrolman!")

            self:send("Team")

            self:addLeave("Thanks!")
        end)

        self:addLeave("I'm good.")
    end
else

end