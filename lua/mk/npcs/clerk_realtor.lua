// RAWR!

NPC.name = "Cheesenut"
-- This sets the model for the NPC.
NPC.model = "models/player/group01/male_05.mdl"
-- This is for player models that support player colors. The values range from 0-1.
NPC.color = Vector(1, 0, 0)

-- Uncomment to make the NPC sit.
--NPC.sequence = "sit"

if (CLIENT) then
    function NPC:onStart()
        self:addText("Hello there, how are you today?")
        self:addOption("I'm good.", function()
            self:addText("That's good!")

            self:addLeave("See you later.")
        end)

        self:addOption("I feel cold.", function()
            self:addText("Do you want to feel warmer?")

            self:addOption("Sure!", function()
                self:addLeave("Ouch!")
            end)
            self:addLeave("No thanks.")
        end)
    end
else

end