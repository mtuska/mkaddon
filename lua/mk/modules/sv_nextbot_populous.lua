// RAWR!

MK.populous = MK.populous or {}
MK.populous.list = MK.populous.list or {}
MK.populous.config = {}

MK.populous.config.autoPopulate = false
MK.populous.config.outsideOnly = true
MK.populous.config.spawnInterval = 5
MK.populous.config.spawnDist = 3000
MK.populous.config.spawnPerPopulate = 10
MK.populous.config.despawnMode = 3 // 1 is PVS, 2 is LOS, 3 is distance
MK.populous.config.despawnDist = 2500
MK.populous.config.playerLimit = 20 // Stop spawning populous with this many players

MK.populous.config.civsPerPlayer = 50
MK.populous.config.maxCivs = 200

MK.populous.config.allowArrest = true

MK.populous.config.allowMug = false
MK.populous.config.timeBetweenMugs = 60
MK.populous.config.pickpocketChance = .1 // Chance of pickpocket upon finding a new victim
MK.populous.config.pickpocketWalletPercent = .05 // percent of player's wallet to take
MK.populous.config.pickpocketMin = 50
MK.populous.config.pickpocketMax = 5000

MK.populous.config.defaultNPC = "citizen"
MK.populous.config.npcs = {}
//MK.populous.config.npcs["citizen_druggie"] = 0.1
//MK.populous.config.npcs["police"] = 0.4

function MK.populous.Add(civ)
	table.insert(MK.populous.list, civ)
end

function MK.populous.GetAll()
	for k,bot in pairs(MK.populous.list) do
		if (!IsValid(bot)) then
			MK.populous.list[k] = nil
		end
	end
    return MK.populous.list
end

function MK.populous.RemoveAll()
	for k,bot in pairs(MK.populous.GetAll()) do
		if (IsValid(bot)) then
			bot:Remove()
			bot = nil
		end
	end
	MK.populous.list = {}
end

if (!table.RandomSeq) then
	function table.RandomSeq(tbl)
		local rand = math.random(1, #tbl)
		return tbl[rand], rand
	end
end

function MK.populous.GetLocation()
	local ply = table.RandomSeq(player.GetAll())
	if IsValid(ply) then
		local newpos
		local fail = true

		local i = 1
		while fail and i<300 do
			newpos = VectorRand()
			newpos.z = 0
			newpos = ply:GetPos() + newpos * math.random(MK.populous.config.spawnDist/2, MK.populous.config.spawnDist)
			fail = not util.IsInWorld(newpos)

			if not fail then
				local a = navmesh.GetNearestNavArea(newpos)
				fail = fail or not IsValid(a)
				fail = fail or a:IsUnderwater()
				fail = fail or a:HasAttributes(NAV_MESH_AVOID)

				if not fail then
					newpos = a:GetRandomPoint()+Vector(0,0,3)
					fail = fail or not bit.band( util.PointContents( newpos ), CONTENTS_EMPTY ) == CONTENTS_EMPTY

					if not fail then
						local hull = {}
						-- local hull = {mask=MASK_NPCSOLID}
						local traces = {}
						local offsets = {Vector(-16,-16,-8),Vector(-16,16,-8),Vector(16,-16,-8),Vector(16,16,-8)}
						for i=1, 4 do
							if not fail then
								hull.start = newpos + offsets[i]
								hull.endpos = hull.start+Vector(0,0,16000)
								traces[i] = util.TraceLine(hull)
								fail = fail or traces[i].HitEntity
								if MK.populous.config.outsideOnly then
									fail = fail or not traces[i].HitSky
								end
							end
						end
					end
				end
			end
			i=i+1
		end
		if i>=300 then return false end
		return newpos
	else
		return false
	end
end

function MK.populous.Populate()
	//Delete npcs which nobody can see:
	local deleted = 0
	local count = 0
	local max = math.min(MK.populous.config.civsPerPlayer * #player.GetAll(), MK.populous.config.maxCivs)
	local mode = MK.populous.config.despawnMode or 1
	local despawnDistSqr = MK.populous.config.despawnDist^2
	for k,npc in pairs(MK.populous.GetAll()) do
		if (!IsValid(npc)) then
			npc = nil
			continue
		end
		count = count + 1
		if mode == 1 then
			local nearby = ents.FindInPVS(npc)
			if #nearby == 0 then
				npc:Remove()
				deleted = deleted + 1
				continue
			end
		elseif mode == 2 then
			local vis = false
			for k2,v in pairs(player.GetAll())do
				vis = vis or !npc:IsLineOfSightClear(v)
				-- vis = vis or !util.TraceLine({start=npc:GetPos(),endpos=v:GetPos(),filter={npc,v}}).Hit
			end
			if vis then
				npc:Remove()
				deleted = deleted + 1
				continue
			end
		elseif mode == 3 then
			local vis = false
			for k2,v in pairs(player.GetAll()) do
				if (v:EyePos():DistToSqr(npc:EyePos()) <= despawnDistSqr) then
					vis = true
					break
				end
			end
			if !vis then
				npc:Remove()
				deleted = deleted + 1
				continue
			end
		else
			print("Invalid mode for MK NPCs!")
		end

		if npc:Health() <= 0 then
			npc:Remove()
			deleted = deleted + 1
		end
	end

	local added = 0
	for i=1, math.random(0, max - (count-deleted)) do
		if (added >= MK.populous.config.spawnPerPopulate) then
			break
		end
		local pos = MK.populous.GetLocation()

		if pos then
			local npcid = MK.populous.config.defaultNPC
			local cur = 1
			local ran = math.Rand(0,1)
			for k,v in pairs(MK.populous.config.npcs) do
				if v >= ran and v < cur then
					npcid = k
					cur = v
				end
			end
			added = added + 1
			local npc = MK.npcs.Spawn(npcid, pos, Angle(0, math.random(-180,180), 0))
			if (IsValid(npc)) then
				MK.populous.Add(npc)

				added = added + 1
			end
		end
	end

	print("Added "..added.." and removed "..deleted.." populous from the world. ("..(count-deleted)+added.." exist in total.)")
end

timer.Create("MK_Populous.Autospawn", 30, 0, function()
    local count = #player.GetAll()
    local limit = MK.populous.config.playerLimit or -1

	if (count > 0 && (limit == -1 or limit > count)) then
		if MK.populous.config.autoPopulate then
			//MK.async(function()
				MK.populous.Populate()
			//end)
		end
	else // nobody online/too many online
		for k,v in pairs(MK.populous.GetAll()) do
			if (not v.Persistent) then
				v:Remove()
			end
		end
	end
end)

hook.Add("PlayerInitialSpawn","MK_Populous.PlayerInitialSpawn", function()
	if (#player.GetAll() == 1 && MK.populous.config.autoPopulate) then
		MK.populous.Populate()
	end
end)