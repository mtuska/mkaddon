//This took me quite a while to do so if you can subscribe to my YouTube in return that would be fab, thanks! https://www.youtube.com/channel/UCzA_5QTwZxQarMzwZFBJIAw

-- lightmap stuff
net.Receive( "daynight_lightmaps", function( len )

	render.RedownloadAllLightmaps();

end );

-- precache
hook.Add( "InitPostEntity", "daynightFirstJoinLightmaps", function()

	render.RedownloadAllLightmaps();

	net.Start("daynight_update")
	net.SendToServer()

end );

net.Receive( "daynight_message", function( len )

	local tab = net.ReadTable();

	if ( #tab > 0 ) then

		chat.AddText( unpack( tab ) );

	end

end );

local TIME_DAWN_END = 6.5;
local TIME_DUSK_START = 19;

daynight = daynight or {
	m_RealTime = 6.5,
	m_Time = 6.5,
	m_Paused = false,

	GetRealTime = function( self )

		// Use this since we're using the server's time, which is sent
		return self.m_RealTime;

	end,

	GetTime = function( self )

		return self.m_Time;

	end
}

net.Receive("daynight_update", function(len)
	daynight.m_RealTime = net.ReadFloat()
	daynight.m_Time = net.ReadFloat()
	daynight.m_Paused = net.ReadBool()
end)

hook.Add( "Think", "daynightThink", function()

	local timeLen = 3600;
	if (daynight.m_Time > TIME_DUSK_START or daynight.m_Time < TIME_DAWN_END) then
		timeLen = daynight_length_night:GetInt();
	else
		timeLen = daynight_length_day:GetInt();
	end

	if ( !daynight.m_Paused and daynight_paused:GetInt() <= 0 ) then
		if ( daynight_realtime:GetInt() <= 0 ) then
			local t = daynight.m_Time + ( 24 / timeLen ) * FrameTime();
			if ( t > 24 ) then
				t = 0;
			end
			if (math.floor(t) != math.floor(daynight.m_Time)) then
				hook.Run("MK_DayNight_Hour", math.floor(t))
			end
			daynight.m_Time = t
		else
			daynight.m_Time = daynight:GetRealTime();
		end
	end

end );