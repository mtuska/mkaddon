// RAWR!

MK.nextbots.targetting = MK.nextbots.targetting or {}
MK.nextbots.targetting.tickTime = MK.nextbots.targetting.tickTime or 0
MK.nextbots.targetting.perceivers = MK.nextbots.targetting.perceivers or {}
MK.nextbots.targetting.stimuli = MK.nextbots.targetting.stimuli or {}
MK.nextbots.targetting.invalidPerceivers = MK.nextbots.targetting.invalidPerceivers or {}
MK.nextbots.targetting.invalidStimuli = MK.nextbots.targetting.invalidStimuli or {}
MK.nextbots.targetting.weaponIntimidation = {
	["cw_ar15"] = 0.25,
}

// The things trying to target something
function MK.nextbots.targetting.getPerceivers()
	return MK.nextbots.targetting.perceivers
end

function MK.nextbots.targetting.addPerceiver(ent)
	if (IsValid(ent)) then
		table.insert(MK.nextbots.targetting.perceivers, ent)
	end
end

// Used by Nextbots when using the TrySetEnemy
function MK.nextbots.targetting.isValidTarget(stimulus)
	if (stimulus:IsPlayer()) then
		if (!stimulus:Alive()) then return false end
		// When you're in a vehicle, it considers you to be noclipping
		if (stimulus:GetMoveType() == MOVETYPE_NOCLIP && !stimulus:InVehicle()) then return false end
		return true
	elseif (stimulus.MKNextbot || stimulus.MKFlyingNextbot) then
		return true
	end
end

function MK.nextbots.targetting.canTarget(perceiver, stimulus)
	if (perceiver == stimulus) then return false end
	if (!MK.nextbots.targetting.isValidTarget(stimulus)) then return false end
	return perceiver:IsEnemy(stimulus)
end

// The things can be targetted
function MK.nextbots.targetting.getStimuli()
	local _stimuli = table.Copy(MK.nextbots.targetting.stimuli)
	if (!GetConVar("ai_ignoreplayers"):GetBool()) then
		for k,v in pairs(player.GetAll()) do
			table.insert(_stimuli, v)
		end
	end
	return _stimuli
end

function MK.nextbots.targetting.addStimulus(ent)
	if (IsValid(ent)) then
		table.insert(MK.nextbots.targetting.stimuli, ent)
	end
end

// Point of view check
function MK.nextbots.targetting.checkPOV(ent, pos, dot)
	local dot = dot or -0.5
	local pos = (pos.GetPos and pos:GetPos()) or pos
	local dir = (ent:GetPos() - pos)
	dir:Normalize()
	if (dir:Dot(ent:GetForward()) < dot) then
		return true
	end
	return false
end

// Line of sight
function MK.nextbots.targetting.checkLOS(ent, pos)
	local tr = util.TraceLine({
			start = ent:EyePos(),
			endpos = pos,
			mask = MASK_NPCSOLID_BRUSHONLY,
	})
	if (tr.HitWorld) then return false end
	return true
end

// This is the threat the perceiver gets from the stimulus
function MK.nextbots.targetting.getThreat(perceiver, stimulus, distance)
	local threat = 0
	if (stimulus.GetIntimidation) then
		threat = stimulus:GetIntimidation()
	end
	if (stimulus.MKNextbot) then
		local wep = MK.nextbots.targetting.weaponIntimidation[stimulus:GetActiveWeapon()]
		threat = threat + (MK.nextbots.targetting.weaponIntimidation[stimulus:GetActiveWeapon()] or 0)
	elseif (stimulus.MKFlyingNextbot) then

	end
	return threat
end

MK.async.cancel("MK_Nextbots.Targetting") // helps for hotreload
MK.async("MK_Nextbots.Targetting", function()
	while(true) do
		if (GetConVar("ai_disabled"):GetBool() || !GetConVar("mk_nb_targetting"):GetBool()) then MK.async.wait(0.5) continue end
		for k,v in pairs(MK.nextbots.targetting.invalidPerceivers) do
			MK.nextbots.targetting.perceivers[k] = nil
		end
		for k,v in pairs(MK.nextbots.targetting.invalidStimuli) do
			MK.nextbots.targetting.stimuli[k] = nil
		end
		MK.nextbots.targetting.invalidPerceivers = {}
		MK.nextbots.targetting.invalidStimuli = {}
		MK.async.yield()
		local _perceivers = MK.nextbots.targetting.getPerceivers()
		if (table.Count(_perceivers) == 0) then continue end
		local _stimulus = MK.nextbots.targetting.getStimuli()
		if (table.Count(_stimulus) == 0) then continue end
		for _,ent in pairs(_perceivers) do
			MK.async.yield()
			if (!IsValid(ent)) then MK.nextbots.targetting.invalidPerceivers[_] = true continue end
			local _dists = {}
			for k,v in pairs(_stimulus) do
				if (!IsValid(v)) then MK.nextbots.targetting.invalidStimuli[k] = true continue end
				if (!MK.nextbots.targetting.canTarget(ent, v)) then ent:RemoveEnemyFromMemory(v) continue end
				local data = {}
				local dist = ent:GetPos():DistToSqr(v:GetPos())
				data.Distance = dist
				if (dist > ent.targetting.looseDistSqr) then ent:UpdateEnemyMemory(v, data) continue end
				if (ent.targetting.bPOV) then
					data.Visibility = MK.nextbots.targetting.checkPOV(ent, v:EyePos())
					if (!data.Visibility) then ent:UpdateEnemyMemory(v, data) continue end
				end
				// Do LOS last, it's the most taxing
				if (ent.targetting.bLOS) then
					data.Visibility = MK.nextbots.targetting.checkLOS(ent, v:EyePos())
					if (!data.Visibility) then ent:UpdateEnemyMemory(v, data) continue end
				end
				_dists[v] = dist
				data.Threat = MK.nextbots.targetting.getThreat(ent, v, dist)
				ent:UpdateEnemyMemory(v, data)
			end
			if (ent.targetting.bMemory) then
				ent:OnTargettingUpdate()
			else
				// If not using memory, just set the closest enemy
				local closestTarget, closestDist
				for k,v in pairs(_dists) do
					if (!closestDist || v <= closestDist) then
						closestDist = v
						closestTarget = k
					end
				end
				if (closestTarget) then
					ent:SetEnemy(closestTarget)
				else
					ent:SetEnemy(nil)
				end
			end
		end
	end
end)

// Event based stimuli(gun fired, etc)
function MK.nextbots.targetting.ProgateStimuli(ent)

end

/*
hook.Add("EntityFireBullets","MK_Nextbot.EntityFireBullets",function(ent, bullet)
	if ent:IsPlayer() then
		local trace = util.QuickTrace(bullet.Src, bullet.Dir, ent)
		local hitpos = trace.HitPos
		local shootpos = bullet.Src
		if isvector(hitpos) then
			local entlist1 = ents.FindInSphere(hitpos, Civs.AlertDist)
			local entlist2 = ents.FindInSphere(shootpos, Civs.AlertDist)
			for k,v in pairs(entlist1) do
				if v:GetClass():find("npc_civ") then
					MK.nextbots.targetting.ProgateStimuli(ent)
					v:FreakOut(shootpos, ent,"shoot")
				end
			end
			for k,v in pairs(entlist2) do
				if v:GetClass():find("npc_civ") then
					MK.nextbots.targetting.ProgateStimuli(ent)
					v:FreakOut(shootpos, ent,"shoot")
				end
			end
		end
	end
end)

*/