// RAWR!

MK.nutrition = MK.nutrition or {}

util.AddNetworkString("MK_Nutrition.Size")

function MK.nutrition.size(ent, size, plys)
	ent.mk = ent.mk or {}
	ent.mk.size = size
	local plys = plys or player.GetAll()
	net.Start("MK_Nutrition.Size")
		net.WriteEntity(ent)
		net.WriteFloat(size)
	net.Send(plys)
end

local Player = FindMetaTable("Player")

function Player:MK_Nutrition_Size(size)
	MK.nutrition.size(self, size)
end

hook.Add("MK_Core.NetworkEntityCreated", "MK_Core.NetworkEntityCreated.MK_Nutrition", function(ply, ent)
	if (ent.mk && ent.mk.size) then
		net.Start("MK_Nutrition.Size")
			net.WriteEntity(ent)
			net.WriteFloat(ent.mk.size)
		net.Send(ply)
	end
end)