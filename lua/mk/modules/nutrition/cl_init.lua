// RAWR!

MK.nutrition = MK.nutrition or {}
MK.nutrition.bones = {}
MK.nutrition.bones.scale = {}
MK.nutrition.bones.scale["ValveBiped.Bip01_Pelvis"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_Spine"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_Spine1"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_Spine2"] = {1, 0.4, 1.5}
MK.nutrition.bones.scale["ValveBiped.Bip01_Spine4"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_Neck1"] = {1, 1, 1.5}
MK.nutrition.bones.scale["ValveBiped.Bip01_R_Shoulder"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_L_Shoulder"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_R_Clavicle"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_L_Clavicle"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_R_Thigh"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_L_Thigh"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_R_Ulna"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_L_Ulna"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_R_UpperArm"] = {1, 0.4, 2}
MK.nutrition.bones.scale["ValveBiped.Bip01_L_UpperArm"] = {1, 0.4, 2}
MK.nutrition.bones.position = {}
MK.nutrition.bones.position["ValveBiped.Bip01_R_UpperArm"] = Vector(5,0,0)
MK.nutrition.bones.position["ValveBiped.Bip01_L_UpperArm"] = Vector(5,0,0)
MK.nutrition.bones.position["ValveBiped.Bip01_R_Thigh"] = Vector(-2,0,0)
MK.nutrition.bones.position["ValveBiped.Bip01_L_Thigh"] = Vector(2,0,0)
MK.nutrition.bones.position["ValveBiped.Bip01_Head1"] = Vector(1,1,0)

function MK.nutrition.size(ent, size)
	if (!IsValid(ent)) then return end
	for k,v in pairs(MK.nutrition.bones.scale) do
		if (!ent:LookupBone(k)) then continue end
		local vec = math.Clamp(size * v[1], v[2], v[3])
		ent:ManipulateBoneScale(ent:LookupBone(k), Vector(vec,vec,vec))
	end

	local per = (size-1)/1
	for k,v in pairs(MK.nutrition.bones.position) do
		if (!ent:LookupBone(k)) then continue end
		local vec = LerpVector(per, Vector(0,0,0), v)
		ent:ManipulateBonePosition(ent:LookupBone(k), vec)
	end
end

net.Receive("MK_Nutrition.Size", function(len)
	local ent = net.ReadEntity()
	local size = net.ReadFloat()

	MK.nutrition.size(ent, size)
end)