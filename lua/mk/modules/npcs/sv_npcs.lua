// RAWR!

util.AddNetworkString("npcOpen")
util.AddNetworkString("npcData")
util.AddNetworkString("npcClose")

hook.Add("KeyPress", "MK_NPC.KeyPress", function(ply, key)
	if (key == IN_USE && ply:GetMoveType() == MOVETYPE_WALK) then
		local data = {}
			data.start = ply:EyePos()
			data.endpos = ply:EyePos() + ply:GetAimVector()*108
			data.filter = ply
			data.mask = CONTENTS_SOLID + CONTENTS_MOVEABLE + CONTENTS_OPAQUE + CONTENTS_DEBRIS + CONTENTS_HITBOX + CONTENTS_MONSTER
		local entity = util.TraceLine(data).Entity

		if (IsValid(entity) && entity:GetClass() == "mk_npc") then
			entity:Interact(ply)

			return false
		end
	end
end)

hook.Add("canPocket", "MK_NPC.canPocket", function(ply, entity)
	if (IsValid(entity) && entity:GetClass() == "mk_npc") then
		return false
	end
end)

hook.Add("CanTool","MK_NPC.CanTool",function(ply,trace)
    if (trace.Entity && IsValid(trace.Entity) && trace.Entity:GetClass():lower():find("npc_civ") && !ply:IsAdmin()) then
        return false
    end
end)

hook.Add( "PhysgunPickup", "MK_NPC.PhysgunPickup", function( ply, ent )
    if (!ply:IsAdmin() && ent:GetClass():lower():find("npc_civ")) then
        return false
    end
end )

function MK.npcs.RemoveAll()
	local i = 0

	for k, v in ipairs(ents.FindByClass("mk_npc")) do
		if (IsValid(v)) then
			v:Remove()
			i = i + 1
		end
	end
	print("Removed "..i.." npcs")
end

function MK.npcs.Spawn(npcid, loc, ang)
	local data = MK.npcs.list[npcid]

	if (!data) then
		print("NPC '"..npcid.."' provided does not exist.")
		return
	end

	if (data.gamemode and data.gamemode:lower() != engine.ActiveGamemode():lower()) then
		print("NPC '"..npcid.."' is not available in this gamemode.")
		return
	end

	local class = "mk_npc"
	if (data.class) then
		class = data.class
	end
	local ent = ents.Create(class)
	ent:SetPos(loc)
	ent:SetAngles(ang)
	ent:Spawn()
	ent:SetCanWalk(data.canWalk or false)
	//ent:SetModel(data.model)
	ent:SetNPC(npcid)
	ent:Activate()
	return ent
end

concommand.Add("mk_createnpc", function(ply, command, arguments)
	if (!ply:IsSuperAdmin()) then
		return
	end

	local npcid = arguments[1] and arguments[1]:lower() or ""
	local data = MK.npcs.list[npcid]

	if (!data) then
		return ply:ChatPrint("The NPC type you provided does not exist.")
	end

	if (data.gamemode and data.gamemode:lower() != engine.ActiveGamemode():lower()) then
		return ply:ChatPrint("That NPC is not available in this gamemode.")
	end

	local position = ply:GetEyeTrace().HitPos
	local angles = (position - ply:GetPos()):Angle()
	angles.r = 0
	angles.p = 0
	angles.y = angles.y + 180

	MK.npcs.Spawn(npcid, ply:GetEyeTrace().HitPos, angles)
end)

concommand.Add("mk_removenpc", function(ply, command, arguments)
	if (!ply:IsSuperAdmin()) then
		return
	end

	local npcid = arguments[1] and arguments[1]:lower()

	if (npcid) then
		local i = 0

		for k, v in ipairs(ents.FindByClass("mk_npc")) do
			if (v:GetNPC() == npcid) then
				v:Remove()
				i = i + 1
			end
		end
		print("Removed "..i.." npcs")
	else
		local entity = ply:GetEyeTrace().Entity

		if (IsValid(entity) and entity:GetClass() == "mk_npc") then
			entity:Remove()
		end
	end
end)

net.Receive("npcClose", function(length, ply)
	ply.npcDialog:OnDialogClosed()
	ply.npcDialog = nil
end)

net.Receive("npcData", function(length, ply)
	local funcid = net.ReadString()
	local npcid = net.ReadString()
	local args = net.ReadTable()
	local entity = ply.npcDialog

	if (IsValid(entity) and ply:GetPos():Distance(entity:GetPos()) <= 128 and entity:GetNPC() == npcid) then
		local data = MK.npcs.list[entity:GetNPC()]

		if (data and type(data["on"..funcid]) == "function") then
			data["on"..funcid](data, ply, unpack(args))
		end
	end
end)