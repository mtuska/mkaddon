// RAWR!

MK.npcs = MK.npcs or {}
MK.npcs.list = MK.npcs.list or {}

function MK.npcs.load(npcid, inc_func)
	NPC = {npcid = npcid}
		NPC.default_team = TEAM_CITIZEN
		NPC.team = TEAM_CITIZEN

		if (SERVER) then
			function NPC:send(ply, funcid, ...)
				local entity = ply.npcDialog

				if (!IsValid(entity) or ply:GetPos():Distance(entity:GetPos()) > 128) then
					return
				end

				net.Start("npcData")
					net.WriteString(funcid)
					net.WriteString(self.npcid)
					net.WriteTable({...})
				net.Send(ply)
			end

			function NPC:onTeam(ply)
				if (ply:Team() == self.team) then
					ply:SetTeam(self.default_team)
				else
					ply:SetTeam(self.team)
				end
			end

			function NPC:close(ply)
				local entity = ply.npcDialog

				if (!IsValid(entity) or ply:GetPos():Distance(entity:GetPos()) > 128) then
					return
				end

				net.Start("npcClose")
					net.WriteString(self.npcid)
				net.Send(ply)

				ply.npcDialog = nil
			end
		else
			function NPC:addText(text, fromMe)
				local panel = npcDialog

				if (IsValid(panel)) then
					return panel:addText(text, fromMe)
				end
			end

			function NPC:addOption(text, callback)
				local panel = npcDialog

				if (IsValid(panel)) then
					return panel:addOption(text, callback)
				end
			end

			function NPC:clear()
				local panel = npcDialog

				if (IsValid(panel)) then
					return panel:clear()
				end
			end

			function NPC:send(funcid, ...)
				net.Start("npcData")
					net.WriteString(funcid)
					net.WriteString(self.npcid)
					net.WriteTable({...})
				net.SendToServer()
			end

			function NPC:addLeave(text)
				local panel = npcDialog
				if (text) then
					text = text .. " <Leave>"
				else
					text = "<Leave>"
				end

				if (IsValid(panel)) then
					return panel:addOption(text, nil, true)
				end
			end

			function NPC:close()
				local panel = npcDialog

				if (IsValid(panel)) then
					panel:Remove()
				end
			end
		end

		inc_func()

		MK.npcs.list[npcid] = NPC
	NPC = nil
end

function MK.npcs.GMInitialize()
	local files, dirs = file.Find("mk/npcs/*", "LUA")

	for k, v in ipairs(files) do
		MK.npcs.load(string.Replace(v, ".lua", ""), function()
			if (SERVER) then
				include("mk/npcs/"..v)
				AddCSLuaFile("mk/npcs/"..v)
			else
				include("mk/npcs/"..v)
			end
		end)
	end

	for k, v in ipairs(dirs) do
		MK.npcs.load(v, function()
			if (SERVER) then
				include("mk/npcs/"..v.."/sv_npc.lua")
				AddCSLuaFile("mk/npcs/"..v.."/cl_npc.lua")
			else
				include("mk/npcs/"..v.."/cl_npc.lua")
			end
		end)
	end
end