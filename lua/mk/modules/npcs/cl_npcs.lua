// RAWR!

npcDialog = npcDialog or {}

net.Receive("npcOpen", function()
	local index = net.ReadUInt(14)
	local entity = Entity(index)

	if (!IsValid(entity)) then
		return
	end

	local npcid = entity:GetNPC()
	
	if (!MK.npcs.list[npcid]) then
		return
	end

	if (MK.npcs.list[npcid].shop) then
		local shop = vgui.Create("cnShop")
		shop:setup(MK.npcs.list[npcid].shop)
		shop.entity = entity
	else
		vgui.Create("mk_npcs_dialog")

		npcDialog.entity = entity
		npcDialog.npcid = npcid
		npcDialog:setup(npcid)

		if (MK.npcs.list[npcid].onStart) then
			MK.npcs.list[npcid]:onStart()
		end
	end
end)

net.Receive("npcData", function()
	local funcid = net.ReadString()
	local npcid = net.ReadString()
	local arguments = net.ReadTable()
	local panel = npcDialog

	if (!IsValid(panel) or panel.npcid != npcid) then
		return
	end

	local data = MK.npcs.list[panel.npcid]

	if (data and type(data["on"..funcid]) == "function") then
		data["on"..funcid](data, unpack(arguments))
	end
end)

net.Receive("npcClose", function()
	local npcid = net.ReadString()
	local panel = npcDialog

	if (IsValid(panel) and panel.npcid == npcid) then
		panel:Remove()
	end
end)