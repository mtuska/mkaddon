// RAWR!

MK.nextbots.culling = MK.nextbots.culling or {}
MK.nextbots.culling.dist = 1000
MK.nextbots.culling.distSqr = MK.nextbots.culling.dist * MK.nextbots.culling.dist

MK.async.cancel("MK_Nextbots.Culling") // helps for hotreload
MK.async("MK_Nextbots.Culling", function()
	while(true) do
		for _,ent in pairs(MK.nextbots.list) do
			for __,ply in pairs(player.GetAll()) do
				MK.async.yield()
				if (!IsValid(ent)||!IsValid(ply)) then continue end
				if (ent:GetPos():DistToSqr(ply:GetPos()) > MK.nextbots.culling.distSqr) then
					ent:SetPreventTransmit(ply, true)
					for k,v in pairs(ent:GetChildren()) do
						v:SetPreventTransmit(ply, true)
					end
				else
					ent:SetPreventTransmit(ply, false)
					for k,v in pairs(ent:GetChildren()) do
						v:SetPreventTransmit(ply, false)
					end
				end
			end
		end
		MK.async.wait(0.2)
	end
end)