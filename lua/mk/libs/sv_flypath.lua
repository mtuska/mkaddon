// RAWR!

local PathSegment = {}
PathSegment.area = nil
PathSegment.curvature = 0
PathSegment.distanceFromStart = 0
PathSegment.forward = Vector()
PathSegment.how = 9
PathSegment.ladder = nil
PathSegment.length = 0
PathSegment.m_portalCenter = Vector()
PathSegment.m_portalHalfWidth = 0
PathSegment.pos = Vector()
PathSegment.type = 2

local FlyPathMeta = {}
FlyPathMeta.__index = FlyPathMeta
function FlyPathMeta:Chase(bot, ent)
    if (!IsValid(ent)) then
        self:Invalidate()
        return
    end
    self.EndPos = ent:GetPos()
    self.GoalPos = ent:GetPos() + Vector(0, 0, 50)
    self:Update(bot)
end

function FlyPathMeta:Compute(from, to, generator)
    self.Nexbot = from
    self.PathBuiltTime = CurTime()
    self.StartPos = from:GetPos()
    self.EndPos = to
    self.GoalPos = to
    self.m_IsValid = true
    //generator(area, fromArea, ladder, elevator, length)
    return true
end

function FlyPathMeta:Draw()

end

function FlyPathMeta:FirstSegment()
    local obj = {}
    setmetatable(obj,PathSegment)
    return obj
end

function FlyPathMeta:GetAge()
    return CurTime() - self.PathBuiltTime
end

function FlyPathMeta:GetAllSegments()
    local obj = {}
    setmetatable(obj,PathSegment)
    return {obj}
end

function FlyPathMeta:GetClosestPosition(pos)
    return pos
end

function FlyPathMeta:GetCurrentGoal()
    return self.GoalPos
end

function FlyPathMeta:GetCursorData()
    return {curvature = 0, forward = Vector(), pos = vector()}
end

function FlyPathMeta:GetCursorPosition()
    return 0
end

function FlyPathMeta:GetEnd()
    return self.EndPos
end

function FlyPathMeta:GetHindrance()
    return nil
end

function FlyPathMeta:GetLength()
    return 0
end

function FlyPathMeta:GetPositionOnPath(distance)
    return Vector()
end

function FlyPathMeta:GetStart()
    return self.StartPos
end

function FlyPathMeta:Invalidate()
    self.m_IsValid = false
end

function FlyPathMeta:IsValid()
    return self.m_IsValid
end

function FlyPathMeta:LastSegment()
    local obj = {}
    setmetatable(obj,PathSegment)
    return obj
end

function FlyPathMeta:MoveCursor(distance)

end

function FlyPathMeta:MoveCursorTo(distance)

end

local SEEK_ENTIRE_PATH = 0
local SEEK_AHEAD = 0
local SEEK_BEHIND = 0
function FlyPathMeta:MoveCursorToClosestPosition(pos, type, alongLimit)

end

function FlyPathMeta:MoveCursorToEnd()

end

function FlyPathMeta:MoveCursorToStart()

end

function FlyPathMeta:ResetAge()
    self.PathBuiltTime = CurTime()
end

function FlyPathMeta:SetGoalTolerance(distance)
    self.Tolerance = distance^2
end

function FlyPathMeta:SetMinLookAheadDistance(minDistance)

end

function FlyPathMeta:Update(bot)
    if (bot:GetPos():DistToSqr(self.GoalPos) > self.Tolerance) then
        //bot.loco:Approach(self.GoalPos, 5)
        local goal = self.GoalPos
        local normal = (goal - bot:GetPos()):GetNormalized()
        local velocity = normal * bot:GetFlySpeed()
        //BroadcastLua("print('Approach "..tostring(normal)..", "..tostring(self.GoalPos)..", "..tostring(bot:GetPos()).."')")
        //if (bot:GetVelocity():Length2D() <= 5) then
            bot:SetVelocity(velocity-bot:GetVelocity())
            //bot:SetVelocity(velocity)
        //end
        //BroadcastLua("print('Approach')")
    else
        bot:SetVelocity(-bot:GetVelocity()) // Negate the velocity
        self:Invalidate()
    end
end

MK.path = MK.path or {}
MK.path.flyPathMeta = FlyPathMeta