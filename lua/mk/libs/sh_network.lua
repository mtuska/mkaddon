// RAWR!

if (SERVER) then
	util.AddNetworkString("MK_Core.NetworkEntityCreated")

	net.Receive("MK_Core.NetworkEntityCreated", function(len, ply)
		local ent = net.ReadEntity()
		if (ent.OnEntityReady) then
			ent:OnEntityReady(ply)
		end
		hook.Run("MK_Core.NetworkEntityCreated", ply, ent)
	end)
elseif (CLIENT) then
	hook.Add("NetworkEntityCreated", "MK_Core.NetworkEntityCreated", function(ent)
		net.Start("MK_Core.NetworkEntityCreated")
			net.WriteEntity(ent)
		net.SendToServer()
	end)
end