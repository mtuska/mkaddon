// RAWR!

MK.path = MK.path or {}
MK.path._path = MK.path._path or Path

function Path(type)
    if (type == "Fly") then
        local path = {}
	    setmetatable(path,MK.path.flyPathMeta)
        return path
    end
    return MK.path._path(type)
end