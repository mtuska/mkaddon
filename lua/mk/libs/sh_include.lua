// RAWR!

// This is to help find which addons take more time to load at start up

MK.include = MK.include or {}
MK.include.initialized = MK.include.initialized or false
MK.include.list = MK.include.list or {}
MK.include.order = MK.include.order or {}
MK.include.times = MK.include.times or {}

MK.include.old_include = include

function MK.include.include(str)
	local st = SysTime()
	local result = MK.include.old_include(str)
	local rt = SysTime() - st

	local level = 1
	local _debug, filename, addon = {}, "", ""
	while (true) do
		local info = debug.getinfo(level, "Sln")
		if ( !info ) then break end
		table.insert(_debug, info)

		local tbl = string.Explode("/", info.short_src)
		filename = info.short_src
		if (info.short_src && tbl[1] == "addons"/* && (!tbl[4] || (tbl[4] != "entities" && tbl[4] != "weapons"))*/) then
			addon = tbl[2]
		end
		level = level + 1
	end
	table.insert(MK.include.list, {filename, rt, _debug})
	if (addon != "") then
		MK.include.times[addon] = MK.include.times[addon] or {}
		table.insert(MK.include.times[addon], rt)

		if (!table.HasValue(MK.include.order, addon)) then
			table.insert(MK.include.order, addon)
		end
	elseif (filename != "") then
		MK.include.times[filename] = MK.include.times[filename] or {}
		table.insert(MK.include.times[filename], rt)

		if (!table.HasValue(MK.include.order, filename)) then
			table.insert(MK.include.order, filename)
		end
	end
	return result
end
// Only override during setup
if (!MK.include.initialized) then
	include = MK.include.include
end

function MK.include.GMInitialize()
	// Reset include function now to lessen memory
	MK.include.initialized = true
	include = MK.include.old_include

	local times = {}
	for k,v in pairs(MK.include.times) do
		local t = 0
		for _,_v in pairs(v) do
			t = t + _v
		end
		times[k] = math.Round(t, 3)
	end

	MK.msg("Addon load order:")
	MK.msg("\t"..table.concat(MK.include.order, ", "))
	MK.msg("Addon include time(ordered by time taken):")
	for k,v in SortedPairsByValue(times, true) do
		MK.msg("\t"..k.."\t"..math.Round(v, 3).."s")
	end
end

function MK.include.WriteTable( t, indent, done )

	done = done or {}
	indent = indent or 0
	local keys = table.GetKeys( t )

	table.sort( keys, function( a, b )
		if ( isnumber( a ) && isnumber( b ) ) then return a < b end
		return tostring( a ) < tostring( b )
	end )

	done[ t ] = true

	local filename = "nm/devlog/includes.txt"

	for i = 1, #keys do
		local key = keys[ i ]
		local value = t[ key ]
		file.Append(filename, string.rep( "\t", indent ) )

		if  ( istable( value ) && !done[ value ] ) then

			done[ value ] = true
			file.Append(filename, tostring( key ) .. ":" .. "\n" )
			MK.include.WriteTable ( value, indent + 2, done )
			done[ value ] = nil

		else

			file.Append(filename, tostring( key ) .. "\t=\t" )
			file.Append(filename, tostring( value ) .. "\n" )

		end

	end

end