// RAWR!

// This is not a module as it includes just basic functionality needed for our nextbot entities

MK.nextbots = MK.nextbots or {}

CreateConVar("mk_nb_locked", 0, FCVAR_NONE, "0: Disables all behavior; 1: Enables all behavior", 0, 1)
CreateConVar("mk_nb_movement", 1, FCVAR_NONE, "0: Disables movement; 1: Enables movement", 0, 1)
CreateConVar("mk_nb_aiming", 1, FCVAR_NONE, "0: Disables aiming; 1: Enables aiming", 0, 1)
CreateConVar("mk_nb_attacking", 1, FCVAR_NONE, "0: Disables attacking; 1: Enables attacking", 0, 1)
CreateConVar("mk_nb_targetting", 1, FCVAR_NONE, "0: Disables targetting; 1: Enables targetting", 0, 1)
CreateConVar("mk_nb_usedoors", 1, FCVAR_NONE, "0: Disables use of doors; 1: Enables use of doors", 0, 1)
CreateConVar("mk_nb_scaledamage", 1, FCVAR_NONE, "Value to scale damage by", 0)
CreateConVar("mk_nb_door_respawntime", 90, FCVAR_NONE, "How long to respawn doors in seconds", 0)

MK.nextbots.list = MK.nextbots.list or {}
MK.nextbots.airnodes = MK.nextbots.airnodes or {}
MK.nextbots.m_pathingPerFrame = MK.nextbots.m_pathingPerFrame or 0
MK.nextbots.m_waitingToPath = MK.nextbots.m_waitingToPath or {}
MK.nextbots.config = {}
MK.nextbots.config.max = 200 // Max number of nextbots
MK.nextbots.config.maxPathing = 2 // Max number of nextbots doing pathing
MK.nextbots.config.cleanupBodies = 60
MK.nextbots.config.vehKillSpeed = 400
MK.nextbots.config.meleeRange = 80
MK.nextbots.config.meleeRangeSqr = MK.nextbots.config.meleeRange * MK.nextbots.config.meleeRange

MK.nextbots.navareas = {
	spawns = {},
	indoors = {},
	outdoors = {},
	water = {},
	avoid = {},
	transient = {},
	other = {},
}

local function isOutside(pos)
	local trace = {}
	trace.start = pos + Vector(0,0,10)
	trace.endpos = trace.start + Vector(0, 0, 32768)
	local tr = util.TraceLine(trace)

	if (tr.HitSky or tr.HitNoDraw) then return true end

	return false
end

MK.async("MK_Nextbots.Navareas", function()
	local count = 0
	local perTick = 250
	local curTick = 0
	MK.msg("Setup navarea thread, "..perTick.." navareas per tick")
	for _,area in pairs(navmesh.GetAllNavAreas()) do
		curTick = curTick + 1
		if (curTick > perTick) then
			curTick = 0
			MK.async.yield()
		end
		count = count + 1
		if (area:HasAttributes(NAV_MESH_AVOID)) then
			table.insert(MK.nextbots.navareas.avoid, area)
		elseif (area:HasAttributes(NAV_MESH_TRANSIENT)) then
			table.insert(MK.nextbots.navareas.transient, area)
		elseif (area:IsUnderwater()) then
			table.insert(MK.nextbots.navareas.water, area)
		elseif (isOutside(area:GetCenter())) then
			table.insert(MK.nextbots.navareas.spawns, area)
		else
			table.insert(MK.nextbots.navareas.other, area)
		end

		count = count + 1
		if (isOutside(area:GetCenter())) then
			table.insert(MK.nextbots.navareas.outdoors, area)
		else
			table.insert(MK.nextbots.navareas.indoors, area)
		end
	end
	MK.msg(count.." Navmesh areas prepared out of "..table.Count(navmesh.GetAllNavAreas()).." navmesh areas")
	MK.msg("\t"..table.Count(MK.nextbots.navareas.spawns).." spawn areas")
	MK.msg("\t"..table.Count(MK.nextbots.navareas.outdoors).." outdoors areas")
	MK.msg("\t"..table.Count(MK.nextbots.navareas.indoors).." indoors areas")
	MK.msg("\t"..table.Count(MK.nextbots.navareas.transient).." transient areas")
	MK.msg("\t"..table.Count(MK.nextbots.navareas.avoid).." avoid areas")
	MK.msg("\t"..table.Count(MK.nextbots.navareas.water).." water areas")
	MK.msg("\t"..table.Count(MK.nextbots.navareas.other).." other areas")
end)

function MK.nextbots.TestSpawn(pos, los)
    local c = util.PointContents(pos)
    if (bit.band(c, CONTENTS_SOLID) == CONTENTS_SOLID) then return false end

    local trace = {}
    trace.start = pos
    trace.endpos = pos + Vector(0, 0, 64)
    trace.mins = Vector(-16, -16, 0)
    trace.maxs = Vector(16, 16, 1)
    local tr = util.TraceHull(trace)
    if (tr.Hit) then return false end

    if (los) then
        for _, v in pairs(player.GetAll()) do
            if (v:VisibleVec(pos)) then return false end
            if (v:CanSeeVec(pos)) then return false end
            if (v:CanSeeVec(pos + Vector(0, 0, 72))) then return false end
        end
    end

    return true
end

function MK.nextbots.Add(ent)
    if (!IsValid(ent)) then return end
    MK.nextbots.list[ent:EntIndex()] = ent
end

function MK.nextbots.Remove()
   for k,bot in pairs(MK.nextbots.GetAll()) do
		if (IsValid(bot)) then
			bot:Remove()
		end
		MK.nextbots.list[k] = nil
	end
end

function MK.nextbots.GetAll()
	for k,bot in pairs(MK.nextbots.list) do
		if (!IsValid(bot)) then
			MK.nextbots.list[k] = nil
		end
	end
    return MK.nextbots.list
end

concommand.Add("mk_nb_force_repath", function(ply)
	if (IsValid(ply) && !ply:IsSuperAdmin()) then return end
	local text = "Repathing all nextbots"
	if (IsValid(ply)) then
		ply:PrintMessage(HUD_PRINTCONSOLE, text)
	else
		print(text)
	end
	for k,v in pairs(MK.nextbots.GetAll()) do
		v:StopMovingToPos()
	end
end)

concommand.Add("mk_nb_force_crouch", function(ply)
	if (IsValid(ply) && !ply:IsSuperAdmin()) then return end
	for k,v in pairs(MK.nextbots.GetAll()) do
		v:Crouch()
	end
end)

concommand.Add("mk_nb_force_uncrouch", function(ply)
	if (IsValid(ply) && !ply:IsSuperAdmin()) then return end
	for k,v in pairs(MK.nextbots.GetAll()) do
		v:UnCrouch()
	end
end)

concommand.Add("mk_nb_spectate", function(ply)
	if (IsValid(ply) && !ply:IsSuperAdmin()) then return end
	if (ply.m_Spectating) then
		ply.m_Spectating = false
		ply:UnSpectate()
		return
	end
	local ent = ply:GetEyeTrace()
	if (!ent.Entity || !ent.Entity.MKNextbot) then return end
	ply:Spectate(OBS_MODE_CHASE)
	ply:SpectateEntity(ent.Entity)
	ply.m_Spectating = true
	ply.m_SpectatEntity = ent.Entity
end)

MK.async.cancel("MK_Players.Spectate")
MK.async("MK_Players.Spectate", function()
	while(true) do
		for k,v in pairs(player.GetAll()) do
			if (v.m_Spectating) then
				if (IsValid(v.m_SpectatEntity)) then
					v:SetPos(v.m_SpectatEntity:EyePos())
				else
					v.m_Spectating = false
					v:UnSpectate()
				end
			end
			MK.async.yield()
		end
		MK.async.wait(1)
	end
end)

MK.async.cancel("MK_Nextbots.Pathing")
MK.async("MK_Nextbots.Pathing", function()
	while(true) do
		MK.nextbots.m_pathingPerFrame = 0
		local i = 0
		for k,v in pairs(MK.nextbots.m_waitingToPath) do
			if (i >= MK.nextbots.config.maxPathing) then break end
			if (IsValid(k) && k.path) then
				i = i + 1
				k:ComputePath(k.path, v[1], v[2])
			end
			MK.nextbots.m_waitingToPath[k] = nil
			MK.async.yield()
		end
		MK.async.wait(0.1)
	end
end)

hook.Add("Think", "MK_Nextbots.Think", function()
	for k,v in pairs(MK.nextbots.GetAll()) do
		if (v.m_physgunned) then
			v.loco:SetVelocity(Vector(0,0,0))
		end
	end
end)

hook.Add("OnPhysgunPickup", "MK_Nextbots.OnPhysgunPickup", function(ply, ent)
	if (IsValid(ent) && (ent.MKNextbot || ent.MKFlyingNextbot)) then
		ent:StopMovingToPos()
		ent:Lock()
		ent.m_physgunned = true
		ent.loco:SetGravity(0)
		ent.loco:SetVelocity(Vector(0,0,0))
	end
end)

hook.Add("PhysgunDrop", "MK_Nextbots.PhysgunDrop", function(ply, ent)
	if (IsValid(ent) && (ent.MKNextbot || ent.MKFlyingNextbot)) then
		ent:UnLock()
		ent.m_physgunned = false
		ent.loco:SetGravity(1000)
	end
end)

hook.Add("PlayerCanPickupWeapon", "DontTakeFromMKNextBots", function(ply,wep)
	if ((wep.Owner && IsValid(wep.Owner) && wep.Owner.MKNextbot) || (wep.GetOwner && IsValid(wep:GetOwner()) && wep:GetOwner().MKNextbot)) then
		return false
	end
	return true
end)