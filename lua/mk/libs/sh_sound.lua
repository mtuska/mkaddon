// RAWR!

MK.sound = MK.sound or {}

if (SERVER) then
	util.AddNetworkString("MK_PlayURL")
	util.AddNetworkString("MK_PlayURL_Kill")
else
	net.Receive("MK_PlayURL", function()
		local url = net.ReadString()
		local start = net.ReadUInt(16)
		local length = net.ReadUInt(16)
		local pos = net.ReadVector()
		if (pos != Vector(0,0,0)) then
			MK.sound.PlayURLAtLoc(url, pos, start, length)
		else
			MK.sound.PlayURL(url, nil, start, length)
		end
	end)
	net.Receive("MK_PlayURL_Kill", function()
		MK.sound.StopSound()
	end)
end

function MK.sound.PlayURL(url, targets, start, length)
	if (!url) then return end
	start = start or 0
	length = length or 0
	if (CLIENT) then
		sound.PlayURL(url, (start != 0 and "noblock") or "", function(channel)
			if (channel != nil&&channel:IsValid()) then
				if (timer.Exists("MK_PlayURL")) then
					timer.Destroy("MK_PlayURL")
				end
				LocalPlayer().url = url
				if (LocalPlayer().channel != nil&&LocalPlayer().channel:IsValid()) then
					LocalPlayer().channel:Stop()
				end
				if (start != 0) then
					channel:SetTime(start)
				end
				channel:Play()
				LocalPlayer().channel = channel
				LocalPlayer().channelLength = length
				if (length > 0) then
					timer.Simple((length*.9), function()
						hook.Add("Think", "MK_PlaySound_Smooth", function()
							if (LocalPlayer().channel == nil||!LocalPlayer().channel:IsValid()) then
								hook.Remove("Think", "MK_PlaySound_Smooth")
							else
								local speed = LocalPlayer().channelLength*.1
								local vol = Lerp(speed * FrameTime(), LocalPlayer().channel:GetVolume(), 0)
								LocalPlayer().channel:SetVolume(vol)
							end
						end)
					end)
					timer.Create("MK_PlayURL", length, 1, function()
						if (LocalPlayer().channel != nil&&LocalPlayer().channel:IsValid()&&LocalPlayer().url == url) then
							MK.sound.StopSound()
						end
					end)
				end
			else
				print("Invalid URL!")
			end
		end)
	else
		targets = targets or player.GetAll()
		net.Start("MK_PlayURL")
			net.WriteString(url)
			net.WriteUInt(start, 16)
			net.WriteUInt(length, 16)
			net.WriteVector(Vector(0,0,0))
		net.Send(targets)
	end
end

function MK.sound.PlayURLAtLoc(url, pos, start, length)
	if (!url) then return end
	start = start or 0
	length = length or 0
	if (CLIENT) then
		sound.PlayURL(url, "3d", function(channel)
			if (channel != nil&&channel:IsValid()) then
				if (timer.Exists("MK_PlayURL")) then
					timer.Destroy("MK_PlayURL")
				end
				LocalPlayer().url = url
				if (LocalPlayer().channel != nil&&LocalPlayer().channel:IsValid()) then
					LocalPlayer().channel:Stop()
				end
				if (start != 0) then
					channel:SetTime(start)
				end
				channel:SetPos(pos)
				channel:Play()
				LocalPlayer().channel = channel
				if (length > 0) then
					timer.Create("MK_PlayURL", length, 1, function()
						if (LocalPlayer().channel != nil&&LocalPlayer().channel:IsValid()&&LocalPlayer().url == url) then
							MK.sound.StopSound()
						end
					end)
				end
			else
				print("Invalid URL!")
			end
		end)
	else
		net.Start("MK_PlayURL")
			net.WriteString(url)
			net.WriteUInt(start, 16)
			net.WriteUInt(length, 16)
			net.WriteVector(pos)
		net.Send(player.GetAll())
	end
end

function MK.sound.StopSound(targets)
	if (CLIENT) then
		LocalPlayer().url = nil
		if (LocalPlayer().channel != nil&&LocalPlayer().channel:IsValid()) then
			LocalPlayer().channel:Stop()
			LocalPlayer().channel = nil
		end
		LocalPlayer():ConCommand("stopsound")
	else
		targets = targets or player.GetAll()
		net.Start("MK_PlayURL_Kill")
		net.Send(targets)
	end
end