// RAWR!

MK.async = MK.async or {}
MK.async.list = MK.async.list or {}
MK.async.warnThreshold = 0.05
MK.async.threshold = 0.2
MK.async.debug_prints = true -- print upon completion

function MK.async.GMInitialize()
	// for any that were created before the gamemode initialized, reset the start time(we don't run coroutines until after the game thread starts)
	for k,v in pairs(MK.async.list) do
		MK.async.list[k][3] = SysTime()
	end
end

function MK.async:__call(name, fn, options)
	if (!isstring(name)) then
		error("Async requires a name in string form!")
	elseif (MK.async.list[name]) then
		error("Async function '"..name.."' already exists!")
	else
		if (MK.async.debug_prints) then
			MK.msg("Async '"..name.."' created")
		end
		options = options or {}
		MK.async.list[name] = {coroutine.create(fn), {}, SysTime(), options}
	end
end

function MK.async.exists(name)
	return MK.async.list[name] != nil
end

function MK.async.cancel(name)
	if (MK.async.list[name]) then
		if (MK.async.debug_prints) then
			MK.msg("Async '"..name.."' killed after "..math.Round(SysTime() - MK.async.list[name][3], 3).."s")
		end
		if (MK.async.list[name][4].onCanceled) then
			MK.async.list[name][4].onCanceled()
		end
		// When the last reference to the coroutine disappears, the garbage collector will take care of it.
		MK.async.list[name] = nil
	end
end

MK.async.yield = coroutine.yield
MK.async.wait = coroutine.wait

hook.Add("Think", "MK_Core.Async", function()
	local st = SysTime()
	for k,v in pairs(MK.async.list) do
		local st1 = SysTime()
		local vals = {coroutine.resume(v[1], table.unpack(v[2]))}
		local st2 = SysTime()
		if (v[4].maxage && (st2 - v[3]) > v[4].maxage) then
			MK.async.cancel(k)
			return
		end
		local tt = st2 - st1
		//MK.msg("Async '"..k.."' stole thread for "..tt.."s")
		if (tt > MK.async.warnThreshold) then
			if (tt > MK.async.threshold) then
				MK.msg("Async '"..k.."' took "..tt.."s! Greater than threshold "..MK.async.threshold.."s, killing coroutine")
				MK.async.cancel(k)
				return
			else
				MK.msg("Async '"..k.."' took "..tt.."s! Nearing kill threshold "..MK.async.threshold.."s")
			end
		end
		local ret = table.remove(vals, 1)
		if (ret) then
			MK.async.list[k][2] = vals
		else
			// coroutine is over
			// We don't perform a status check, since an extra check would be useless, just don't print that the corountine is dead
			if (table.Count(vals) > 0 && vals[1] != "cannot resume dead coroutine") then
				// Assume an error occured
				MK.msg("Async '"..k.."' error: "..vals[1])
			end
			if (MK.async.debug_prints) then
				MK.msg("Async '"..k.."' completed after "..math.Round(st2 - v[3], 3).."s")
			end
			if (isfunction(v[4].onCompleted)) then
				v[4].onCompleted(table.unpack(vals))
			end
			MK.async.list[k] = nil
		end
	end
	local tt = SysTime() - st
	if (tt > MK.async.threshold) then
		MK.msg("Async group of coroutine are over threshold! Check your functions!")
	end
end)

MK.async.__index = MK.async
setmetatable(MK.async, MK.async)