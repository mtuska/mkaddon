// RAWR!

MK = MK or {}
MK.folderName = "mk"
MK.startTime = MK.startTime or SysTime()
MK.initialized = MK.initialized or false
MK.modules = MK.modules or {}

include("sh_util.lua")
if (SERVER) then
	AddCSLuaFile("sh_util.lua")
end

function MK.msg(text)
	print("[MK] "..text)
end

function MK.Initialize()
	if (MK.initialized) then return end
	MK.msg("Initializing")
	// Libraries are then loaded, these contain specific functionalities and are used in-junctions with hooks and configs(these are modules/plugins)
	MK.util.IncludeInternalDir("libs/external")
	MK.util.IncludeInternalDir("libs")
	// Includes the metatables for the MK data types
	MK.util.IncludeInternalDir("meta")

	MK.msg("Libraries Loaded")

	local files, dirs = file.Find("mk/modules/*", "LUA")
	for k, v in pairs(files) do
		//print(v, string.sub(v, string.len(v)-4, 4))
		//if (string.sub(v, string.len(v)-4, 4) != ".lua") then continue end
		local name = string.sub(v, 1, -5)
		name = string.sub(name, 4)
		if (hook.Run("MK_Core.Module", name) != false) then
			MK.msg("Initializing MK Module: "..name)
			MK.util.IncludeInternal("modules/"..v)
			MK.modules[name] = true
		end
	end
	for k, v in pairs(dirs) do
		if (hook.Run("MK_Core.Module", v) != false) then
			MK.msg("Initializing MK Module: "..v)
			MK.util.IncludeInternalDir("modules/"..v)
			MK.modules[v] = true
		end
	end
	hook.Run("MK_Core.ModulesLoaded", false)

	// Library Initialization
	for k,v in pairs(MK) do
		if (v != MK.async && istable(v)) then
			if (v.ShInitialize) then
				v.ShInitialize() // Shared initialize(useful when you want Initialize in server/client realms, but also in shared)
			end
			if (v.Initialize) then
				v.Initialize()
			end
		end
	end

	// Include and Initialize gamemode specific files
	if (GAMEMODE) then
		MK.GMInitialize()
	end

	MK.initialized = true
	hook.Run("MK_Core.Initialized")
	MK.msg("Initialized")
end

function MK.GMInitialize()
	local time = math.Round(SysTime() - MK.startTime, 3)
	MK.msg("Server took "..time.."s from start to gamemode initialization!")

	local dir = "gamemodes/"..GAMEMODE.FolderName

	// Gamemode specific implementations
	MK.util.IncludeInternalDir(dir)
	MK.util.IncludeInternalDir(dir.."/hooks")
	MK.util.IncludeInternalDir(dir.."/meta")

	local files, dirs = file.Find("mk/"..dir.."/modules/*", "LUA")
	for k, v in pairs(files) do
		//print(v, string.sub(v, string.len(v)-4, 4))
		//if (string.sub(v, string.len(v)-4, 4) != ".lua") then continue end
		local name = string.sub(v, 1, -5)
		name = string.sub(name, 4)
		if (hook.Run("MK_Core.Module", name) != false) then
			MK.msg("Initializing MK Module: "..name)
			MK.util.IncludeInternal(dir.."modules/"..v)
			MK.modules[name] = true
		end
	end
	for k, v in pairs(dirs) do
		if (hook.Run("MK_Core.Module", v) != false) then
			MK.msg("Initializing MK Module: "..v)
			MK.util.IncludeInternalDir(dir.."modules/"..v)
			MK.modules[v] = true
		end
	end
	hook.Run("MK_Core.ModulesLoaded", true)

	MK.util.IncludeInternalDir("maps/"..game.GetMap())

	// Library Gamemode Initialization
	for k,v in pairs(MK) do
		if (v != MK.async && istable(v)&&v.GMInitialize) then
			v.GMInitialize()
		end
	end
end

hook.Add("Initialize", "Initialize.MK_Core", function()
	MK.GMInitialize()
end)

MK.Initialize()