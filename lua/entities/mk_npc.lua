AddCSLuaFile()

ENT.Base 			= "mk_base_nextbot"
ENT.Spawnable		= false

function ENT:SetupDataTables()
	self:MKSetupDataTables()
	self:NetworkVar("String", 2, "NPC")
end
if (CLIENT) then
	function ENT:Initialize()
		self:MKInitialize()
		self:addBubble()
	end

	return
end

function ENT:Initialize()
	self:MKInitialize()

	self:SetName("Civilian")

	self:SetWalkSpeed(math.random(50, 120))
	self:SetRunSpeed(math.random(180, 250))

	self:SetModelScale(math.Rand(0.8, 1.1), 0)
	self:SetPlayerColor(Vector(math.random(), math.random(), math.random()))
end

function ENT:RunBehaviour()
	while (true) do
		if (self.talking) then
			self:FaceTowardsAndWait(self.talking:GetPos())
		else
			coroutine.wait(math.random(1, 3))
			if (self:GetCanWalk()) then
				if (!self.talking) then
					self.loco:SetDesiredSpeed(self:GetWalkSpeed())
					self:MoveTo(self:GetDestination(), {useDoors = false})
				end
			else
				// Since most these are stationary NPCs, have them face a standard way
				//self:FaceTowardsAndWait(self:GetPos())
			end
		end

		coroutine.yield()
	end
end

function ENT:Think()
	self.MKBaseClass.Think(self)
	//self.loco:Jump()
end

function ENT:OnDialogClosed()
	self.talking = nil
end

function ENT:Interact(activator)
	if (!self:GetNPC()) then
		return
	end

	local data = MK.npcs.list[self:GetNPC()]

	if (data) then
		if (data.customUse && !data:customUse(activator, self)) then
			return
		end

		if (!IsValid(activator.npcDialog)) then
			activator.npcDialog = self
		else
			return
		end

		self.talking = activator
		self:StopMovingToPos()

		net.Start("npcOpen")
			net.WriteUInt(self:EntIndex(), 14)
		net.Send(activator)
	end
end