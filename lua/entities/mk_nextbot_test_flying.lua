// RAWR!

AddCSLuaFile()

ENT.Base 			= "mk_base_nextbot_flying"
ENT.Spawnable		= false

list.Set( "NPC", "mk_nextbot_test_flying", {
	Name = "Flying Test",
	Class = "mk_nextbot_test_flying",
	Category = "MK Nextbots"
} )

if (CLIENT) then
	return
end

ENT.targetting = {}
ENT.targetting.bEnabled = true
ENT.targetting.bPOV = false
ENT.targetting.bLOS = false
ENT.targetting.viewDist = 1000
ENT.targetting.looseDist = 2000

function ENT:Initialize()
	self:MKInitialize()

	self:SetName("Test Bot")

	//self:SetModel("models/bots/merasmus/merasmus.mdl") // merasmus
	self:SetModel("models/props_halloween/halloween_demoeye.mdl") // monoculus

	self:SetFlySpeed(100)
	self:SetHealth(1000)

	self:SetCollisionBounds(Vector(-32,-32,-32), Vector(32,32,32))
	self:SetPos(self:GetPos() + Vector(0,0,32))
end

function ENT:RunBehaviour()
	while (true) do
		self:FlyTo(self:GetDestination(), {maxage = 3, useDoors = false})

		coroutine.wait(5)

		coroutine.yield()
	end
end

function ENT:IsEnemy(ent)
	return true
end