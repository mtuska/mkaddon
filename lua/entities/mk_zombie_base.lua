// RAWR!

AddCSLuaFile()

ENT.Base 			= "mk_base_nextbot"
ENT.Spawnable		= false

if (CLIENT) then return end

ENT.targetting = {}
ENT.targetting.bEnabled = true
ENT.targetting.bPOV = false
ENT.targetting.bLOS = false
ENT.targetting.viewDist = 1000
ENT.targetting.looseDist = 2000

ENT.Damage = {1, 10} // Damage min and max
ENT.m_Sounds = {
	miss = {
		Sound("npc/zombie/claw_miss1.wav"),
		Sound("npc/zombie/claw_miss2.wav")
	},
	hit = {
		Sound("npc/zombie/zombie_hit.wav")
	},
	hit_flesh = {
		Sound("npc/zombie/claw_strike1.wav"),
		Sound("npc/zombie/claw_strike2.wav"),
		Sound("npc/zombie/claw_strike3.wav")
	},
	idle = {
		Sound("npc/zombie/zombie_voice_idle1.wav"),
		Sound("npc/zombie/zombie_voice_idle2.wav"),
		Sound("npc/zombie/zombie_voice_idle3.wav"),
		Sound("npc/zombie/zombie_voice_idle4.wav"),
		Sound("npc/zombie/zombie_voice_idle5.wav"),
		Sound("npc/zombie/zombie_voice_idle6.wav"),
		Sound("npc/zombie/zombie_voice_idle7.wav"),
		Sound("npc/zombie/zombie_voice_idle8.wav"),
		Sound("npc/zombie/zombie_voice_idle9.wav"),
		Sound("npc/zombie/zombie_voice_idle10.wav"),
		Sound("npc/zombie/zombie_voice_idle11.wav"),
		Sound("npc/zombie/zombie_voice_idle12.wav"),
		Sound("npc/zombie/zombie_voice_idle13.wav"),
		Sound("npc/zombie/zombie_voice_idle14.wav")
	},
	alert = {
		Sound("npc/zombie/zombie_alert1.wav"),
		Sound("npc/zombie/zombie_alert2.wav"),
		Sound("npc/zombie/zombie_alert3.wav")
	}
}

function ENT:Initialize()
    self:MKInitialize()

	self:SetName("Zombie")

	self:SetWalkSpeed(math.random(50, 120))
	self:SetRunSpeed(math.random(180, 250))

	self:SetModelScale(math.Rand(0.8, 1.2), 0)
	self:SetPlayerColor(Vector(math.random(), math.random(), math.random()))

	if (MK.modules.nutrition) then
		local size = 1
		if (math.Rand(0, 1) < 0.3) then
			size = math.Rand(0.4, 2)
		else
			size = math.Rand(0.8, 1.2)
		end
		self:MK_Nutrition_Size(size)
	end

    self.m_JustSpawned = true

    self:SetHealth(500)
    self:SetMaxHealth(500)
    self:SetWalkSpeed(50)
    self:SetRunSpeed(75)
    self:SetFindStance(false)
end

function ENT:RunBehaviour()
    while(true) do
        if (self.m_JustSpawned) then
            self.m_JustSpawned = false
            self:VoiceSound(self.m_Sounds.alert)
            self:PlaySequenceAndWait("taunt_zombie_original", 1)
        end

        if (self:CheckEnemy()) then
			self.Investigate = nil
            self.loco:SetDesiredSpeed(self:GetRunSpeed())
            self:Chase({useAttack = true})
		elseif (self.Investigate) then
			self.loco:SetDesiredSpeed(self:GetWalkSpeed())
			self:VoiceSound(self.m_Sounds.idle)
			local pos = self.Investigate
			self:MoveTo(self.Investigate)
			if (pos == self.Investigate) then
				self.Investigate = nil
			end
        else
            self.loco:SetDesiredSpeed(self:GetWalkSpeed())
    		//self.loco:SetDeathDropHeight( self.StepHeight )

            self:VoiceSound(self.m_Sounds.idle)

    		self:MoveTo(self:GetDestination())

			coroutine.wait(math.random(1.5))

	        if (math.random(100) <= 33) then
				self:VoiceSound(self.m_Sounds.idle)
			end
        end

        coroutine.yield()
    end
end

function ENT:AttackBehaviour()
	if (math.random(100) <= 1) then
		self:VoiceSound(self.m_Sounds.idle)
	end

	// Do a distance check in squares, since this will actually be run A LOT
	if (self:GetPos():DistToSqr(self:GetEnemy():GetPos()) > 10000) then // 100^2
		return
	end

    self:Attack(self:GetEnemy())
end

function ENT:Attack(ent)
	if (CurTime() < (self.m_NextAttack or 0)) then return end
    self.m_NextAttack = CurTime() + 2

	self.loco:ClearStuck() // We're attacking, doubt that we're stuck

	local enemy = ent // We check if enemy is valid before calling this function
	if (enemy.InVehicle && enemy:InVehicle()) then
		enemy = ent
	end

	self.m_Attacking = true
	//self:RestartGesture(ACT_GMOD_GESTURE_RANGE_ZOMBIE, true, true)
	local layer = self:AddGestureSequence(self:LookupSequence("zombie_attack_0"..math.random(1, 7)), true)
	self:SetLayerPlaybackRate(layer,0.6)
	self:SetPlaybackRate(0.1)

	local damage = math.random(unpack(self.Damage))
	timer.Simple(0.9, function()
		if (!IsValid(self)) then return end
		if (!self:Alive()) then return end

		if (!IsValid(enemy)) then
			self:VoiceSound(self.m_Sounds.miss, true)
			return
		end

		if (enemy.Alive && !enemy:Alive()) then
			self:VoiceSound(self.m_Sounds.miss, true)
			return
		end

		if (self:GetRangeTo(enemy) < 55) then
			if (ent:GetClass():find("breakable")) then
				enemy:Fire("Break", "", 0)
			elseif (enemy.m_HitsLeft) then
				enemy.m_HitsLeft = enemy.m_HitsLeft - 1
			else
				if (enemy:IsPlayer()) then
					enemy:ViewPunch(Angle(math.random(-1, 1)*damage, math.random(-1, 1)*damage, math.random(-1, 1)*damage))
				end
				enemy:TakeDamage(damage, self)

				local moveAdd=Vector(0,0,150)
				if (!enemy:IsOnGround()) then
					moveAdd=Vector(0,0,0)
				end
				enemy:SetVelocity(moveAdd+((enemy:GetPos()-self:GetPos()):GetNormal()*100)) -- apply the velocity
			end

			if (enemy.Alive) then
				self:VoiceSound(self.m_Sounds.hit_flesh, true)

				local bleed = ents.Create("info_particle_system")
				bleed:SetKeyValue("effect_name", "blood_impact_red_01")
				bleed:SetPos(enemy:GetPos() + Vector(0,0,70))
				bleed:Spawn()
				bleed:Activate()
				bleed:Fire("Start", "", 0)
				bleed:Fire("Kill", "", 0.2)
			else
				self:VoiceSound(self.m_Sounds.hit, true)
			end
		else
			self:VoiceSound(self.m_Sounds.miss, true)
		end
		self.m_Attacking = false
	end)
end

function ENT:DoorBehaviour(ent)
	local state = self:GetDoorState(ent)
	if (state == DOOR_STATE_NULL) then
		// Not a prop door for sure... gonna have to just open it instead

	elseif (state == DOOR_STATE_CLOSED) then
		// Attack that damn door!
		if (!ent.m_HitsLeft || ent.m_HitsLeft > 0) then
			ent.m_HitsLeft = ent.m_HitsLeft or 2
			self:Attack(ent)
			return
		else
			ent.m_HitsLeft = nil
			local door = ents.Create("prop_physics")

			if door then
				door:SetModel(ent:GetModel())
				door:SetPos(ent:GetPos())
				door:SetAngles(ent:GetAngles())
				door:Spawn()
				door.FalseProp = true
				//door:EmitSound("Wood_Plank.Break")
				door:EmitSound("physics/wood/wood_crate_break3.wav")
				local phys = door:GetPhysicsObject()

				if (phys ~= nil and phys ~= NULL and phys:IsValid()) then
					phys:ApplyForceCenter(self:GetForward():GetNormalized() * 20000 + Vector(0, 0, 2))
				end

				local effectdata = EffectData()
				effectdata:SetOrigin( door:GetPos() + door:OBBCenter() )
				effectdata:SetMagnitude( 5 )
				effectdata:SetScale( 2 )
				effectdata:SetRadius( 5 )
				util.Effect( "Sparks", effectdata )

				door:SetSkin(ent:GetSkin())
				door:SetColor(ent:GetColor())
				door:SetMaterial(ent:GetMaterial())
				ent.phys_door = door
				self:BreakDoor(ent)
			end
		end
	end
end

function ENT:BreakableBehaviour(ent)
	self:Attack(ent)
end

function ENT:OnSetEnemy()
    self:StopMovingToPos()
    self:VoiceSound(self.m_Sounds.alert)
end

function ENT:OnInjured(dmginfo)
	self.MKBaseClass.OnInjured(self, dmginfo)
	if (!self:HasEnemy()) then
		self:TrySetEnemy(dmginfo:GetAttacker())
	end
end

function ENT:OnKilled(dmginfo)
	self.MKBaseClass.OnKilled(self, dmginfo)
end

function ENT:GetIntimidation()
	return 1
end

function ENT:IsEnemy(ent)
	if (ent.IsZombie and ent:IsZombie()) then
		return false
	end
	return true
	//return ent.SteamID and ent:SteamID() == "STEAM_0:1:7102488"
end

function ENT:IsZombie()
	return true
end

function ENT:CalcIdleActivity()
	return ACT_HL2MP_IDLE_ZOMBIE
end

function ENT:CalcWalkActivity()
	return ACT_HL2MP_WALK_ZOMBIE_01
end

function ENT:CalcRunActivity()
	return ACT_HL2MP_RUN_ZOMBIE
end

--[[
npc/zombie/claw_miss1.wav
npc/zombie/claw_miss2.wav
npc/zombie/claw_strike1.wav
npc/zombie/claw_strike2.wav
npc/zombie/claw_strike3.wav
npc/zombie/foot1.wav
npc/zombie/foot2.wav
npc/zombie/foot3.wav
npc/zombie/foot_slide1.wav
npc/zombie/foot_slide2.wav
npc/zombie/foot_slide3.wav
npc/zombie/moan_loop1.wav
npc/zombie/moan_loop2.wav
npc/zombie/moan_loop3.wav
npc/zombie/moan_loop4.wav
npc/zombie/zombie_alert1.wav
npc/zombie/zombie_alert2.wav
npc/zombie/zombie_alert3.wav
npc/zombie/zombie_die1.wav
npc/zombie/zombie_die2.wav
npc/zombie/zombie_die3.wav
npc/zombie/zombie_hit.wav
npc/zombie/zombie_pain1.wav
npc/zombie/zombie_pain2.wav
npc/zombie/zombie_pain3.wav
npc/zombie/zombie_pain4.wav
npc/zombie/zombie_pain5.wav
npc/zombie/zombie_pain6.wav
npc/zombie/zombie_pound_door.wav
npc/zombie/zombie_voice_idle1.wav
npc/zombie/zombie_voice_idle10.wav
npc/zombie/zombie_voice_idle11.wav
npc/zombie/zombie_voice_idle12.wav
npc/zombie/zombie_voice_idle13.wav
npc/zombie/zombie_voice_idle14.wav
npc/zombie/zombie_voice_idle2.wav
npc/zombie/zombie_voice_idle3.wav
npc/zombie/zombie_voice_idle4.wav
npc/zombie/zombie_voice_idle5.wav
npc/zombie/zombie_voice_idle6.wav
npc/zombie/zombie_voice_idle7.wav
npc/zombie/zombie_voice_idle8.wav
npc/zombie/zombie_voice_idle9.wav
npc/zombie/zo_attack1.wav
npc/zombie/zo_attack2.wav
]]--
