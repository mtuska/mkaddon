// RAWR!

ENT.WeaponAttachment = "anim_attachment_RH"
ENT.ReloadAnimations = {
	["ar2"] = "reload_ar2_original",
	["smg"] = "reload_smg1_original",
	["pistol"] = "reload_pistol_original",
	["duel"] = "reload_dual_original",
	["revolver"] = "reload_revolver_original",
	["shotgun"] = "reload_shotgun_original",
}

function ENT:CanAttack()
	if (self.Reloading) then
		return false
	end
	return true
end

function ENT:PrimaryAttack()
	if (!self:CanAttack()) then
		return
	end
	local wep = self:GetActiveWeapon()
	if (IsValid(wep)&&wep.PrimaryAttack) then
		if (wep.DarkRPBased) then
			wep:PrimaryAttack()
		elseif (string.find(self:GetActiveWeapon():GetClass(), "m9k") && // m9k hax
				self:CanAttack() && self:GetActiveWeapon():CanPrimaryAttack() && self:GetActiveWeapon():GetNextPrimaryFire() < CurTime()) then
			self:GetActiveWeapon():ShootBulletInformation()
			self:GetActiveWeapon():TakePrimaryAmmo(1)
			self:GetActiveWeapon():EmitSound(self:GetActiveWeapon().Primary.Sound)
			self:SetAnimation(PLAYER_ATTACK1)
			self:MuzzleFlash()
			self:GetActiveWeapon():SetNextPrimaryFire(CurTime()+1/(self:GetActiveWeapon().Primary.RPM/60))
			self:GetActiveWeapon().RicochetCoin = (math.random(1,4))
		elseif ((!wep.CanPrimaryAttack || wep:CanPrimaryAttack()) && wep:GetNextPrimaryFire() < CurTime()) then
			wep:PrimaryAttack()
		end
		//print("cant Primary Attack")
	end
end
ENT.FirePrimary = ENT.PrimaryAttack

function ENT:SecondaryAttack()
	if (!self:CanAttack()) then
		return
	end
	local wep = self:GetActiveWeapon()
	if (IsValid(wep)&&wep.SecondaryAttack) then
		/*if (wep.DarkRPBased) then
			wep:SecondaryAttack()
		else*/if ((!wep.CanSecondaryAttack || wep:CanSecondaryAttack()) && wep:GetNextSecondaryFire() < CurTime()) then
			wep:SecondaryAttack()
		end
	end
end
ENT.FireSecondary = ENT.SecondaryAttack

function ENT:Reload()
	local wep = self:GetActiveWeapon()
	if (IsValid(wep) && wep:Clip1() < wep.Primary.ClipSize) then
		if (self.ReloadAnimations[wep:GetHoldType()]) then
			local seq = self:LookupSequence(self.ReloadAnimations[wep:GetHoldType()])
			timer.Simple(self:SequenceDuration(seq), function()
				if (IsValid(self)) then
					self.Reloading = false
				end
			end)
			local layer = self:AddGestureSequence(seq, true)
		else
			print("Missing reload animation for hold type ", wep:GetHoldType())
			timer.Simple(1, function()
				if (IsValid(self)) then
					self.Reloading = false
				end
			end)
		end
		self.Reloading = true
		self:RemoveAmmo((wep.Primary.ClipSize - wep:Clip1()), wep.Primary.Ammo, false)
		wep:SetClip1(wep.Primary.ClipSize)
	end
end

local function SpawnWeapon(self, class)
	local wep = ents.Create(class)
	if (!IsValid(wep)) then
		MsgN(string.format("Failed spawning '%s' as weapon for %s", class, self:GetClass()))
		return nil
	end
	wep:SetPos(self:GetPos())
	wep:SetOwner(self)
	wep.Owner = self
	wep:Spawn()
	if (wep:Clip1() <= 0) then
		wep:SetClip1(1) // BaseClass now checks for clip <= 0, let's set this to 1 on spawn to be sure
	end
	if (wep.isNearWall) then // cw2.0 fix
		wep.isNearWall = self.weaponOverrides["cw2.0"].isNearWall
		wep.FireBullet = self.weaponOverrides["cw2.0"].FireBullet
	end
	return wep
end

ENT.invalidWeapons = {"weapon_357", "weapon_pistol", "weapon_bugbait", "weapon_crossbow", "weapon_crowbar", "weapon_frag", "weapon_physcannon", "weapon_ar2", "weapon_rpg", "weapon_slam", "weapon_shotgun", "weapon_smg1", "weapon_stunstick"}
function ENT:Give(class, bNoAmmo)
	class = class:lower()
	if (table.HasValue(self.invalidWeapons, class)) then
		ErrorNoHalt("Can not give "..tostring(self).." weapon "..class..". Incompatable with nextbots\n")
		return
	elseif (!IsValid(self.Weapons[class])) then
		local wep = SpawnWeapon(self, class)
		if (wep and wep:IsValid()) then
			local oldWep = self:GetActiveWeapon()
			if (IsValid(oldWep)) then
				if (oldWep:GetClass() == class) then
					return
				end
				oldWep:SetNoDraw(true)
				//wep:Remove()
			end
			wep:SetClip1(wep.Primary.ClipSize)
			wep:SetClip2(wep.Secondary.ClipSize)

			local pos = self:GetAttachment(self:LookupAttachment(self.WeaponAttachment)).Pos -- location of the hand attachment
			wep:SetPos(pos)
			wep:SetParent(self)

			wep.OldCollisionGroup = wep:GetCollisionGroup()
			wep:SetCollisionGroup(COLLISION_GROUP_NONE)
			wep.OldSolid = wep:GetSolid()
			wep:SetSolid(SOLID_NONE)

			wep:Fire("setparentattachment", "anim_attachment_RH")
			wep:AddEffects(EF_BONEMERGE)
			wep:SetAngles(self:GetForward():Angle())

			if (wep.Equip) then
				wep:Equip(self)
			end

			self:DeleteOnRemove(wep)
			self:SetActiveWeapon(wep)
			self.Weapons[class] = wep
		end
		if (bNoAmmo) then

		end
	end
end

function ENT:SelectWeapon(class)
	if (!class || class == "") then return end
	if (!self:GetWeapon(class)) then return end
	local wep = self:GetActiveWeapon()
	if (IsValid(wep)) then
		if (wep:GetClass() == class) then
			return
		end
		wep:SetNoDraw(true)
		//wep:Remove()
	end

	local wep = self:GetWeapon(class)
	wep:SetNoDraw(false)
	self:SetActiveWeapon(wep)

	/*local pos = self:GetAttachment(self:LookupAttachment(self.WeaponAttachment)).Pos -- location of the hand attachment

	local wep = SpawnWeapon(self, class)
	if (wep and wep:IsValid()) then
		wep:SetPos(pos)
		wep:SetParent(self)

		wep.OldCollisionGroup = wep:GetCollisionGroup()
		wep:SetCollisionGroup(COLLISION_GROUP_NONE)
		wep.OldSolid = wep:GetSolid()
		wep:SetSolid(SOLID_NONE)

		wep:Fire("setparentattachment", "anim_attachment_RH")
		wep:AddEffects(EF_BONEMERGE)
		wep:SetAngles(self:GetForward():Angle())

		if (wep.Equip) then
			wep:Equip(self)
		end

		self.Weapon = wep
		self:SetActiveWeapon(wep)
	end*/
end

function ENT:PickUpWeapon(wep)
	if (hook.Call("NextbotCanPickupWeapon", GAMEMODE, self, wep) == false) then return false end
	self:Give(wep:GetClass())
	wep:Remove()
	return true
end

function ENT:DropNamedWeapon(class)
	if (self.Weapons[class]) then
		self:DropWeapon(self.Weapons[class])
	end
end

function ENT:DropWeapon(wep)
	local class = wep:GetClass():lower()
	if (IsValid(wep)) then
		self.Weapons[class] = nil
	end
	wep:Remove()
	wep = nil

	local wep = SpawnWeapon(self, class)
	if (wep and wep:IsValid()) then
		local vel = wep:GetVelocity()
		local pos = wep:GetPos()
		local ang = wep:GetAngles()
		wep:SetGravity(1)
		wep:SetVelocity(vel or Vector(0,0,0))
		wep:SetPos(pos)
		wep:SetAngles(ang)
	end
end