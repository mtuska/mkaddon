// RAWR!

// Player functions

function ENT:AddDeaths(num)
	self:SetDeaths(self:Deaths() + num)
end

function ENT:AddFrags(num)
	self:SetFrags(self:Deaths() + num)
end

function ENT:AddFrozenPhysicsObject(ent, physobj)

end

function ENT:AllowImmediateDecalPainting()
	return false
end

function ENT:Ban(min, reason)
	self:Remove()
end

util.AddNetworkString("MKNextBot_ColorRagdoll")
function ENT:CreateRagdoll()
	local rag = ents.Create("prop_ragdoll")
	if not IsValid(rag) then return nil end
	rag:SetPos(self:GetPos())
	rag:SetAngles(self:GetAngles())
	rag:SetModel(self:GetModel())
	rag:Spawn()
	rag:Activate()
	net.Start("MKNextBot_ColorRagdoll")
		net.WriteEntity(rag)
		net.WriteVector(self:GetPlayerColor())
	net.Broadcast()

	-- nonsolid to players, but can be picked up and shot
	rag:SetCollisionGroup(COLLISION_GROUP_WEAPON)

	-- position the bones
	local num = rag:GetPhysicsObjectCount()-1
	local v = self:GetVelocity()
	-- bullets have a lot of force, which feels better when shooting props,
	-- but makes bodies fly, so dampen that here
	/*if dmginfo:IsDamageType(DMG_BULLET) or dmginfo:IsDamageType(DMG_SLASH) then
		v = v / 5
	end*/
	for i=0, num do
		local bone = rag:GetPhysicsObjectNum(i)
		if IsValid(bone) then
			local bp, ba = self:GetBonePosition(rag:TranslatePhysBoneToBone(i))
			if bp and ba then
				bone:SetPos(bp)
				bone:SetAngles(ba)
			end
			-- not sure if this will work:
			bone:SetVelocity(v)
		end
	end
	timer.Simple(30, function()
		if (IsValid(rag)) then
			rag:Remove()
		end
	end)

	self:SetRagdollEntity(rag)
end

function ENT:CrosshairDisable()

end

function ENT:CrosshairEnable()

end

function ENT:DebugInfo()
	MsgN("Name: "..self:GetName())
	MsgN("Class: "..self:GetClass())
	MsgN("Pos: "..tostring(self:GetPos()))
end

function ENT:DetonateTripmines()
	// NPCs probably won't have tripmines, no worries
end

function ENT:DrawViewModel(bool, vm)
	// Don't worry, pretty sure the bot doesn't need a view model ;)
end

function ENT:DrawWorldModel(bool)
	local wep = self:GetActiveWeapon()
	if (IsValid(wep)) then
		wep:SetNoDraw(!bool)
	end
end

function ENT:DropObject()

end

function ENT:EnterVehicle(veh)
	// In the future there should be an ai mod for vehicles we could enable
end

function ENT:EquipSuit()

end

function ENT:ExitVehicle()

end

function ENT:Flashlight(bool)

end

function ENT:Freeze(bool)
	// skip over everything in RunBehaviour()?
	self.m_Frozen = bool
end

function ENT:GetInfo(cVarName)
	return nil
end

function ENT:GetInfoNum(cVarName, default)
	return default or 0
end

function ENT:GetTimeoutSeconds()
	return 0
end

function ENT:GiveAmmo(num, ammoType, displayPopup)
	self.WeaponAmmo[ammoType] = (self.WeaponAmmo[ammoType] or 0) + num
end

function ENT:GodDisable()
	self.m_God = false
end
function ENT:GodEnable()
	self.m_God = true
end

function ENT:IPAddress()
	return "0.0.0.0"
end

function ENT:IsConnected()
	return true
end

function ENT:IsFullyAuthenticated()
	return true
end

function ENT:IsListenServerHost()
	return false
end

function ENT:IsTimingOut()
	return false
end

function ENT:Kick(reason)
	self:Remove()
end

function ENT:Kill(dmginfo)
	self:OnKilled(dmginfo or DamageInfo())
end
function ENT:KillSilent()
	//self:Remove()
	self:OnKilled(dmginfo or DamageInfo())
end

function ENT:Lock()
	self.m_Locked = true
end

function ENT:PacketLoss()
	return 0
end

function ENT:PickupObject(ent)

end

function ENT:PlayStepSound(volume)

end

function ENT:RemoveAllAmmo()
	table.Empty(self.WeaponAmmo)
end

function ENT:RemoveAllItems()
	self:RemoveAllAmmo()
	self:StripWeapons()
end

function ENT:RemoveSuit()

end

function ENT:Say(text, teamOnly)
	local teamOnly = teamOnly or false

	local text = hook.Run("PlayerSay", self, text, teamOnly)
	if (text != "") then
		// Net message to clients
	end
end

function ENT:ScreenFade(flags, color, fadeTime, fadeHold)
end

function ENT:SendHint(name, delay)
end

function ENT:SendLua(lua)
end

function ENT:SetLaggedMovementValue(timescale)
end

function ENT:SetNoTarget(visibility)

end

function ENT:SetObserverMode(observerMode)

end

function ENT:SetupHands(ent)

end

function ENT:SetUserGroup( name )
	self:SetNWString( "UserGroup", name )
end

function ENT:SetViewEntity(viewEntity)
end

function ENT:ShouldDropWeapon(drop)

end

function ENT:Spectate(spectateMode)

end

function ENT:SpectateEntity(ent)

end

function ENT:SprayDecal(sprayOrigin, sprayEndPos)

end

function ENT:SprintDisable()

end

function ENT:SprintEnable()

end

function ENT:StopZooming()

end

function ENT:StripAmmo()
	table.Empty(self.WeaponAmmo)
end

function ENT:StripWeapon(class)

end

function ENT:StripWeapons()
	if (self:GetActiveWeapon()) then
		self:GetActiveWeapon():Remove()
	end
	table.Empty(self.Weapons)
end

function ENT:SuppressHint()

end

function ENT:SwitchToDefaultWeapon()

end

function ENT:TimeConnected()
	return CurTime() - self.SpawnTime
end

function ENT:TraceHullAttack(startPos, endPos, mins, maxs, damage, damageFlags, damageForce, damageAllNPCs)

	// Should return the hit entity
	return nil
end

function ENT:UnLock()
	self.m_Locked = false
end

function ENT:UnSpectate()

end
