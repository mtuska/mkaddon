// RAWR!

function ENT:IsDoor(ent)
	if (!IsValid(ent)) then return false end
	local class = ent:GetClass()
	
	if (class:find("door")||class:find("func_movelinear")) then
		return true
	end
	return false
end

DOOR_STATE_NULL = -1;
DOOR_STATE_CLOSED = 0;
DOOR_STATE_OPENING = 1;
DOOR_STATE_OPEN = 2;
DOOR_STATE_CLOSING = 3;
DOOR_STATE_AJAR = 4;

function ENT:GetDoorState(ent)
	return ent:GetSaveTable().m_eDoorState or DOOR_STATE_NULL;
end

TOGGLE_STATE_NULL = -1;
TOGGLE_STATE_AT_TOP = 0;
TOGGLE_STATE_AT_BOTTOM = 1;
TOGGLE_STATE_GOING_UP = 2;
TOGGLE_STATE_GOING_DOWN = 3;

function ENT:GetToggleState(ent)
	return ent:GetSaveTable().m_toggle_state or TOGGLE_STATE_NULL;
end

function ENT:IsDoorLocked(ent)
	return ent:GetSaveTable().m_bLocked == true
end

function ENT:OpenDoor(ent, delay, bUnlock, bSound)
	if (self:IsDoor(ent)) then
		if (bUnlock) then
			ent:Fire("Unlock", "", delay)
			delay = delay + 0.025

			if (bSound) then
				ent:EmitSound("physics/wood/wood_box_impact_hard3.wav")
			end
		end

		if (ent:GetClass() == "prop_dynamic") then
			ent:Fire("SetAnimation", "open", delay)
			ent:Fire("SetAnimation", "close", delay + 5)
		elseif (string.lower(ent:GetClass()) == "prop_door_rotating") then
			local target = ents.Create("info_target")
				target:SetName(tostring(target))
				target:SetPos(self:GetPos())
				target:Spawn()
			ent:Fire("OpenAwayFrom", tostring(target), delay)

			timer.Simple(delay + 1, function()
				if (IsValid(target)) then
					target:Remove()
				end
			end)
		elseif (string.lower(ent:GetClass()) == "func_door") then
			if (bit.band(player.GetByID(1):GetEyeTrace().Entity:GetSpawnFlags(), 256) || bit.band(player.GetByID(1):GetEyeTrace().Entity:GetSpawnFlags(), 65536)) then
				ent:Fire("Open", "", delay)
			else
				return false // Can't open this door from here
			end
		else
			ent:Fire("Open", "", delay)
		end
	end
	return true
end

function ENT:BreakDoor(ent)
	ent:Extinguish()
	ent:SetNoDraw(true)
	ent:SetNotSolid(true)

	timer.Simple(GetConVar("mk_nb_door_respawntime"):GetInt(), function()
		if IsValid(ent) then
			ent:SetNoDraw(false)
			ent:SetNotSolid(false)
			if IsValid(ent.phys_door) then
				SafeRemoveEntity(ent.phys_door)
				ent.phys_door = nil
			end
		end
	end)
end