// RAWR!

ENT.targetting = ENT.targetting or {}
ENT.targetting.bEnabled = ENT.targetting.bEnabled or false
ENT.targetting.bMemory = ENT.targetting.bMemory or false // Should set enemy to nil when none found in LOS/distance?
ENT.targetting.bPOV = ENT.targetting.bPOV or false // Perform POV checks?
ENT.targetting.bLOS = ENT.targetting.bLOS or false // Perform LOS checks?
ENT.targetting.viewDist = ENT.targetting.viewDist or 1000 // Source units away that the NPC can "see"
ENT.targetting.looseDist = ENT.targetting.looseDist or 2000 // Source units away that the NPC can looses "sight"

// Square the distances, takes less CPU then to perform a square root
ENT.targetting.viewDistSqr = ENT.targetting.viewDist * ENT.targetting.viewDist
ENT.targetting.looseDistSqr = ENT.targetting.looseDist * ENT.targetting.looseDist

ENT.EnemyMemory = {}
ENT.EnemyMemoryVariables = {}
ENT.EnemyMemoryVariables["Position"] = Vector()
ENT.EnemyMemoryVariables["Visibility"] = false
ENT.EnemyMemoryVariables["Distance"] = ENT.targetting.looseDist + 1
ENT.EnemyMemoryVariables["Threat"] = 0

function ENT:IsEnemy(ent)
	return false
end

function ENT:HasEnemy()
	return IsValid(self:GetEnemy())
end

function ENT:CheckEnemy()
	if (self:GetEnemy()) then
		if (IsValid(self:GetEnemy()) && self:IsEnemy(self:GetEnemy())) then
			return true
		end
		self:SetEnemy(nil)
	end
	return false
end

function ENT:TrySetEnemy(ent)
	if (MK.nextbots.targetting.canTarget(self, ent)) then
		self:SetEnemy(ent)
	end
end

function ENT:Int_OnSetEnemy(old, new)
	if (old != new) then
		self.LastEnemy = old
		self.EnemySetTime = CurTime()
		if (new == nil) then
			self:OnLooseEnemy()
		else
			self:OnSetEnemy()
		end
	end
end

function ENT:GetIntimidation()
	return 0
end

function ENT:RemoveEnemyFromMemory(ent)
	self.EnemyMemory[ent] = nil
end

function ENT:UpdateEnemyMemory(ent, data)
	if (self.targetting.bMemory) then
		// Since this function accepts a pos by NPC default, reroute this
		if (!istable(data)) then if (isvector(data)) then self:UpdateEnemyPosition(self, data) end return end
		local _data = table.Copy(self.EnemyMemory[ent] or ENT.EnemyMemoryVariables)
		local __data = table.Merge(table.Copy(_data), data) // copy _data again, so we can send _data as the original
		self.EnemyMemory[ent] = __data
	end
end

// Fired when the targetting update is completed
// By default, we just select the closest stimulus. Overwrite to have better analysis
function ENT:OnTargettingUpdate()
	if (!self:GetEnemyVisibility()) then
		// Pick the next best enemy to target since ours is out of sight
		local closestTarget, closestDist
		for k,v in pairs(self.EnemyMemory) do
			if (!v.Visibility) then continue end
			if (!closestDist || v.Distance <= closestDist) then
				closestDist = v.Distance
				closestTarget = k
			end
		end
		if (closestTarget) then
			self:SetEnemy(closestTarget)
		else
			self:SetEnemy(nil)
		end
	end
end

for k,v in pairs(ENT.EnemyMemoryVariables) do
	ENT["UpdateEnemyMemory"..k] = function(self, ent, var)
		if (self.targetting.bMemory) then
			local data = {}
			data[k] = var
			self:UpdateEnemyMemory(ent, data)
		end
	end
	ENT["GetEnemyMemory"..k] = function(self, ent, var)
		if (self.targetting.bMemory) then
			return self.EnemyMemory[k]
		end
		return nil
	end
	ENT["GetEnemy"..k] = function(self, ent)
		if (self:HasEnemy()) then
			return self["GetEnemyMemory"..k](self, self:GetEnemy())
		end
		return nil
	end
end

function ENT:ClearEnemyMemory()
	self:SetEnemy(nil)
	self:SetThreat(0)
	self.EnemyMemory = {}
end

function ENT:OnLooseEnemy()

end

function ENT:OnSetEnemy()

end