// RAWR!

local Entity = FindMetaTable("Entity") // For overriding default Entity metatable functions

Entity.oldIsNPC = Entity.oldIsNPC or Entity.IsNPC
function Entity:IsNPC()
	if (self.IsMKNPC && self:IsMKNPC()) then
		return true
	end
	return self:oldIsNPC()
end

function ENT:IsMKNextBot()
	return true
end

function ENT:IsOutside(pos)
	local pos = pos or (self:GetPos() + self:OBBCenter())
	local trace = {}
	trace.start = pos
	trace.endpos = trace.start + Vector(0, 0, 32768)
	trace.filter = self;
	local tr = util.TraceLine(trace)

	if (tr.HitSky or tr.HitNoDraw) then return true end

	return false
end

function ENT:InView(pos, dot)
	local dot = dot or -0.5
	local pos = (pos.GetPos and pos:GetPos()) or pos
	local dir = (self:GetPos() - pos)
	dir:Normalize()
	if (dir:Dot(self:GetForward()) < dot) then
		return true
	end
	return false
end



// Player functions

function ENT:AccountID()
	return nil // For bots and in singleplayer, this will return no value.
end

function ENT:AddCleanup(typ, ent)

end

function ENT:AddCount(typ, ent)

end

function ENT:AddVCDSequenceToGestureSlot()

end

function ENT:Alive()
	return true -- if we weren't alive, we wouldn't be here!
end

function ENT:AllowFlashlight(bool)

end

function ENT:AnimResetGestureSlot(numSlot)

end
function ENT:AnimRestartGesture(numSlot, act, loop)
	self:RestartGesture(act)
end
function ENT:AnimRestartMainSequence()
	self:SetCycle(0)
end
function ENT:AnimSetGestureSequence(numSlot, seqId)

end
function ENT:AnimSetGestureWeight(numSlot, weight)

end

function ENT:Armor()
	return self:GetArmor()
end

function ENT:CanUseFlashlight()
	return false
end

function ENT:ChatPrint(message)

end

function ENT:CheckLimit(str)
	return false
end

function ENT:CheckGroup(str)
	return false
end

function ENT:ConCommand(command)
	concommand.Run(self, command, nil, nil)
end

function ENT:Crouching()
	return self:GetCrouching()
end

function ENT:Deaths()
	return self:GetDeaths()
end

function ENT:DoAnimationEvent(data)
	MsgN("Nextbot:DoAnimationEvent: data = "..tostring(data))
	self:DoCustomAnimEvent(PLAYERANIMEVENT_CUSTOM_GESTURE, data)
end
function ENT:DoAttackEvent()
	MsgN("DoAttackEvent")
	self:DoCustomAnimEvent(PLAYERANIMEVENT_ATTACK_PRIMARY, nil)
end
function ENT:DoCustomAnimEvent(event, data)
	MsgN("Nextbot:DoAnimationEvent: event = "..tostring(event)..", data = "..tostring(data))
	hook.Run("DoAnimationEvent", self, event, data)
end
function ENT:DoReloadEvent()
	MsgN("DoReloadEvent")
	self:DoCustomAnimEvent(PLAYERANIMEVENT_RELOAD, nil)
end
function ENT:DoSecondaryAttack()
	MsgN("DoSecondaryAttack")
	self:DoCustomAnimEvent(PLAYERANIMEVENT_ATTACK_SECONDARY, nil)
end

function ENT:FlashlightIsOn()
	return false
end

function ENT:Frags()
	return 0
end

function ENT:GetAimVector()
	//if (self.Door) then
	//	return (self.Door:GetPos() - self:GetPos()):GetNormalized():Angle():Forward()
	/*if (self:GetEnemy() && self:CheckLOS(self:GetEnemy())) then
		return (self:GetEnemy():OBBCenter() - self:GetShootPos()):GetNormalized():Angle():Forward()
	else*/
		return self:GetForward()
	//end
end

function ENT:GetAmmoCount(ammoType)
	return self.WeaponAmmo[ammoType] or 0
end

function ENT:GetClassID()
	return 0
end

function ENT:GetCount(str, minus)

end

function ENT:GetCurrentCommand()
	return nil
end

function ENT:GetDrivingEntity()
	return false
end

function ENT:GetDrivingMode()
	return 0
end

function ENT:GetEyeTrace()
	local TraceData = {}
	TraceData.start		= self:GetEyePos()
	TraceData.endpos	= self:GetEyePos() + (self:GetAimVector() * (4096 * 8))
	TraceData.filter	= {self, self:GetActiveWeapon()}
	TraceData.mask		= MASK_BLOCKLOS_AND_NPCS

	return util.TraceLine( TraceData )
end

function ENT:GetEyeTraceNoCursor()
	return self:GetEyeTrace()
end

function ENT:GetFOV()
	return 50
end

function ENT:GetHands()
	return nil
end

function ENT:GetHoveredWidget()
	return nil
end

function ENT:GetHull()
	return self:GetHullMins(), self:GetHullMaxs()
end

function ENT:GetHullDuck()
	return self:GetHullDuckMins(), self:GetHullDuckMaxs()
end

function ENT:GetLaggedMovementValue()
	return 1
end

function ENT:GetObserverMode()
	return OBS_MODE_NONE
end

function ENT:GetObserverTarget()
	return nil
end

function ENT:GetPData( name, default )
	name = Format( "%s[%s]", self:UniqueID(), name )
	local val = sql.QueryValue( "SELECT value FROM playerpdata WHERE infoid = " .. SQLStr(name) .. " LIMIT 1" )
	if ( val == nil ) then return default end
	return val
end

function ENT:GetPressedWidget()
	return nil
end

function ENT:GetRenderAngles()
	return Angle()
end

function ENT:GetEyePos()
	local bone = self:LookupAttachment("eyes")
	if (bone > 0) then
		return self:GetAttachment(self:LookupAttachment("eyes")).Pos
	end
	return self:GetPos()
end

function ENT:GetEyeAngles()
	local wep = self:GetActiveWeapon()
	if (IsValid(wep)&&wep:LookupAttachment("muzzle") != 0) then
		return wep:GetAttachment(wep:LookupAttachment("muzzle")).Ang
	end
	local bone = self:LookupAttachment("eyes")
	if (bone > 0) then
		return self:GetAttachment(self:LookupAttachment("eyes")).Ang
	end
	return self:GetAngles()
end

function ENT:GetShootPos()
	local shootPos = self:GetAttachment(self:LookupAttachment("eyes")).Pos

	local wep = self:GetActiveWeapon()

	if (IsValid(wep)&&wep:LookupAttachment("muzzle") != 0) then
		shootPos = wep:GetAttachment(wep:LookupAttachment("muzzle")).Pos
	end

	return shootPos
end

function ENT:GetTool(mode)
	return {}
end

function ENT:GetUserGroup()
	return self:GetNWString("UserGroup", "user")
end

function ENT:GetVehicle()
	return nil
end

function ENT:GetViewEntity()
	return nil
end

function ENT:GetViewModel(vm)
	return self:GetActiveWeapon()
end

function ENT:GetWeapon(class)
	if SERVER then
		return self.Weapons[class] or nil
	end
	return nil
end

function ENT:GetWeapons()
	if SERVER then
		return table.Copy(self.Weapons)
	end
	return {}
end

function ENT:HasGodMode()
	return false
end

function ENT:HasWeapon(class)
	if (self.GetWeapon && IsValid(self:GetWeapon(class))) then
		return true
	else
		return false
	end
end

function ENT:InVehicle()
	return false
end

function ENT:IsAdmin()
	if ( self.IsFullyAuthenticated && !self:IsFullyAuthenticated() ) then return false end
	if ( self:IsSuperAdmin() ) then return true end
	if ( self:IsUserGroup("admin") ) then return true end
	return false
end

function ENT:IsBot()
	return true
end

function ENT:IsDrivingEntity()
	return false
end

function ENT:IsFrozen()
	return false
end

function ENT:IsPlayingTaunt()
	return false
end

function ENT:IsSuitEquipped()
	return false
end

function ENT:IsSuperAdmin()
	-- Admin SteamID need to be fully authenticated by Steam!
	if ( self.IsFullyAuthenticated && !self:IsFullyAuthenticated() ) then return false end
	return ( self:IsUserGroup("superadmin") )
end

function ENT:IsTyping()
	return false
end

function ENT:IsUserGroup( name )
	if ( !self:IsValid() ) then return false end
	return ( self:GetNWString( "UserGroup" ) == name )
end

function ENT:IsWorldClicking()
	return false
end

function ENT:KeyDown(key)
	return false
end

function ENT:KeyDownLast(key)
	return false
end

function ENT:KeyPressed(key)
	return false
end

function ENT:KeyReleased(key)
	return false
end

function ENT:LagCompensation(bool)

end

function ENT:LastHitGroup()
	return HITGROUP_GENERIC
end

function ENT:LimitHit(typ)

end

function ENT:MotionSensorPos(bone)
	// Should be safe to return nothing
end

function ENT:Name()
	return self:GetName()
end

function ENT:Nick()
	return self:GetName()
end

function ENT:PhysgunUnfreeze()
	return 0
end

function ENT:Ping()
	return 0
end

function ENT:PrintMessage(typ, msg)

end

function ENT:RemoveAmmo(num, ammoType)
	self.WeaponAmmo[ammoType] = math.Clamp((self.WeaponAmmo[ammoType] or 0) - num, 0, 99999)
end

function ENT:RemovePData( name )
	name = Format( "%s[%s]", self:UniqueID(), name )
	sql.Query( "DELETE FROM playerpdata WHERE infoid = "..SQLStr(name) )
end

function ENT:ResetHull()
	self:SetHull(Vector(-16, -16, 0)*self:GetModelScale(), Vector(16, 16, 72)*self:GetModelScale())
	self:SetHullDuck(Vector(-16, -16, 0)*self:GetModelScale(), Vector(16, 16, 36)*self:GetModelScale())
end

function ENT:SetAmmo(ammoCount, ammoType)
	self.WeaponAmmo[ammoType] = ammoCount
end

function ENT:SetClassID(classID)

end

function ENT:SetDrivingEntity(drivingEntity, drivingMode)

end

function ENT:SetDSP(soundFilter, fastReset)

end

function ENT:SetEyeAngles()

end

function ENT:SetFOV(fov, time)

end

function ENT:SetHands(hands)

end

function ENT:SetHoveredWidget(widget)

end

function ENT:SetHull(hullMins, hullMaxs)
	self:SetHullMins(hullMins)
	self:SetHullMaxs(hullMaxs)
end

function ENT:SetHullDuck(hullMins, hullMaxs)
	self:SetHullDuckMins(hullMins)
	self:SetHullDuckMaxs(hullMaxs)
end

function ENT:SetPData( name, value )
	name = Format( "%s[%s]", self:UniqueID(), name )
	sql.Query( "REPLACE INTO playerpdata ( infoid, value ) VALUES ( "..SQLStr(name)..", "..SQLStr(value).." )" )
end

function ENT:SetPressedWidget(pressedWidget)

end

function ENT:SetRenderAngles(ang)

end

function ENT:SetSuppressPickupNotices(doSuppress)

end

function ENT:StartSprinting()

end

function ENT:StartWalking()

end

function ENT:SteamID()
	return "nextbot"
end

function ENT:SteamID64()
	return "nextbot"
end

function ENT:StopSprinting()

end

function ENT:StopWalking()

end

function ENT:Team()
	return self:GetTeam()
end

function ENT:TranslateWeaponActivity(act)
	local wep = self:GetActiveWeapon()
	if (IsValid(wep)) then
		if (wep.TranslateActivity) then
			if (self.HoldType != wep:GetHoldType() && wep:GetHoldType() != "") then
				wep:SetWeaponHoldType(wep:GetHoldType()) -- the weapon:SetHoldType doesn't update correctly. Reset it
				wep:SetHoldType(wep:GetHoldType()) -- Having weird network issues, since SetWeaponHoldType doesn't network, see if this works...
			end
			return wep:TranslateActivity(act)
		else

		end
	end

	return act
end

function ENT:UnfreezePhysicsObjects()

end

function ENT:UniqueID()
	return "MKBots_"..self:GetClass().."_"..self:EntIndex()
end

function ENT:UniqueIDTable(key)
	return {}
end

function ENT:UserID()
	return 0
end

function ENT:ViewPunch(ang)

end

function ENT:ViewPunchReset(tolerance)
	local tolerance = tolerance or 0

end
