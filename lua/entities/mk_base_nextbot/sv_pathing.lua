// RAWR!

ENT.chaseTolerance = ENT.chaseTolerance or 50

function ENT:GetDestination()
	local area = navmesh.GetNearestNavArea(self:GetPos(), false, 1000)
	if not area then return self:GetPos() end

	local previous = {}

	local i = 0
	while i < 20 or (area:HasAttributes(NAV_MESH_AVOID) and i < 100) do

		if math.random(0,1) != 1 then
			local new = table.RandomSeq(area:GetAdjacentAreas() or {})

			if not new then //nowhere to go
				-- print("Nowhere to go: ", i) 
				break
			end

			if not previous[new] then //we've not been here already.
				previous[new] = true
				area = new
			end

		-- elseif (not area:HasAttributes(NAV_MESH_AVOID)) then
			-- -- print("Random cutout: ", i)
			-- break
		end

		i=i+1
		-- if i==Civs.WanderDist then
			-- print("Full path: ", i) 
		-- end
	end

	return area:GetRandomPoint()
end
/*
function ENT:GetDestination(pos, radius, bForward, bCanUseIndoors)
	self:FindDestination(pos, radius, bForward, bCanUseIndoors)
	while (!self.m_Destination) do
		// We don't have a position yet(and hopefully we're inside a coroutine)
		coroutine.wait(0.5)
	end

	if (self.m_Destination) then
		local _pos = self.m_Destination
		self.m_Destination = nil
		// Find the next point, so that it's available by the time the bot hits the current
		self:FindDestination(pos, radius, bForward, bCanUseIndoors)
		return _pos
	end
	return nil
end

function ENT:FindDestination(pos, radius, bForward, bCanUseIndoors) // Calculate end location of a wander
	if (self.m_FindingDestination) then return false end

	self.m_FindingDestination = true

	local radius = radius or 3000
	local pos = pos or self:GetPos()
	local bCanUseIndoors = bCanUseIndoors or false
	local radiussqr = radius^2
	if (bForward) then
		pos = pos + self:GetForward() * radius // Find preferably forward positions
	end
	MK.async(tostring(self)..".FindDestination", function()
		local areas = {}
		table.Add(areas, MK.nextbots.navareas.outdoors)
		if (bCanUseIndoors) then
			table.Add(areas, MK.nextbots.navareas.indoors)
		end
		local area = nil

		local i = 0
		for _,testarea in RandomPairs(areas) do
			i = i + 1
			local dist = pos:DistToSqr(testarea:GetCenter())
			if (dist <= radiussqr) then
				area = testarea
				break
			end
			print("hai")
			if (i % 100 == 0) then
				coroutine.wait(0.2)
				// See if self is still available when the thread starts back up
				if (!IsValid(self)) then return false end
			end
		end

		if (!area) then
			local rnd = table.Random(areas)
			if (rnd) then
				//ErrorNoHalt("Using a random position")
				self.m_FindingDestination = nil
				self.m_Destination = rnd:GetRandomPoint()
			end
			MsgN(self, " Failed to find new location!")
			self:Remove()
			return
		end
		self.m_FindingDestination = nil
		self.m_Destination = area:GetRandomPoint()
	end)
end
*/
function ENT:FindSpots( tbl )
	local tbl = tbl or {}

	tbl.pos			= tbl.pos			or self:WorldSpaceCenter()
	tbl.radius		= tbl.radius		or 1000
	tbl.stepdown	= tbl.stepdown		or 20
	tbl.stepup		= tbl.stepup		or 20
	tbl.type		= tbl.type			or 'hiding'

	-- Use a path to find the length
	local path = Path( "Follow" )

	-- Find a bunch of areas within this distance
	local areas = navmesh.Find( tbl.pos, tbl.radius, tbl.stepdown, tbl.stepup )

	local found = {}

	-- In each area
	for _, area in pairs( areas ) do
		-- get the spots
		local spots

		if ( tbl.type == 'hiding' ) then spots = area:GetHidingSpots() end

		for k, vec in pairs( spots ) do
			-- Work out the length, and add them to a table
			path:Invalidate()

			self:ComputePath(path, vec, true)

			table.insert( found, { vector = vec, distance = path:GetLength() } )
		end
	end

	return found
end

function ENT:FindSpot( type, options )
	local spots = self:FindSpots( options )
	if ( !spots || #spots == 0 ) then return end

	if ( type == "near" ) then
		table.SortByMember( spots, "distance", true )
		return spots[1].vector
	end

	if ( type == "far" ) then
		table.SortByMember( spots, "distance", false )
		return spots[1].vector
	end

	-- random
	return spots[ math.random( 1, #spots ) ].vector
end

function ENT:ComputePath(path, pos, options, bForce) // Find the path to take to the end location
	if (!isvector(pos)) then error("expected vector got "..type(pos)) end
	if (!path) then error("got invalid path") end
	if (MK.nextbots.m_pathingPerFrame >= MK.nextbots.config.maxPathing && !bForce) then
		MK.nextbots.m_waitingToPath[self] = {pos, options}
		return "busy"
	end
	MK.nextbots.m_pathingPerFrame = MK.nextbots.m_pathingPerFrame + 1

	self.m_pathing = true
	local generator/* = function (area, fromArea, ladder, elevator, length)
		if ( !IsValid( fromArea ) ) then
			// first area in path, no cost
			return 0
		else
			if ( !self.loco:IsAreaTraversable( area ) ) then
				// our locomotor says we can't move here
				return -1
			end

			// compute distance traveled along path so far
			local dist = 0

			if (IsValid(ladder)) then
				dist = ladder:GetLength()
			elseif (length > 0) then
				// optimization to avoid recomputing length
				dist = length
			else
				dist = (area:GetCenter() - fromArea:GetCenter()):GetLength()
			end

			local cost = dist + fromArea:GetCostSoFar()

			return cost
		end
	end*/
	// Avoid using the custom generator, takes time of about 0.05s vs the standard generator at ~0.001
	if (options.customGenerator) then
		generator = function (area, fromArea, ladder, elevator, length)
			if (!IsValid(fromArea)) then
				// first area in path, no cost
				return 0
			else
				if (!self.loco:IsAreaTraversable(area)) then
					// our locomotor says we can't move here
					return -1
				end

				// compute distance traveled along path so far
				local dist = 0

				if (IsValid(ladder)) then
					dist = ladder:GetLength()
				elseif (length > 0) then
					// optimization to avoid recomputing length
					dist = length
				else
					dist = (area:GetCenter() - fromArea:GetCenter()):Length()
				end

				local cost = dist + fromArea:GetCostSoFar()

				local _ents = ents.FindInBox(area:GetCorner(0), area:GetCorner(2) + Vector(0,0,100))
				local doorOpenPenalty = 10
				local doorClosedPenalty = 20
				local breakablePenalty = 20
				for k,v in pairs(_ents) do
					if (v:GetClass():find("door")) then
						if (self:GetDoorState(v) == DOOR_STATE_CLOSED) then
							cost = cost + doorClosedPenalty
						else
							cost = cost + doorOpenPenalty
						end
					elseif (v:GetClass():find("breakable")) then
						cost = cost + breakablePenalty
					end
				end

				local transientPenalty = 1.2
				if area:HasAttributes(NAV_MESH_TRANSIENT) then
					cost = cost + transientPenalty * dist
				end

				local avoidPenalty = 2.5
				local chance = math.Rand(0,1)
				if area:HasAttributes(NAV_MESH_AVOID) then
					if (options.useAvoid || (chance < 0.3)) then
						cost = cost + avoidPenalty * dist
					else
						return -1
					end
				end

				local waterPenalty = 50 // Seriously, please avoid the water unless you HAVE to...
				if (area:IsUnderwater()) then
					cost = cost + waterPenalty * dist
				end

				// check height change
				local deltaZ = fromArea:ComputeAdjacentConnectionHeightChange(area)
				if (deltaZ >= self.loco:GetStepHeight()) then
					if (deltaZ >= self.loco:GetMaxJumpHeight()) then
						// too high to reach
						return -1
					end

					// jumping is slower than flat ground
					local jumpPenalty = 5
					cost = cost + jumpPenalty * dist
				elseif (deltaZ < -self.loco:GetDeathDropHeight()) then
					// too far to drop
					return -1
				end

				return cost
			end
		end
	end
	//local time = SysTime()
	path:Compute(self, pos, generator)
	//print("Time to generate: ", (SysTime() - time))
	self.m_pathing = false
	return "ok"
end

function ENT:StopMovingToPos()
	self.m_AllowedToMove = false
end

local function LOS(a,b,ap,bp)
	return !util.TraceLine({start=ap,endpos=bp,filter={a,b},mask=MASK_VISIBLE}).Hit
end

function ENT:RunAndHide(from, radius)
	self.loco:SetDesiredSpeed(self:GetRunSpeed())
	self.loco:SetAcceleration(500)

	local attacker = self:GetEnemy()

	-- find the furthest away hiding spot

	-- local spots = self:FindSpots( { type = "hiding", radius = 35258046 } )
	local radius = radius or 2000
	local spots = self:FindSpots( { type = "hiding", radius = radius } )
	local pos

	from = from or attacker:GetPos()

	if from && from.DistToSqr then
		//calculate furthest spot from the attacker and us.
		-- local distBetweenUs = self:GetPos():DistToSqr(from)
		pos = spots[1].vector
		local furthest = from:DistToSqr(pos)
		for k,spot in pairs(spots)do
			local vis = LOS(self,attacker,spot.vector,attacker:EyePos()) or LOS(self,attacker,spot.vector+Vector(0,0,64),attacker:EyePos())
			if vis then continue end

			if attacker:VisibleVec(spot.vector + Vector(0,0,64)) then continue end
			local distFromMe = spot.distance*spot.distance
			local distFromThem = from:DistToSqr(spot.vector)

			if distFromThem > furthest then
				-- if (distFromThem > distFromMe) then
					pos = spot.vector
					furthest = distFromThem
				-- end
			end
		end
	else
		pos = spots[1].vector
	end

	-- if the position is valid
	if (pos) then
		local result = self:MoveTo(pos, {stuckCheck = false, useDoors = false})
		if (self:CheckEnemy()/* && self:CheckLOS(self:GetEnemy())*/) then
			self:RunAndHide(self:GetEnemy()) // They can still see us, keep running
		end
	else
		MsgC(Color(255,0,0), "Could not find a place to run and hide!!\n")

		self:ResetSequence(self:LookupSequence("idle_all_cower"))
		coroutine.wait(.1)
	end
end

function ENT:Chase(options)
	options = options or {}
	options.customGenerator = false // too much time
	options.chase = true
	options.forcePath = true // Chase pathing is pretty cheap, we'll let these force through
	options.repath = 0.3
	options.tolerance = options.tolerance or self.chaseTolerance
	options.chaseDist = options.chaseDist or 20
	if (self:GetEnemy():InVehicle()) then
		self:MoveTo(self:GetEnemy():GetVehicle():GetPos(), options)
	else
		local len = -options.chaseDist
		local ang = self:WorldToLocal(self:GetEnemy():GetPos()):Angle()
		local pos = self:GetEnemy():GetPos() + ((ang + self:GetAngles()):Forward() * len)
		self:MoveTo(pos, options)
	end
end

function ENT:PositionUpdate(pos, options)
	if (options.chase) then
		local len = -options.chaseDist
		local ang = self:WorldToLocal(self:GetEnemy():GetPos()):Angle()
		pos = self:GetEnemy():GetPos() + ((ang + self:GetAngles()):Forward() * len)
	end
	return pos
end

function ENT:MoveToByHand(pos, options)
	// Guess the navmesh couldn't get ya there? I'll try...
	// Let's just use some simple behavior and get us to a navmesh

	// We'll remove the Z factor, to help us out some
	pos.z = self:GetPos().z

	local tolerance = options.tolerance ^ 2
	local time = SysTime()
	local i = 0
	while (self.m_AllowedToMove) do
		if (!MK.nextbots.m_waitingToPath[self]) then
			i = i + 1
		end
		if (i == 10) then // Break once we find a real navmesh again
			if (IsValid(navmesh.GetNavArea(self:GetPos(), 8))) then
				break
			end
			i = 0
		end
		if (options.useAttack && GetConVar("mk_nb_attacking"):GetBool() && self:HasEnemy()) then
			self:AttackBehaviour()
		end
		if (GetConVar("mk_nb_movement"):GetBool()) then
			self.loco:Approach(pos, 1)
			if (tolerance > (self:GetPos():DistToSqr(pos))) then
				break
			end
		end
		if (self:HasEnemy()) then
			self:FaceTowardsAndWait(self:GetEnemy():GetPos())
			//self.loco:FaceTowards(self:GetEnemy():GetPos())
		else
			self.loco:FaceTowards(pos)
		end
		if (self.loco:IsStuck()) then
			self:HandleStuck()
			return "stuck"
		end
		if (options.maxage && ((SysTime() - time) > options.maxage)) then return "timeout" end
		coroutine.yield()
	end
	return "ok"
end

function ENT:MoveTo(pos, options)
	if (!pos) then error("No position for move to!") end
	self.m_AllowedToMove = true
	options = options or {}
	if (!tonumber(options.repath) || options.repath == 0) then
		options.repath = nil
	end
	if (options.useDoors && !GetConVar("mk_nb_usedoors"):GetBool()) then
		options.useDoors = false
	end
	options.lookahead = options.lookahead or 300
	options.tolerance = options.tolerance or 25
	options.forcePath = options.forcePath or false

	if (options.chase) then
		// path:Chase has curious issues...
		//path = Path("Chase")
		self.path = Path("Follow")
	else
		self.path = Path("Follow")
	end
	local path = self.path
	path:SetMinLookAheadDistance(options.lookahead)
	path:SetGoalTolerance(options.tolerance)

	if (self:ComputePath(path, pos, options, options.forcePath) != "ok" || !path:IsValid()) then
		ErrorNoHalt("No valid path\n")
		return self:MoveToByHand(pos, options)
		//return "failed"
	end
	self.LastPos = {self:GetPos()}

	while (path:IsValid() and self.m_AllowedToMove) do
		// Provide attack behaviours first
		if (options.useAttack && GetConVar("mk_nb_attacking"):GetBool() && self:HasEnemy()) then
			self:AttackBehaviour()
		end
		if (self:HasEnemy()) then
			self:FaceTowardsAndWait(self:GetEnemy():GetPos())
			//self.loco:FaceTowards(self:GetEnemy():GetPos())
		end

		// If RunTrace is currently taking over, pause our behaviour
		if (self.m_TraceMove) then coroutine.yield() continue end

		if (GetConVar("mk_nb_movement"):GetBool()) then
			if (self.loco:IsUsingLadder()) then
				/*if (!self.m_Ladder) then
					local navarea = navmesh.GetNearestNavArea(self:GetPos(), false, 128)
					self.m_Ladder = navarea:GetLadders()[1]
				end
				print("ladders, damn", self.m_Ladder)
				self.loco:SetGravity(0)
				self.loco:SetVelocity(Vector(0,0,0))
				self:SetPos(self:GetPos() + Vector(0,0, 10))
				path:Update(self)*/
				self:Remove() // need more functions to do ladders, just die
			else
				if (options.chase) then
					// path:Chase has curious issues...
					//path:Chase(self, self:GetEnemy())
					if (!self:HasEnemy()) then break end
					path:Update(self)
				else
					path:Update(self)
				end

				pos = self:PositionUpdate(pos, options)
			end
		end

		-- Draw the path (only visible on listen servers or single player)
		if ((options.draw || GetConVar("nav_edit"):GetBool()) && !game.IsDedicated()) then path:Draw() end
		-- If we're stuck then call the HandleStuck function and abandon
		if (self.loco:IsStuck()) then
			self:HandleStuck()
			return "stuck"
		end
		-- If they set maxage on options then make sure the path is younger than it
		if (options.maxage && (path:GetAge() > options.maxage)) then return "timeout" end
		-- If they set repath then rebuild the path every x seconds
		if (self.m_ForceRepath || (options.repath && !self.m_pathing && (path:GetAge() > options.repath))) then
			self.m_ForceRepath = nil
			self:ComputePath(path, pos, options, true)
		end

		coroutine.yield()
	end
	self.path = nil

	return "ok"
end

function ENT:HandleStuck()
	// Locomotion says we're stuck!
	print(self, ' HandleStuck')

	// TODO: We should try and semi-hide the fixing when visible
	/*local isvisible = false
	for k,v in pairs(player.GetAll()) do
		if (v:Visible(self)) then // Visible does a ray trace... not my fav
			isvisible = true
			break
		end
	end*/
	if (self.path && !self:HasEnemy()) then
		local percent = self.path:GetCursorPosition() / self.path:GetLength()
		local cursor = (percent + .1) * self.path:GetLength()
		self.path:MoveCursorTo(cursor)
		self:SetPos(self.path:GetPositionOnPath(cursor))
		self.loco:ClearStuck()
	else
		local navarea = navmesh.GetNavArea(self:GetPos(), 100)
		local areas = nil
		if (navarea) then
			areas = navarea:GetAdjacentAreas()
		end
		if (areas) then
			local area = table.Random(areas)
			self:SetPos(area:GetRandomPoint())
			self.loco:ClearStuck()
		else
			// You're not worth it, commit sodoku
			self:Remove()
		end
	end
end

function ENT:HandleStuckOnDoor()
	print(self, ' HandleStuckOnDoor')
	self:HandleStuck()
end

function ENT:HandleDoorLocked()
	print(self, ' HandleDoorLocked')
	self:StopMovingToPos() // Get a new path
	self.loco:ClearStuck()
end

function ENT:OnStuck()

end

function ENT:OnUnStuck()

end