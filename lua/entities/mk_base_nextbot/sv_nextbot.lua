// RAWR!

util.AddNetworkString("MK_Nextbots.Ragdoll")

function ENT:Initialize()
	self:MKInitialize() // used to help with the child hierarchy
end

function ENT:MKInitialize()
	MK.nextbots.Add(self) // Add itself to the global nextbot list, so that way it doesn't get lost
	if (MK.nextbots.targetting) then
		if (self.targetting.bEnabled) then
			MK.nextbots.targetting.addPerceiver(self)
		end
		MK.nextbots.targetting.addStimulus(self)
	end
	self.MKBaseClass = scripted_ents.Get("mk_base_nextbot")

	local model, name = table.Random(player_manager.AllValidModels())
	self:SetModel(model)

	self:DrawShadow(false)

	self:SetSolidMask(MASK_NPCSOLID)
	self:SetCollisionGroup(COLLISION_GROUP_NPC)
	self:SetUseType(SIMPLE_USE)
	self:AddEFlags(EFL_DONTBLOCKLOS + EFL_FORCE_CHECK_TRANSMIT)
	self:SetSolid(SOLID_BBOX)
	self:SetHull(Vector(-14, -14, 0)*self:GetModelScale(), Vector(14, 14, 72)*self:GetModelScale())
	self:SetHullDuck(Vector(-14, -14, 0)*self:GetModelScale(), Vector(14, 14, 36)*self:GetModelScale())
	self:SetCollisionBounds(self:GetHull())
	self:SetMoveType(MOVETYPE_CUSTOM)

	self:SetMaxHealth(100)
	self:SetHealth(self:GetMaxHealth())

	self:SetName("MK NextBot Base")

	self:SetCrouching(false)
	self:SetAllowFullRotation(false)
	self:SetAllowWeaponsInVehicle(false)
	self:SetAvoidPlayers(true)
	self:SetCanWalk(true)
	self:SetCanZoom(false)
	self:SetNoCollideWithTeammates(false)
    self:SetFindStance(true) // Use RunTrace to crouch/jump/etc

	self:SetArmor(0)
	self:SetCrouchedWalkSpeed(0)
	self:SetDeaths(0)
	self:SetFrags(0)
	self:SetJumpPower(200)
	self:SetMaxSpeed(500)
	self:SetRunSpeed(500)
	self:SetStepSize(18)
	self:SetTeam(0)
	self:SetWalkSpeed(100)

	self:SetDuckSpeed(0)
	self:SetUnDuckSpeed(0)

	self:SetPlayerColor(Vector(1,1,1))
	self:SetCurrentViewOffset(Vector(0,0,0))
	self:SetViewOffset(Vector(0,0,0))
	self:SetViewOffsetDucked(Vector(0,0,0))
	self:SetWeaponColor(Vector(1,1,1))

	self:SetViewPunchAngles(Angle(0,0,0))

	self.SpawnTime = CurTime()
	self.NextContact = CurTime()
	self.NextExcuseMe = CurTime()
	self.SpawnPos = self:GetPos()
	self.SpawnRot = self:GetAngles()
	self.Weapons = {}
	self.WeaponAmmo = {}
	self.LastPos = {self:GetPos()}
	self.m_AllowedToMove = true
	self.m_UseAvoid = false // Should we use avoid navareas?
	timer.Simple(0, function() // Next frame call Post
		if (IsValid(self)) then
			self:MKInitialize_Post()
		end
	end)
end

function ENT:MKInitialize_Post()
	self:SetGender(util.SharedRandom(self:EntIndex(), 0, 1) < .5 and "male" or "female")
	self:SetHull(Vector(-14, -14, 0)*self:GetModelScale(), Vector(14, 14, 72)*self:GetModelScale())
	self:SetHullDuck(Vector(-14, -14, 0)*self:GetModelScale(), Vector(14, 14, 36)*self:GetModelScale())
	self:SetCollisionBounds(self:GetHull())
end

function ENT:Think()
	//print(self.loco:IsUsingLadder())
end

function ENT:BehaveStart()
	self.BehaveThread = coroutine.create( function() self:RunBehaviour() end )
	self.TraceThread = coroutine.create( function() self:RunTrace() end )
end

function ENT:BehaveUpdate( fInterval )
	//local time = SysTime()
	if (GetConVar("mk_nb_locked"):GetBool()) then return end
	if (GetConVar("ai_disabled"):GetBool()) then return end
	if (self.m_Locked) then return end
	if (self.m_Frozen) then return end

	if (!self.BehaveThread) then return end

	--
	-- Give a silent warning to developers if RunBehaviour has returned
	--
	if ( coroutine.status( self.BehaveThread ) == "dead" ) then
		self.BehaveThread = nil
		Msg( self, " Warning: ENT:RunBehaviour() has finished executing\n" )

		return
	end

	--
	-- Continue RunBehaviour's execution
	--
	local ok, message = coroutine.resume( self.BehaveThread )
	if ( ok == false ) then
		self.BehaveThread = nil
		ErrorNoHalt( self, " Error: ", message, "\n" )
	end

	if ( !self.TraceThread ) then return end

	if ( coroutine.status( self.TraceThread ) == "dead" ) then
		self.TraceThread = nil
		Msg( self, " Warning: ENT:RunTrace() has finished executing\n" )

		return
	end

	local ok, message = coroutine.resume( self.TraceThread )
	if ( ok == false ) then
		self.TraceThread = nil
		ErrorNoHalt( self, " Error: ", message, "\n" )
	end
	//print(self, " took "..(SysTime() - time).." for BehaveUpdate")
end

function ENT:RunBehaviour()
	self:Lock()
	error("Child nextbot should override ENT:RunBehaviour()")
end

function ENT:AttackBehaviour()
	if (IsValid(self:GetActiveWeapon())) then
		self:FirePrimary() // MK Nextbot function that corrects weapons for nextbot use
	end
end

function ENT:RunTrace()
	while(true) do
		if (self.m_NavAreaCrouch) then coroutine.wait(0.5) continue end
		if (self.loco:IsClimbingOrJumping()) then coroutine.wait(0.5) continue end
		local navarea = navmesh.GetNearestNavArea(self:GetPos(), false, 128)
		local startpos = self:GetPos()
		local dir = self:GetAimVector()
		local mins, maxs = self:GetHull()
		local dmins, dmaxs = self:GetHullDuck()
		local len = 20
		mins.z = mins.z + dmaxs.z

		local trhull = util.TraceHull( {
			start = startpos,
			endpos = startpos + dir * len,
			maxs = maxs,
			mins = mins,
			filter = self
		} )
		self.m_Trace = trhull

		if (trhull.Entity && IsValid(trhull.Entity)) then
			local ent = trhull.Entity
			if (self:IsDoor(ent)) then
				self:DoorBehaviour(ent)
				coroutine.wait(0.2)
				continue
			elseif (ent:GetClass():find("breakable")) then
				self:BreakableBehaviour(ent)
				coroutine.wait(0.2)
				continue
			end
		end
		self.m_Door = nil

		if (self:GetFindStance()) then
			dmins.z = dmins.z + self:GetStepSize()

			local trduck = util.TraceHull( {
				start = startpos,
				endpos = startpos + dir * len,
				maxs = dmaxs,
				mins = dmins,
				filter = self
			} )

			if (navarea:HasAttributes(NAV_MESH_JUMP)) then
				self:Jump()
			elseif (trhull.Hit && !trduck.Hit) then
				self:Crouch()
			elseif (self:Crouching()) then
				self:UnCrouch()
			elseif (!trhull.Hit && trduck.Hit && (!trduck.Entity:IsWorld() || !navarea:HasAttributes(NAV_MESH_STAIRS))) then
				self:Jump()
			elseif (trhull.Hit && trduck.Hit && !navarea:HasAttributes(NAV_MESH_STAIRS)) then
				mins.z = mins.z + self:GetStepSize()

				local trcrjump = util.TraceHull( {
					start = startpos,
					endpos = startpos + dir * len,
					maxs = maxs,
					mins = mins,
					filter = self
				} )

				if (!trcrjump.Hit && (!trduck.Entity:IsWorld() || !navarea:HasAttributes(NAV_MESH_STAIRS))) then
					self:Jump()
					self:Crouch()
				end
			end
		end
		coroutine.wait(0.1)
	end
end

function ENT:DoorBehaviour(ent)
	local state = self:GetDoorState(ent)
	if (self.m_Door && self.m_Door == ent) then
		if (self:IsDoorLocked(ent)) then
			// idk how, but the door is locked now
			self:HandleDoorLocked()

			return "stuck"
		elseif (state == DOOR_STATE_NULL) then
			// Door doesn't provide standard states!
			local state = self:GetToggleState(ent)
			if (state == TOGGLE_STATE_NULL) then
				//idk what else to do for you
				self:HandleStuckOnDoor()

				return "stuck"
			elseif (state == TOGGLE_STATE_GOING_UP || state == TOGGLE_STATE_GOING_DOWN) then
				self.m_TraceMove = true
				coroutine.wait(0.5)
				self.m_TraceMove = false
			end
		elseif (state == DOOR_STATE_CLOSED) then
			self:HandleStuckOnDoor()

			return "stuck"
		elseif (state == DOOR_STATE_CLOSING) then
			// Boo! We need to step back maybe or something... idk yet
			self.m_TraceMove = true
			self.loco:ClearStuck()
			local pos = self:GetPos()-(self:GetForward()*50) // Move them back, the door is probably opening toward us
			while(self:GetPos():DistToSqr(pos) > 200 || self:GetDoorState(ent) == DOOR_STATE_CLOSING) do
				self.loco:Approach(pos, 1)
				coroutine.yield()
			end
			self.m_ForceRepath = true
			self.m_TraceMove = false
		elseif (state == DOOR_STATE_OPENING && CurTime() >= (self.m_DoorStart + 1)) then
			// Boo! We need to step back maybe or something... idk yet
			self.m_TraceMove = true
			self.loco:ClearStuck()
			local pos = self:GetPos()-(self:GetForward()*50) // Move them back, the door is probably opening toward us
			while(self:GetPos():DistToSqr(pos) > 200 || self:GetDoorState(ent) == DOOR_STATE_OPENING) do
				self.loco:Approach(pos, 1)
				coroutine.yield()
			end
			self.m_ForceRepath = true
			self.m_TraceMove = false
		end
	else
		if (state == DOOR_STATE_CLOSED || state == DOOR_STATE_NULL) then
			if (!self:IsDoorLocked(ent)) then
				local res = self:OpenDoor(ent, 0, false, false, self:GetPos())
				if (!res) then // If opendoor says false, means we can't interact with the door directly, just fuck it
					self:HandleDoorLocked()

					return "stuck"
				end
			end
			self.m_TraceMove = true // Disable MoveTo
			coroutine.wait(0.2) // Just a small delay to see if the door is opening
			if (self:IsDoorLocked(ent)) then
				// Well fuck you too door!
				// This door is locked, lets move on with our lives
				self:HandleDoorLocked()

				return "stuck"
			else
				//coroutine.wait(0.5)
				self.m_TraceMove = false
			end
		end
		self.m_Door = ent
		self.m_DoorStart = CurTime()
	end
	return "ok"
end

function ENT:BreakableBehaviour(ent)
	ent:Fire("Break", "", 0)
end

function ENT:OnLeaveGround()
	self.m_InAir = true
	self.m_InAirStart = CurTime()
end

function ENT:OnLandOnGround()
	self.m_InAir = false
	self.m_Jumping = false
end

function ENT:OnNavAreaChanged(old, new)
    /*if new:HasAttributes( NAV_MESH_JUMP ) then
        self:Jump()
    end*/
    if (!self:Crouching() and new:HasAttributes(NAV_MESH_CROUCH)) then
		self.m_NavAreaCrouch = true
		self:Crouch()
	end
    if (self:Crouching() and !new:HasAttributes(NAV_MESH_CROUCH)) then
		self.m_NavAreaCrouch = false
		self:UnCrouch()
	end
end

function ENT:IsJumping()
	return self.m_Jumping
end

function ENT:Jump()
	if (self.m_Jumping) then return end

	self.loco:Jump()
	self.m_Jumping = true
	self.m_bFirstJumpFrame = true
end

function ENT:Crouch()
	self:SetCollisionBounds(self:GetHullDuck())
	self.m_Crouching = true
	self:SetCrouching(true)
end

function ENT:UnCrouch()
	self:SetCollisionBounds(self:GetHull())
	self.m_Crouching = false
	self:SetCrouching(false)
end

function ENT:FaceTowardsAndWait(pos)
	self.loco:FaceTowards(pos)
	while (!self:InView(pos, -0.8)) do
		self.loco:FaceTowards(pos)
		coroutine.yield()
	end
end

function ENT:OnContact(other)
	if (self.NextContact > CurTime()) then
		return
	else
		self.NextContact = CurTime() + 1
	end
	if IsValid(other) and other:IsVehicle() then
		self:OnVehicleContact(other)
	end
end

function ENT:OnVehicleContact(other)
	local speed = other:GetVelocity():Length()
	if speed > MK.nextbots.config.vehKillSpeed then
		local attk = other:GetDriver()
		local dmg = DamageInfo()
		dmg:SetDamageType(DMG_VEHICLE)
		dmg:SetDamage(self:GetMaxHealth())
		dmg:SetAttacker(attk:IsValid() and attk or other)
		dmg:SetInflictor(other)
		dmg:SetDamageForce(other:GetVelocity()*20)
		dmg:SetDamagePosition(self:GetPos())
		self:TakeDamageInfo(dmg)
	elseif speed > MK.nextbots.config.vehKillSpeed/4 then
		local attk = other:GetDriver()
		local dmg = DamageInfo()
		dmg:SetDamageType(DMG_VEHICLE)
		dmg:SetDamage(self:GetMaxHealth()*(speed/MK.nextbots.config.vehKillSpeed))
		dmg:SetAttacker(attk:IsValid() and attk or other)
		dmg:SetInflictor(other)
		dmg:SetDamageForce(other:GetVelocity()*20)
		dmg:SetDamagePosition(self:GetPos())
		self:TakeDamageInfo(dmg)
	//elseif speed > 20 and self:Health() > 0 then
		//self:Spook(1)
	end
end

function ENT:OnIgnite()

end

hook.Add("EntityTakeDamage", "MK_Nextbots.Hitgroups.EntityTakeDamage", function(target, dmginfo)
	if (target.MKNextbot) then
		local pos = dmginfo:GetDamagePosition()
		local hitgroup = 0
		local dist_to_hitgroups = {}

		for hitbox,hitgroup in pairs(target.HitBoxToHitGroup) do
			local bone = target:GetHitBoxBone(hitbox, 0)
			if bone then
				local bonepos, boneang = target:GetBonePosition(bone)

				table.insert(dist_to_hitgroups, {bonename = target:GetBoneName(bone), hitgroup = hitgroup, dist = pos:Distance(bonepos)})
			end
		end

		table.SortByMember(dist_to_hitgroups, "dist", true)
		hitgroup = dist_to_hitgroups[1].hitgroup
		dmginfo:ScaleDamage(GetConVar("mk_nb_scaledamage"):GetFloat())

		hook.Run("ScaleNextbotDamage", target, hitgroup, dmginfo)
	end
end)

// Use gamemode here, so that a hook can easily override it
local _gm = GM or GAMEMODE
function _gm:ScaleNextbotDamage(nextbot, hitgroup, dmginfo)
	if (hitgroup == HITGROUP_HEAD) then
		local s = 2
		local wep = util.WeaponFromDamage(dmginfo)
		if (IsValid(wep) && wep.GetHeadshotMultiplier) then
			s = wep:GetHeadshotMultiplier(nextbot, dmginfo)
		end
		dmginfo:ScaleDamage(s)
	elseif (hitgroup == HITGROUP_LEFTARM || hitgroup == HITGROUP_RIGHTARM || hitgroup == HITGROUP_LEFTLEG || hitgroup == HITGROUP_RIGHTLEG) then
      	dmginfo:ScaleDamage(0.25)
	elseif (hitgroup == HITGROUP_GEAR) then
		dmginfo:ScaleDamage(0)
	end
end

function ENT:OnInjured(dmginfo)
	self:AddGestureSequence(self:LookupSequence("flinch_gesture"))
end

function ENT:OnOtherKilled(victim, dmginfo)

end

function ENT:OnKilled( dmginfo )
	//hook.Run( "OnNPCKilled", self, dmginfo:GetAttacker(), dmginfo:GetInflictor() )
	hook.Run( "OnNextbotKilled", self, dmginfo:GetAttacker(), dmginfo:GetInflictor() )
	for k,v in pairs(self.Weapons) do
		if (IsValid(v)) then
			v:Remove()
		end
	end

	local attacker = dmginfo:GetAttacker()
	if (IsValid(attacker) && attacker:IsPlayer()) then
		attacker:AddFrags(1)
	end
	net.Start("MK_Nextbots.Ragdoll")
		net.WriteEntity(self)
		net.WriteVector(dmginfo:GetDamageForce())
	net.Broadcast()
	if (IsValid(self)) then
		self:Remove()
	end
end

function ENT:Use(activator, caller, useType, value)

end

// https://maurits.tv/data/garrysmod/wiki/wiki.garrysmod.com/index8f77.html
function ENT:VoiceSound(tbl, overlap, force)
	if ((self.VoiceTime or 0) > CurTime() && !force && !overlap) then return end
	if ((self.VoiceTime or 0) > CurTime() && !overlap) then
		self:StopSound(self.LastSoundFile)
	end
	self.VoiceTime = CurTime() + 1.5

	local tbl = istable(tbl) and tbl or {tbl}

	local snd = table.Random(tbl)
	self.LastSoundFile = snd
	self:EmitSound(snd, SNDLVL_TALKING, 100, 1, CHAN_VOICE)
end