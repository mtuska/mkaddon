// RAWR!

// Our clientside version of Nextbot Animation Functions
ENT.m_fAnimSpeed = 1
ENT.m_Activity = ACT_INVALID

function ENT:StartActivity(act)
    if (self.m_Activity == act) then return end
    self.m_Activity = act
    if (self:SelectWeightedSequence(act)) then
        self:SetSequence(self:SelectWeightedSequence(act))
    end
end

function ENT:GetActivity()
    return self:GetSequenceActivity(self:GetSequence())
end