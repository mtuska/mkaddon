// RAWR!

AddCSLuaFile()

ENT.Base 			= "base_anim"
ENT.Type			= "nextbot"
ENT.PrintName		= "Madkiller Max's Nextbot Base"
ENT.Author			= "Madkiller Max"
ENT.Contact			= "Message someone who cares"
ENT.Purpose			= "To make creating nextbots more efficent and optimized"
ENT.Instructions	= "Create a new nextbot and override it's RunBehavior! Do whatever you want, I don't care."
ENT.RenderGroup		= RENDERGROUP_OPAQUE
ENT.ScriptedEntityType = "npc"
ENT.MKNextbot = true

ENT.HitBoxToHitGroup = {
	[0] = HITGROUP_HEAD,
	[16] = HITGROUP_CHEST,
	[15] = HITGROUP_STOMACH,
	[5] = HITGROUP_RIGHTARM,
	[2] = HITGROUP_LEFTARM,
	[12] = HITGROUP_RIGHTLEG,
	[8] = HITGROUP_LEFTLEG
}

function ENT:SetupDataTables()
	self:MKSetupDataTables()
end

function ENT:MKSetupDataTables()
	self:InstallDataTable()
	// String has a max of 4 slots
	self:NetworkVar("String", 0, "Name")
	self:NetworkVar("String", 1, "Gender")

	// The rest have max of 64 slots
	self:NetworkVar("Entity", 0, "Enemy")
	self:NetworkVarNotify("Enemy", self.OnVarChanged)
	self:NetworkVar("Entity", 1, "ActiveWeapon")
	self:NetworkVar("Entity", 2, "RagdollEntity")

	self:NetworkVar("Bool", 0, "SightOfEnemy")
	self:NetworkVar("Bool", 1, "Crouching")
	self:NetworkVar("Bool", 2, "AllowFullRotation")
	self:NetworkVar("Bool", 3, "AllowWeaponsInVehicle")
	self:NetworkVar("Bool", 4, "AvoidPlayers")
	self:NetworkVar("Bool", 5, "CanWalk")
	self:NetworkVar("Bool", 6, "CanZoom")
	self:NetworkVar("Bool", 7, "NoCollideWithTeammates")
	self:NetworkVar("Bool", 8, "FindStance")
	self:NetworkVar("Bool", 9, "Arrested") // DarkRP
	self:NetworkVar("Bool", 10, "Wanted") // DarkRP

	self:NetworkVar("Int", 0, "Armor")
	self:NetworkVar("Int", 1, "CrouchedWalkSpeed")
	self:NetworkVar("Int", 2, "Deaths")
	self:NetworkVar("Int", 3, "Frags")
	self:NetworkVar("Int", 4, "JumpPower")
	self:NetworkVarNotify("JumpPower", self.OnVarChanged)
	self:NetworkVar("Int", 5, "MaxSpeed")
	self:NetworkVar("Int", 6, "RunSpeed")
	self:NetworkVar("Int", 7, "StepSize")
	self:NetworkVarNotify("StepSize", self.OnVarChanged)
	self:NetworkVar("Int", 8, "Team")
	self:NetworkVar("Int", 9, "WalkSpeed")

	self:NetworkVar("Float", 0, "DuckSpeed")
	self:NetworkVar("Float", 1, "UnDuckSpeed")
	self:NetworkVar("Float", 2, "Threat")

	self:NetworkVar("Vector", 0, "PlayerColor")
	self:NetworkVar("Vector", 1, "CurrentViewOffset")
	self:NetworkVar("Vector", 2, "ViewOffset")
	self:NetworkVar("Vector", 3, "ViewOffsetDucked")
	self:NetworkVar("Vector", 4, "WeaponColor")
	self:NetworkVarNotify("WeaponColor", self.OnVarChanged)
	self:NetworkVar("Vector", 5, "HullMins")
	self:NetworkVar("Vector", 6, "HullMaxs")
	self:NetworkVar("Vector", 7, "HullDuckMins")
	self:NetworkVar("Vector", 8, "HullDuckMaxs")

	self:NetworkVar("Angle", 0, "ViewPunchAngles")
end

function ENT:OnVarChanged(name, old, new)
	if (name == "StepSize") then
		if (SERVER) then
			self.loco:SetStepHeight(new)
		end
	elseif (name == "WeaponColor") then
	elseif (name == "JumpPower") then
		if (SERVER) then
			self.loco:SetJumpHeight(math.Round(new/3.448)) // JumpPower is some magic number in velocity. JumpHeight is a distance. Default player jump power is 200, and default jump height is 58(200/3.4=58)
		end
	elseif (name == "Enemy") then
		if (SERVER) then
			self:Int_OnSetEnemy(old, new) // Internal function
		end
	end
end

function ENT:OnReloaded()
	MKNextbotOverrideEntityMetatable()
end

// Fuck you, why can't I replace these locally!?!?
hook.Add("InitPostEntity", "MK_Nextbots.InitPostEntity", function()
	MKNextbotOverrideEntityMetatable()
end)

function MKNextbotOverrideEntityMetatable()
	local meta = debug.getregistry().Entity
	meta._EyePos = meta._EyePos or meta.EyePos
	function meta:EyePos()
		if (IsValid(self) && self.MKNextbot) then
			return self:GetEyePos()
		end
		return self:_EyePos()
	end
	meta._EyeAngles = meta._EyeAngles or meta.EyeAngles
	function meta:EyeAngles()
		if (IsValid(self) && self.MKNextbot) then
			return self:GetEyeAngles()
		end
		return self:_EyeAngles()
	end
end

if (SERVER) then
	AddCSLuaFile("sh_functions.lua")
	AddCSLuaFile("cl_functions.lua")
	AddCSLuaFile("cl_nextbot.lua")
	AddCSLuaFile("sh_animations.lua")
	AddCSLuaFile("cl_animations.lua")
	AddCSLuaFile("cl_enemy.lua")
	AddCSLuaFile("sh_mk.lua")
	AddCSLuaFile("sh_darkrp.lua")

	include("sh_functions.lua")
	include("sh_mk.lua")

	include("sv_functions.lua")

	include("sv_nextbot.lua")
	include("sh_animations.lua")
	include("sv_animations.lua")

	include("sv_pathing.lua")
	include("sv_weapons.lua")
	include("sv_weapon_overrides.lua")
	include("sv_enemy.lua")
	include("sv_doors.lua")

	include("sh_darkrp.lua")
else
	include("sh_functions.lua")
	include("sh_mk.lua")
	include("sh_darkrp.lua")
	include("sh_animations.lua")

	include("cl_functions.lua")

	include("cl_nextbot.lua")
	include("cl_animations.lua")
	include("cl_enemy.lua")
end
