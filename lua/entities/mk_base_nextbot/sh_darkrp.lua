// RAWR!

// Collection of DarkRP player & and DarkRP helper functions
ENT.DarkRPVars = {}
if (!DarkRP) then return end

function ENT:Mug(attacker)
	if !IsValid(attacker) then return end
	if DarkRP and DarkRP.createMoneyBag and self:getDarkRPVar("money", 0) > 0 then
		
		if (attacker:isCP()) then return end
		
		if (attacker.LastCivMug and CurTime() - attacker.LastCivMug < (MK.civs.config.timeBetweenMugs or 0)) then return end
		self.Mugged = true
		
		local result = hook.Run("MK_Civs.Mug", self, attacker, self:getDarkRPVar("money", 0))
		
		if result == false then return end
		
		local amount = isnumber(result) and result or self:getDarkRPVar("money", 0)
		
		self:ThrowMoney(amount)
		attacker.LastCivMug = CurTime()
	end
end

function ENT:ThrowMoney(amount)
	amount = amount or self:getDarkRPVar("money", 0)
	if DarkRP and DarkRP.createMoneyBag and self:getDarkRPVar("money", 0) > 0 then
		local bag = DarkRP.createMoneyBag(self:GetPos()+Vector(0,0,25), amount)
		bag:SetOwner(self)
		bag:GetPhysicsObject():ApplyForceCenter(self.loco:GetGroundMotionVector()*2)
		
		self:addMoney(-amount)
	end
end

function ENT:addMoney(amount)
	if not amount then return false end
    local total = self:getDarkRPVar("money", 0) + math.floor(amount)

    self:setDarkRPVar("money", total)
end

function ENT:wanted(cp, reason, time)
	if (self:isCP()) then return end
	self:SetWanted(true)
	
	local actorNick = IsValid(cp) and cp:Nick() or DarkRP.getPhrase("disconnected_player")
    local centerMessage = DarkRP.getPhrase("wanted_by_police", self:Nick(), reason, actorNick)
    local printMessage = DarkRP.getPhrase("wanted_by_police_print", actorNick, self:Nick(), reason)

	for _, ply in ipairs(player.GetAll()) do
        ply:PrintMessage(HUD_PRINTCENTER, centerMessage)
        ply:PrintMessage(HUD_PRINTCONSOLE, printMessage)
    end
end

function ENT:unWanted(cp)
	self:SetWanted(true)
	
	local expiredMessage = IsValid(cp) and DarkRP.getPhrase("wanted_revoked", self:Nick(), cp:Nick() or "") or DarkRP.getPhrase("wanted_expired", self:Nick())

    for _, ply in ipairs(player.GetAll()) do
        ply:PrintMessage(HUD_PRINTCENTER, expiredMessage)
        ply:PrintMessage(HUD_PRINTCONSOLE, expiredMessage)
    end
end

function ENT:onArrestStickUsed(user)
	self:arrest(nil, user)
end

function ENT:onUnArrestStickUsed(user)
	self:unArrest(user)
end

function ENT:arrest(time, arrester)
	if (self:isCP()) then return end
	time = time or GAMEMODE.Config.jailtimer or 120
	if (DarkRP.retrieveJailPos() != nil) then
		self:SetPos(DarkRP.retrieveJailPos())
		if (self:isArrested()) then
			return
		end
		self:SetArrested(true)
		self:SetWanted(false)
		self.UseDoors = false
		
		local phrase = DarkRP.getPhrase("hes_arrested", self:Nick(), time)
		for _, v in ipairs(player.GetAll()) do
			v:PrintMessage(HUD_PRINTCENTER, phrase)
		end
		
		timer.Create("Nextbot"..self:EntIndex(), time, 1, function()
			self:SetPos(MK.civs.GetLocation())
			self:unArrest()
		end)
	end
end

function ENT:unArrest(unarrester)
	if (!self:isArrested()) then
		return
	end
	self:SetArrested(false)
	self.UseDoors = true
	
	DarkRP.notifyAll(0, 4, DarkRP.getPhrase("hes_unarrested", self:Nick()))
	timer.Remove("Nextbot"..self:EntIndex())
end

function ENT:isArrested()
	return self:GetArrested()
end

function ENT:isMayor()
	return false
end

function ENT:isWanted()
    return self:GetWanted()
end

function ENT:isCP()
    return false
end

function ENT:isChief()
    return false
end

function ENT:getDarkRPVar(var, default)
	self.DarkRPVars = self.DarkRPVars or {}
	return self.DarkRPVars[var] or default
end

function ENT:setDarkRPVar(var, value)
	if not IsValid(self) then return end

    if value == nil then return self:removeDarkRPVar(var) end
    
    self.DarkRPVars = self.DarkRPVars or {}
    self.DarkRPVars[var] = value
end

function ENT:removeDarkRPVar(var)
    self.DarkRPVars = self.DarkRPVars or {}
    self.DarkRPVars[var] = nil
end

function ENT:getEyeSightHitEntity(searchDistance, hitDistance, filter)
    searchDistance = searchDistance or 100
    hitDistance = (hitDistance or 15) * (hitDistance or 15)

    filter = filter or function(p) return p:IsPlayer() and p ~= self end

    self:LagCompensation(true)

    local shootPos = self:GetShootPos()
    local entities = ents.FindInSphere(shootPos, searchDistance)
    local aimvec = self:GetAimVector()

    local smallestDistance = math.huge
    local foundEnt

    for k, ent in pairs(entities) do
        if not IsValid(ent) or filter(ent) == false then continue end

        local center = ent:GetPos()

        -- project the center vector on the aim vector
        local projected = shootPos + (center - shootPos):Dot(aimvec) * aimvec

        if aimvec:Dot((projected - shootPos):GetNormalized()) < 0 then continue end

        -- the point on the model that has the smallest distance to your line of sight
        local nearestPoint = ent:NearestPoint(projected)
        local distance = nearestPoint:DistToSqr(projected)

        if distance < smallestDistance then
            local trace = {
                start = self:GetShootPos(),
                endpos = nearestPoint,
                filter = {self, ent}
            }
            local traceLine = util.TraceLine(trace)
            if traceLine.Hit then continue end

            smallestDistance = distance
            foundEnt = ent
        end
    end

    self:LagCompensation(false)

    if smallestDistance < hitDistance then
        return foundEnt, math.sqrt(smallestDistance)
    end

    return nil
end

function ENT:SteamName()
	return self:GetName()
end

// This is the cheaty way to enable DarkRP's stick_base weaponry
function ENT:onStunStickUsed(owner)
	self.loco:SetVelocity((self:GetPos() - owner:GetPos()) * 7)

	owner:EmitSound(owner:GetActiveWeapon().FleshHit[math.random(#owner:GetActiveWeapon().FleshHit)])
end

function ENT:onArrestStickUsed(owner)
	local canArrest, message = hook.Call("canArrest", DarkRP.hooks, owner, self)
    if not canArrest then
        if message then DarkRP.notify(owner, 1, 5, message) end
        return
    end

    self:arrest(nil, owner)
    DarkRP.notify(self, 0, 20, DarkRP.getPhrase("youre_arrested_by", owner:Nick()))

    if owner.SteamName then
        DarkRP.log(owner:Nick() .. " (" .. owner:SteamID() .. ") arrested " .. self:Nick(), Color(0, 255, 255))
    end
end

function ENT:onUnArrestStickUsed(owner)
	local canUnarrest, message = hook.Call("canUnarrest", hookCanUnarrest, owner, self)
    if not canUnarrest then
        if message then DarkRP.notify(owner, 1, 5, message) end
        return
    end

    self:unArrest(owner)
    DarkRP.notify(self, 0, 4, DarkRP.getPhrase("youre_unarrested_by", owner:Nick()))

    if owner.SteamName then
        DarkRP.log(owner:Nick() .. " (" .. owner:SteamID() .. ") unarrested " .. self:Nick(), Color(0, 255, 255))
    end
end