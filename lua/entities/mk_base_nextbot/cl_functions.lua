// RAWR!

net.Receive("MKNextBot_ColorRagdoll", function(len)
	local rag = net.ReadEntity()
	local color = net.ReadVector()
	rag.GetPlayerColor = function() return color end
end)

// Player functions

function ENT:AddPlayerOption( name, timeout, votecallback, drawcallback )

end

function ENT:GetFriendStatus()
	return "none"
end

function ENT:DrawName()
	return true
end

function ENT:GetPlayerInfo()
	local info = {}
	info["friendname"] = {}
	info["customfiles"] = {}
	info["fakeplayer"] = true
	info["filesdownloaded"] = 0
	info["friendid"] = 0
	info["name"] = self:GetName()
	info["userid"] = self:UserID()
	return info
end

function ENT:IsMuted()
	return false
end

function ENT:IsSpeaking()
	return false
end

function ENT:IsVoiceAudible()
	return true
end

function ENT:SetMuted(mute)

end

function ENT:ShouldDrawLocalPlayer()
	return false
end

function ENT:ShowProfile()

end

function ENT:VoiceVolume()
	return 1
end
