// RAWR!

// Remember, self in these function is actually a swep

ENT.weaponOverrides = {}
ENT.weaponOverrides["cw2.0"] = {}
ENT.weaponOverrides["cw2.0"].isNearWall = function(self)
	return false
end

ENT.weaponOverrides["cw2.0"].FireBullet = function(self, damage, cone, clumpSpread, bullets)
    local sp = self.Owner:GetShootPos()
	if self.Owner:Crouching() then
		cone = cone * 0.85
	end
	
	local Dir = (self.Owner:EyeAngles() + self.Owner:GetViewPunchAngles() + Angle(math.Rand(-cone, cone), math.Rand(-cone, cone), 0) * 25):Forward()
	clumpSpread = clumpSpread or self.ClumpSpread
	
    local bul = {}
	CustomizableWeaponry.callbacks.processCategory(self, "adjustBulletStructure", bul)
	
    local tr = {}
	for i = 1, bullets do
		local Dir2 = Dir
		
		if clumpSpread and clumpSpread > 0 then
			Dir2 = Dir + Vector(math.Rand(-1, 1), math.Rand(-1, 1), math.Rand(-1, 1)) * clumpSpread
		end
		
		if not CustomizableWeaponry.callbacks.processCategory(self, "suppressDefaultBullet", sp, Dir2, commandNumber) then
			bul.Num = 1
			bul.Src = sp
			bul.Dir = Dir2
			bul.Spread 	= zeroVec --Vector(0, 0, 0)
			bul.Tracer	= 3
			bul.Force	= damage * 0.3
			bul.Damage = math.Round(damage)
			bul.Callback = self.bulletCallback
			
			self.Owner:FireBullets(bul)
			
			tr.start = sp
			tr.endpos = tr.start + Dir2 * self.PenetrativeRange
			tr.filter = self.Owner
			tr.mask = self.NormalTraceMask
			
			local trace = util.TraceLine(tr)
				
			if trace.Hit and not trace.HitSky then
				local canPenetrate, dot = self:canPenetrate(trace, Dir2)
				
				if canPenetrate and dot > 0.26 then
					tr.start = trace.HitPos
					tr.endpos = tr.start + Dir2 * self.PenStr * (self.PenetrationMaterialInteraction[trace.MatType] and self.PenetrationMaterialInteraction[trace.MatType] or 1) * self.PenMod
					tr.filter = self.Owner
					tr.mask = self.WallTraceMask
					
					trace = util.TraceLine(tr)
					
					tr.start = trace.HitPos
					tr.endpos = tr.start + Dir2 * 0.1
					tr.filter = self.Owner
					tr.mask = self.NormalTraceMask
					
					trace = util.TraceLine(tr) -- run ANOTHER trace to check whether we've penetrated a surface or not
					
					if not trace.Hit then
						bul.Num = 1
						bul.Src = trace.HitPos
						bul.Dir = Dir2
						bul.Spread 	= Vec0
						bul.Tracer	= 4
						bul.Force	= damage * 0.15
						bul.Damage = bul.Damage * 0.5
						
						self.Owner:FireBullets(bul)
						
						bul.Num = 1
						bul.Src = trace.HitPos
						bul.Dir = -Dir2
						bul.Spread 	= Vec0
						bul.Tracer	= 4
						bul.Force	= damage * 0.15
						bul.Damage = bul.Damage * 0.5
						
						self.Owner:FireBullets(bul)
					end
				else
					if self:canRicochet(trace) then
						dot = dot or self:getSurfaceReflectionDotProduct(trace, Dir2)
						Dir2 = Dir2 + (trace.HitNormal * dot) * 3
						math.randomizeVector(Dir2, 0.06)
						
						bul.Num = 1
						bul.Src = trace.HitPos
						bul.Dir = Dir2
						bul.Spread 	= Vec0
						bul.Tracer	= 0
						bul.Force	= damage * 0.225
						bul.Damage = bul.Damage * 0.75
						
						self.Owner:FireBullets(bul)
					end
				end
			end
		end
	end
		
	tr.mask = self.NormalTraceMask
end