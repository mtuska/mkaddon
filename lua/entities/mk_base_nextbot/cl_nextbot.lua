// RAWR!

function ENT:Initialize()
	self:MKInitialize() // used to help with the child hierarchy
end

function ENT:MKInitialize()
	//PrintTable(self:GetSequenceList())
	self:DrawShadow(false)
	self:SetIK(false)

	self.MKBaseClass = scripted_ents.Get("mk_nextbot")
	self.Weapons = {}
	self.WeaponAmmo = {}
end

function ENT:addCoffeeCup()
	/*local shootpos = self:GetAttachment(self:LookupAttachment("anim_attachment_LH"))

	local prop = ents.Create("prop_physics")
	prop:SetModel(Model("models/props/cs_office/coffee_mug.mdl"))
	prop:SetOwner(self)

	prop:Spawn()

	prop:SetSolid(SOLID_NONE)
	prop:SetParent(self)

	prop:Fire("setparentattachment", "anim_attachment_LH")
	prop:SetLocalAngles(Angle(0, 90, 0))

	self.CoffeeCup = prop*/
end

function ENT:addBubble()
	local bone = self:LookupBone("ValveBiped.Bip01_Head1")
	if (bone) then
		self.bubble = ClientsideModel("models/extras/info_speech.mdl", RENDERGROUP_OPAQUE)
		local pos = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_Head1"))
		self.bubble:SetPos(pos + Vector(0, 0, 15))
		self.bubble:SetModelScale(0.2, 0)
		self.bubble:SetParent(self)
	else
		error("Model '"..self:GetModel().."' has no bone ValveBiped.Bip01_Head1!")
	end
end

function ENT:OnRemove()
	if (IsValid(self.bubble)) then
		self.bubble:Remove()
	end
end

function ENT:Draw()
	self:DrawModel()
	self:DrawDebug()
	self:BodyUpdate()

	local realTime = RealTime()

	if (IsValid(self.bubble)) then
		local realTime = RealTime()

		local bone = self:LookupBone("ValveBiped.Bip01_Head1")
		if (bone) then
			local pos = self:GetBonePosition(bone)
			self.bubble:SetRenderOrigin(pos + Vector(0, 0, 15 + math.sin(realTime * 3) * 0.75))
			self.bubble:SetRenderAngles(Angle(0, realTime * 100, 0))
		else
			error("Model '"..self:GetModel().."' has no bone ValveBiped.Bip01_Head1!")
			self.bubble:Remove()
		end
	end
end

function ENT:DrawDebug()
	if (!GetConVar("mk_nb_debug_draw"):GetBool()) then return end

	local startpos = self:GetPos()
	local dir = self:GetAimVector()
	local dmins, dmaxs = self:GetHullDuck()
	local mins, maxs = self:GetHull()
	local len = 20
	mins.z = mins.z + dmaxs.z + 1 // add 1 for show

	local tr = util.TraceHull( {
		start = startpos,
		endpos = startpos + dir * len,
		maxs = maxs,
		mins = mins,
		filter = self
	} )

	render.DrawWireframeBox( startpos, Angle( 0, 0, 0 ), mins, maxs, Color( 255, 255, 255 ), true )
	render.DrawWireframeBox( tr.HitPos, Angle( 0, 0, 0 ), mins, maxs, tr.Hit and Color( 255, 0, 0 ) or Color( 255, 255, 255 ), true )

	if (self:GetFindStance()) then
		dmins.z = dmins.z + self:GetStepSize()

		local tr = util.TraceHull( {
			start = startpos,
			endpos = startpos + dir * len,
			maxs = dmaxs,
			mins = dmins,
			filter = self
		} )

		render.DrawWireframeBox( startpos, Angle( 0, 0, 0 ), dmins, dmaxs, Color( 0, 255, 0 ), true )
		render.DrawWireframeBox( tr.HitPos, Angle( 0, 0, 0 ), dmins, dmaxs, tr.Hit and Color( 255, 0, 0 ) or Color( 0, 255, 0 ), true )

		mins.z = mins.z + self:GetStepSize()

		local tr = util.TraceHull( {
			start = startpos,
			endpos = startpos + dir * len,
			maxs = maxs,
			mins = mins,
			filter = self
		} )

		render.DrawWireframeBox( startpos, Angle( 0, 0, 0 ), mins, maxs, Color( 0, 0, 255 ), true )
		render.DrawWireframeBox( tr.HitPos, Angle( 0, 0, 0 ), mins, maxs, tr.Hit and Color( 255, 0, 0 ) or Color( 0, 0, 255 ), true )
	end

	render.DrawLine( self:GetShootPos(), self:GetShootPos() + (self:GetEyeAngles():Forward() * 1000), Color( 0, 255, 0 ), true )
	if self:HasEnemy() then
		local ang = self:WorldToLocal(self:GetEnemy():GetPos()):Angle()
		render.DrawLine( self:GetPos(), self:GetPos() + ((self:GetAngles() + ang):Forward() * self:GetPos():Distance(self:GetEnemy():GetPos())), Color( 255, 0, 0 ), true )
	end

	local positions = {}
	positions[1] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_R_Calf"))
	positions[2] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_L_Calf"))
	positions[3] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_R_Thigh"))
	positions[4] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_L_Thigh"))
	positions[5] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_L_Calf"))
	positions[6] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_Spine"))
	if (self:LookupBone("ValveBiped.Bip01_Head1")) then
		positions[7] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_Head1"))
	end

	local dir = self:GetForward()
	local len = 50

	local trs = {}
	for k,pos in pairs(positions) do
		trs[k] = util.TraceLine({
			start = pos,
			endpos = pos + dir * len,
			filter = self
		})
	end

	for k,tr in pairs(trs) do
		render.DrawLine( tr.StartPos, tr.HitPos, tr.Hit and Color(255,0,0) or Color( 0, 255, 0 ), true )
	end
end

net.Receive("MK_Nextbots.Ragdoll", function()
	local nextbot = net.ReadEntity()
	local force = net.ReadVector()
	
	if not IsValid(nextbot) then
		print("Could not create Nextbot ragdoll: ", nextbot, force)
		return
	end
	
	local col = nextbot:GetPlayerColor()
	local scale = nextbot:GetModelScale()
	local rag = nextbot:BecomeRagdollOnClient()
	rag.GetPlayerColor = function() return col end
	rag:SetModelScale(scale, 0)
	
	if not IsValid(rag) then
		print("Nextbot ragdoll was not created: ", nextbot, rag)
		return
	end

	for i=0, nextbot:GetBoneCount()-1 do
		rag:ManipulateBoneScale(i, nextbot:GetManipulateBoneScale(i))
		rag:ManipulateBonePosition(i, nextbot:GetManipulateBonePosition(i))
	end
	
	for i=1, rag:GetPhysicsObjectCount() do
		if IsValid(rag:GetPhysicsObjectNum(i)) then
			rag:GetPhysicsObjectNum(i):ApplyForceCenter(force/2 or Vector(0,0,0))
		end
	end
	
	timer.Simple(120, function() // remove body after 2 minutes
		if rag:IsValid() then
			rag:Remove()
		end
	end)
end)
