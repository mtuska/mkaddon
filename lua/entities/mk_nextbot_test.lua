// RAWR!

AddCSLuaFile()

ENT.Base 			= "mk_base_nextbot"
ENT.Spawnable		= false

list.Set( "NPC", "mk_nextbot_test", {
	Name = "Test",
	Class = "mk_nextbot_test",
	Category = "MK Nextbots"
} )

if (CLIENT) then
	return
end

function ENT:Initialize()
	self:MKInitialize()

	self:SetName("Test Bot")

	self:SetWalkSpeed(50)
	self:SetRunSpeed(50)
	self.loco:SetDesiredSpeed(self:GetWalkSpeed())

	self:SetModelScale(math.Rand(0.8, 1.2), 0)
	self:SetPlayerColor(Vector(math.random(), math.random(), math.random()))

	if (MK.modules.nutrition) then
		local size = 1
		if (math.Rand(0, 1) < 0.3) then
			size = math.Rand(0.4, 2)
		else
			size = math.Rand(0.8, 1.2)
		end
		self:MK_Nutrition_Size(size)
	end
end

function ENT:RunBehaviour()
	while (true) do
		self:MoveTo(Vector(-1551, 525, 441), {useDoors = false}) // rp_evilmelon top ladder
		//self:MoveTo(Vector(-7426, -4741, 72), {useDoors = false})// rp_rockford_v2b pd
		//self:MoveTo(self:GetDestination(), {useDoors = false})

		coroutine.yield()
	end
end