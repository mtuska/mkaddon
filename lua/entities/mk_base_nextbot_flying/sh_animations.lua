// RAWR!

function ENT:BodyUpdate()
    local val = (SERVER && 1) or 2
    if (GetConVar("mk_nb_animated"):GetInt() != val) then return end
	local velocity = self:GetVelocity()
    self:SequenceUpdate(velocity)

	if (self:HasEnemy()) then
		local ang = self:WorldToLocal(self:GetEnemy():EyePos()):Angle()
		self:SetAngles(self:GetAngles() + ang)
	end

	/*print("velocity", velocity)
	for i = 0, self:GetNumPoseParameters() - 1 do
		local sPose = self:GetPoseParameterName(i)
		print(sPose, self:GetPoseParameter(sPose), "|", self:GetPoseParameterRange(i))
	end
	print("~~~~~~~~~~~~~~~~~~~")*/

	//self:SetPoseParameter("anger", 0)
	//self:SetPoseParameter("surprised", 0)

    self:FrameAdvance()
end

function ENT:SequenceUpdate(velocity)
    if (self.m_LoopSeqName && self.m_LoopSeqName != "") then
		if (!self.m_PlayingSequence && CurTime() >= self.m_UpdateSeqLoop) then
			self.m_UpdateSeqLoop = CurTime() + self:PlaySequence(self.m_LoopSeqName, self.m_LoopSeqSpeed)
		end
	end
end

function ENT:PlaySequence(name, speed)
	self.m_PlayingSequence = true
	local len = self:SetSequence( name )
	speed = speed or 1

	self:ResetSequenceInfo()
	self:SetCycle( 0 )
	self:SetPlaybackRate( speed )
	local time = len/speed

	timer.Simple(time, function()
		if (!IsValid(self)) then return end
		self.m_PlayingSequence = false
		//self:StartActivity(self:GetActivity())
	end)
	return time
end

function ENT:LoopSequence(name, speed)
	if (self.m_LoopSeqName == name) then return end
	if (name == "") then
		self.m_PlayingSequence = false
	end
	self.m_LoopSeqName = name
	self.m_LoopSeqSpeed = speed or 1
	self.m_UpdateSeqLoop = CurTime()
end