// RAWR!

function ENT:GetDestination() // Calculate end location of a wander
	if (self.ResumePos) then
		return self.ResumePos
	end
	local radius = radius or 2000

	local loc = nil
	while (!util.IsInWorld(loc)) do
		local i = 0
		/*for k,pos in RandomPairs(MK.nextbots.airnodes) do
			if (self:TestPVS(pos) && self:VisibleVec(pos)) then
				i = i + 1
				//if (self:GetPos():DistToSqr(pos) <= radius) then
					//return pos
					loc = pos
					break
				//end
			end
		end
		BroadcastLua("print('"..i.."')")*/
		loc = loc or self:GetPos() + Vector(math.random(-250,250),math.random(-250,250),math.random(-250,250))
	end
	return loc
end

function ENT:StopMovingToPos()
	self.m_AllowedToMove = false
	self:SetVelocity(-self:GetVelocity()) // Negate the velocity
end

function ENT:FlyTo(pos, options)
	self:SetDebugLocation(pos)

	local options = options or {}

	self.path = Path("Fly")
	local path = self.path

	path:SetMinLookAheadDistance(options.lookahead or 300)
	path:SetGoalTolerance(options.tolerance or 20)

	path:Compute(self, pos or self:GetPos())

	if (!path:IsValid()) then return "failed" end

    self.m_AllowedToMove = true
	self.LastPos = {self:GetPos()}

    while (path:IsValid() && self.m_AllowedToMove) do
		if (options.useAttack && GetConVar("mk_nb_attacking"):GetBool() && self:HasEnemy()) then
			self:AttackBehaviour()
		end
		if (GetConVar("mk_nb_movement"):GetBool()) then
        	path:Update(self)
		end
		
		if (self:CustomStuck(pos) == "stuck") then
			return "stuck"
		end

		-- Draw the path (only visible on listen servers or single player)
		if ( options.draw ) then
			path:Draw()
		end
		-- If we're stuck then call the HandleStuck function and abandon
		/*if ( self.loco:IsStuck() ) then
			self:HandleStuck();
			return "stuck"
		end*/
		-- If they set maxage on options then make sure the path is younger than it
		if ( options.maxage ) then
			if ( path:GetAge() > options.maxage ) then self:SetVelocity(-self:GetVelocity()) return "timeout" end
		end
		-- If they set repath then rebuild the path every x seconds
		if ( options.repath ) then
			if ( path:GetAge() > options.repath ) then path:Compute( self, pos ) end
		end

		coroutine.yield()
    end
	self.path = nil

	return "ok"
end

function ENT:CustomStuck(pos)
	/*local count = #self.LastPos
	local minCheck = 10
	if count > minCheck then
		self.LastPos[minCheck+1] = nil
		local sum = 0
		for i=1, minCheck do
			sum = sum + (self:GetPos() - self.LastPos[i]):Length2DSqr()
		end
		self.LastPos = {self:GetPos()}
		print("CustomStuck: "..(sum/minCheck))
		if (sum/minCheck) < 200 then
			//BroadcastLua("print('math: "..(sum/minCheck).."')")
			self:HandleCustomStuck()

			return "stuck"
		end
	end*/
end

function ENT:HandleCustomStuck()

end