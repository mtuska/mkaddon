// RAWR!

AddCSLuaFile()

ENT.Base 			= "base_anim"
ENT.Type			= "anim"
ENT.PrintName		= "Madkiller Max's Nextbot Flying Base"
ENT.Author			= "Madkiller Max"
ENT.Contact			= "Message someone who cares"
ENT.Purpose			= "To make creating nextbots more efficent and optimized"
ENT.Instructions	= "Create a new nextbot and override it's RunBehavior! Do whatever you want, I don't care."
ENT.RenderGroup		= RENDERGROUP_OPAQUE
ENT.ScriptedEntityType = "npc"
ENT.MKFlyingNextbot = true

function ENT:SetupDataTables()
	self:InstallDataTable()
	self:NetworkVar("String", 0, "Name")

	self:NetworkVar("Entity", 0, "Enemy")
	self:NetworkVarNotify("Enemy", self.OnVarChanged)

	self:NetworkVar("Int", 0, "FlySpeed")

	self:NetworkVar("Vector", 0, "DebugLocation")
end

function ENT:OnVarChanged(name, old, new)
	if (name == "Enemy") then
		if (SERVER) then
			self:Int_OnSetEnemy(old, new) // Internal function
		end
	end
end

if (SERVER) then
	AddCSLuaFile("sh_functions.lua")
	AddCSLuaFile("sh_animations.lua")
	AddCSLuaFile("cl_nextbot.lua")
	AddCSLuaFile("cl_functions.lua")
	AddCSLuaFile("cl_animations.lua")
	AddCSLuaFile("cl_enemy.lua")
	include("sh_functions.lua")
	include("sh_animations.lua")
	include("sv_functions.lua")
	include("sv_animations.lua")
	include("sv_nextbot.lua")
	include("sv_enemy.lua")
	include("sv_pathing.lua")
else
	include("sh_functions.lua")
	include("sh_animations.lua")
	include("cl_nextbot.lua")
	include("cl_functions.lua")
	include("cl_animations.lua")
	include("cl_enemy.lua")
end
