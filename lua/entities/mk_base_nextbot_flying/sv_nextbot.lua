// RAWR!

ENT.ViewDistance = 1000
ENT.LooseDistance = 2000

function ENT:Initialize()
	self:MKInitialize() // used to help with the child hierarchy
end

function ENT:MKInitialize()
	MK.nextbots.Add(self) // Add itself to the global nextbot list, so that way it doesn't get lost
	if (MK.nextbots.targetting) then
		if (self.targetting.bEnabled) then
			MK.nextbots.targetting.addPerceiver(self)
		end
		MK.nextbots.targetting.addStimulus(self)
	end
	self.MKBaseClass = scripted_ents.Get("mk_base_nextbot_flying")

	self:SetCollisionGroup(COLLISION_GROUP_NPC)
	self:SetUseType(SIMPLE_USE)
	self:AddEFlags(EFL_DONTBLOCKLOS + EFL_FORCE_CHECK_TRANSMIT)
	self:SetSolid(SOLID_BBOX)
    self:SetMoveType(MOVETYPE_FLY)

    self:SetFlySpeed(50)
	self:SetHealth(100)

    self.SpawnTime = CurTime()
	self.SpawnPos = self:GetPos()
    self.ViewDistanceSqr = self.ViewDistance^2
	self.LooseDistanceSqr = self.LooseDistance^2
    self.LastPos = {self:GetPos()}
	self.m_AllowedToMove = true

    //self:SetVelocity(Vector(0,0,10))
    self:BehaveStart()
	timer.Simple(0, function() // Next frame call Post
		if (IsValid(self)) then
			self:MKInitialize_Post()
		end
	end)
end

function ENT:MKInitialize_Post()

end

function ENT:Think()
	self:BodyUpdate()
    self:BehaveUpdate()

	self:NextThink(CurTime() + 0.1)
	return true
end

function ENT:BehaveStart()
	self.BehaveThread = coroutine.create( function() self:RunBehaviour() end )
end

function ENT:BehaveUpdate( fInterval )
	if (GetConVar("mk_nb_locked"):GetBool()) then return end
	if (self.m_Locked) then return end
	if ( !self.BehaveThread ) then return end

	--
	-- Give a silent warning to developers if RunBehaviour has returned
	--
	if ( coroutine.status( self.BehaveThread ) == "dead" ) then
		self.BehaveThread = nil
		Msg( self, " Warning: ENT:RunBehaviour() has finished executing\n" )

		return
	end

	--
	-- Continue RunBehaviour's execution
	--
	local ok, message = coroutine.resume( self.BehaveThread )
	if ( ok == false ) then
		self.BehaveThread = nil
		ErrorNoHalt( self, " Error: ", message, "\n" )
	end
end

function ENT:RunBehaviour()
	self:Lock()
	error("Child nextbot should override ENT:RunBehaviour()")
end

function ENT:OnInjured(dmginfo)
	print("I took some damage!!")
end

// Not currently implemented
function ENT:OnOtherKilled(victim, dmginfo)

end

function ENT:OnKilled( dmginfo )
	hook.Run( "OnFlyingNextbotKilled", self, dmginfo:GetAttacker(), dmginfo:GetInflictor() )

	local attacker = dmginfo:GetAttacker()
	if (IsValid(attacker) && attacker:IsPlayer()) then
		attacker:AddFrags(1)
	end
	/*net.Start("MK_Nextbots.Ragdoll")
		net.WriteEntity(self)
		net.WriteVector(dmginfo:GetDamageForce())
	net.Broadcast()*/
	if (IsValid(self)) then
		self:Remove()
	end
end

hook.Add("EntityTakeDamage", "MK_FlyingNextbots.EntityTakeDamage", function(target, dmginfo)
	if (target.MKFlyingNextbot) then
		hook.Run("ScaleFlyingNextbotDamage", target, HITGROUP_GENERIC, dmginfo)

		target:SetHealth(target:Health() - dmginfo:GetDamage())
		if (target:Health() <= 0) then
			target:OnKilled(dmginfo)
			if (IsValid(target)) then
				target:Remove()
			end
		else
			target:OnInjured(dmginfo)
		end
	end
end)

// Use gamemode here, so that a hook can easily override it
local _gm = GM or GAMEMODE
function _gm:ScaleNextbotDamage(nextbot, hitgroup, dmginfo)
	if (hitgroup == HITGROUP_HEAD) then
		local s = 2
		local wep = util.WeaponFromDamage(dmginfo)
		if (IsValid(wep) && wep.GetHeadshotMultiplier) then
			s = wep:GetHeadshotMultiplier(nextbot, dmginfo)
		end
		dmginfo:ScaleDamage(s)
	elseif (hitgroup == HITGROUP_LEFTARM || hitgroup == HITGROUP_RIGHTARM || hitgroup == HITGROUP_LEFTLEG || hitgroup == HITGROUP_RIGHTLEG) then
      	dmginfo:ScaleDamage(0.25)
	elseif (hitgroup == HITGROUP_GEAR) then
		dmginfo:ScaleDamage(0)
	end
end
