// RAWR!

function ENT:Initialize()
	self:MKInitialize() // used to help with the child hierarchy
end

function ENT:MKInitialize()
	//PrintTable(self:GetSequenceList())

	self.MKBaseClass = scripted_ents.Get("mk_nextbot_flying")
end

function ENT:Draw()
    self:DrawModel()
    self:DrawDebug()
    self:BodyUpdate()
end

function ENT:DrawDebug()
	if (!GetConVar("mk_nb_debug_draw"):GetBool()) then return end

	local mins, maxs = self:GetCollisionBounds()
	render.DrawWireframeBox(self:GetPos(), Angle(), mins, maxs, Color(255,255,255), true)

    render.DrawWireframeSphere(self:GetDebugLocation(), 20, 10, 10, Color( 255, 255, 255 ), true )

	render.DrawLine(self:GetPos(), self:GetDebugLocation(), Color(255, 215, 0), false)

	render.DrawLine(self:GetDebugLocation() + Vector(0,0,5), self:GetDebugLocation() - Vector(0,0,5), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetDebugLocation() + Vector(0,5,0), self:GetDebugLocation() - Vector(0,5,0), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetDebugLocation() + Vector(5,0,0), self:GetDebugLocation() - Vector(5,0,0), Color( 255, 0, 0 ), true)

	render.DrawLine(self:GetPos() + Vector(0,0,5), self:GetPos() - Vector(0,0,5), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetPos() + Vector(0,5,0), self:GetPos() - Vector(0,5,0), Color( 255, 0, 0 ), true)
	render.DrawLine(self:GetPos() + Vector(5,0,0), self:GetPos() - Vector(5,0,0), Color( 255, 0, 0 ), true)

	render.DrawLine( self:GetShootPos(), self:GetShootPos() + (self:GetShootAngles():Forward() * 1000), Color( 0, 255, 0 ), true )
	if self:HasEnemy() then
		local ang = self:WorldToLocal(self:GetEnemy():GetPos()):Angle()
		render.DrawLine( self:GetPos(), self:GetPos() + ((self:GetAngles() + ang):Forward() * self:GetPos():Distance(self:GetEnemy():GetPos())), Color( 255, 0, 0 ), true )
	end
/*
	local clr = color_white
	if ( tr.Hit ) then
		clr = Color( 255, 0, 0 )
		//PrintTable(tr)
	end

	render.DrawWireframeBox( startpos, Angle( 0, 0, 0 ), mins, maxs, Color( 255, 255, 255 ), true )
	render.DrawWireframeBox( tr.HitPos, Angle( 0, 0, 0 ), mins, maxs, clr, true )

	local positions = {}
	positions[1] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_R_Calf"))
	positions[2] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_L_Calf"))
	positions[3] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_R_Thigh"))
	positions[4] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_L_Thigh"))
	positions[5] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_L_Calf"))
	positions[6] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_Spine"))
	positions[7] = self:GetBonePosition(self:LookupBone("ValveBiped.Bip01_Head1"))

	local dir = self:GetForward()
	local len = 50

	local trs = {}
	for k,pos in pairs(positions) do
		trs[k] = util.TraceLine({
			start = pos,
			endpos = pos + dir * len,
			filter = self
		})
	end

	for k,tr in pairs(trs) do
		render.DrawLine( tr.StartPos, tr.HitPos, tr.Hit and Color(255,0,0) or Color( 0, 255, 0 ), true )
	end*/
end