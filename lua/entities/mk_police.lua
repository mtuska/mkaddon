// RAWR!

AddCSLuaFile()

ENT.Base 			= "mk_base_nextbot"
ENT.Spawnable		= false

list.Set( "NPC", "mk_police", {
	Name = "Police",
	Class = "mk_police",
	Category = "MK Nextbots"
} )

if (CLIENT) then return end

ENT.targetting = {}
ENT.targetting.bEnabled = true
ENT.targetting.bReset = false
ENT.targetting.bClosest = true
ENT.targetting.bPOV = false
ENT.targetting.bLOS = false
ENT.targetting.viewDist = 1000
ENT.targetting.looseDist = 2000

function ENT:Initialize()
	self:MKInitialize()
	self.chaseTolerance = math.random(50, 250)

	local tbl = {
		"models/player/police_fem.mdl",
		"models/player/police.mdl"
	}
	self:SetModel(table.Random(tbl))
	--self:SetModel("models/police.mdl")

	self:SetName("Police")

	self:Give("stungun")
	self:Give("arrest_stick")
	self:Give("door_ram")
	self:Give("stunstick")
	self:Give("cw_ar15")
	self:Give("cw_p99")
	self:GiveAmmo(10000, "5.56x45MM")
	self:GiveAmmo(10000, "9x19MM")

	self:ChooseWeapon()

	self.StayNearSpawn = false

	self:SetWalkSpeed(80)
	self:SetRunSpeed(100)
	if (GAMEMODE.Config && GAMEMODE.Config.runspeedcp) then
		self:SetRunSpeed(GAMEMODE.Config.runspeedcp)
	end

	self.DarkRPVars["HasGunlicense"] = true

	//table.Add(MK.civs.police, self)
	//self.m_Locked = true
end

function ENT:OnTargettingUpdate()
	if (!self:GetEnemyVisibility()) then
		// Pick the next best enemy to target since ours is out of sight
		local closestTarget, closestDist
		for k,v in pairs(self.EnemyMemory) do
			if (!v.Visibility) then continue end
			if (!closestDist || v.Distance <= closestDist) then
				closestDist = v.Distance
				closestTarget = k
			end
		end
		if (closestTarget) then
			ent:SetEnemy(closestTarget)
		end
	end
end

function ENT:AttackBehaviour()
	//BroadcastLua("print('Behave attack')")
	self:ChooseWeapon()
	local wep = self:GetActiveWeapon()
	if (IsValid(wep)) then
		if (string.find(wep:GetClass(), "door_ram")) then
			if (!wep:GetIronsights()) then
				self:FireSecondary()
			else
				self:FirePrimary()
			end
		end
		if (self:HasEnemy() && self:VisibleVec(self:GetEnemy():EyePos())) then
			if (string.find(wep:GetClass(), "stungun")) then
				if (!self:GetEnemy():IsPlayer()) then
					self:FireSecondary()
				elseif (self:GetEyePos():DistToSqr(self:GetEnemy():EyePos()) < 160000) then // 400^2
					self:FirePrimary()
				end
				//wep.Charge = 0
			elseif (string.find(wep:GetClass(), "stick")) then
				if (!IsValid(self:GetEnemy().tazeragdoll) && self:GetEyePos():DistToSqr(self:GetEnemy():EyePos()) < 8100) then // 90^2
					//BroadcastLua("print('Beat him!')")
					self:FirePrimary()
				end
			elseif (string.find(wep:GetClass(), "cw_ar15")) then
				self:FirePrimary()
			end
		end
	end
	self.BaseClass.AttackBehaviour(self)
end

function ENT:RunBehaviour()
	while (true) do
		if (self:HasEnemy()) then
			self.loco:SetDesiredSpeed(self:GetRunSpeed())

			self:Chase({chaseDist = 500, useAttack = true})
		elseif (self.Investigate) then
			self.loco:SetDesiredSpeed(self:GetRunSpeed())
			local pos = self:GetPos()
			self:MoveTo(self.Investigate, {useAttack = true})
			if (pos == self.Investigate) then
				//self.Investigate = nil
			end
		else
			self.loco:SetDesiredSpeed(self:GetWalkSpeed())

			self:MoveTo(self:GetDestination())
		end

		coroutine.yield()
	end
end

local function canRam(ply)
    return IsValid(ply) and (ply.warranted or (ply.isWanted && ply:isWanted()) or (ply.isArrested && ply:isArrested()))
end

function ENT:HandleDoorLocked() // Since police have a door ram, we don't get blocked by locked doors
	if (self:GetEnemy() && canRam(self:GetEnemy())) then
		self.loco:ClearStuck()
	elseif (self.LastEnemy && canRam(self.LastEnemy)) then
		self.loco:ClearStuck()
	else
		self.BaseClass.HandleDoorLocked(self)
	end
end

function ENT:ChooseWeapon()
	if (self:HasEnemy()) then
		if (self.Door && self:GetDoorState(self.Door) == DOOR_STATE_CLOSED && self:IsDoorLocked(self.Door) && canRam(self:GetEnemy())) then
			self:SelectWeapon("door_ram")
		elseif (self.Prop) then // Hopefully the server setting let the doorram hit props...
			self:SelectWeapon("door_ram")
		elseif (self:GetThreat() > .6) then
			self:SelectWeapon("cw_p99")
		elseif (self:GetEnemy().isWanted && self:GetEnemy():isWanted()) then
			if (!IsValid(self:GetEnemy().tazeragdoll) && self:HasWeapon("stungun") && self:GetEyePos():DistToSqr(self:GetEnemy():EyePos()) > 22500 && CurTime() >= (self.EnemySetTime + 10)) then // 150^2
				self:SelectWeapon("stungun")
			else
				self:SelectWeapon("arrest_stick")
			end
		elseif (!self:GetEnemy():IsPlayer()) then
			if (string.find(self:GetEnemy():GetClass():lower(), "zombie")) then
				self:SelectWeapon("cw_p99")
			end
		else
			self:SelectWeapon("stunstick")
		end
	elseif (self.Investigate && self.LastEnemy) then

	else
		self:SelectWeapon("stunstick")
	end

	if (self:GetActiveWeapon():Clip1() == 0 && self:GetAmmoCount(self:GetActiveWeapon():GetPrimaryAmmoType()) == 0) then
		self:Reload()
	end
end

function ENT:OnKilled(dmginfo)
	self.BaseClass.OnKilled(self, dmginfo)

	self:VoiceSound("npc/metropolice/die"..math.random(1,4)..".wav", false, true)
end

function ENT:OnInjured(dmginfo)
	self.MKBaseClass.OnInjured(self, dmginfo)
	self:TrySetEnemy(dmginfo:GetAttacker())
end

function ENT:OnSetEnemy()
	// Break the current move to
	self:StopMovingToPos()
	if (self:CheckEnemy()) then
		self:VoiceSound("npc/metropolice/vo/holditrightthere.wav")
	else
		if (IsValid(self.LastEnemy) && ((self.LastEnemy.isArrested && self.LastEnemy:isArrested()) || !self.LastEnemy:Alive())) then
			self:VoiceSound("npc/combine_soldier/vo/overwatchtargetcontained.wav", false, true)
		end
	end
	self:ChooseWeapon()
end

function ENT:isCP()
	return true
end

function ENT:IsEnemy(ent)
	//return false
	if (ent.isCP and ent:isCP()) then
		return false
	end
	if (ent.IsZombie and ent:IsZombie()) then
		return true
	end
	if (ent.isWanted and ent:isWanted()) then
		return true
	end
	return false
	//return ent.SteamID and ent:SteamID() == "STEAM_0:1:7102488"
end

--[[
npc/combine_soldier/die1.wav
npc/combine_soldier/die2.wav
npc/combine_soldier/die3.wav
npc/combine_soldier/gear1.wav
npc/combine_soldier/gear2.wav
npc/combine_soldier/gear3.wav
npc/combine_soldier/gear4.wav
npc/combine_soldier/gear5.wav
npc/combine_soldier/gear6.wav
npc/combine_soldier/pain1.wav
npc/combine_soldier/pain2.wav
npc/combine_soldier/pain3.wav
npc/combine_soldier/vo npc/combine_soldier/vo/administer.wav
npc/combine_soldier/vo/affirmative.wav
npc/combine_soldier/vo/affirmative2.wav
npc/combine_soldier/vo/affirmativewegothimnow.wav
npc/combine_soldier/vo/alert1.wav
npc/combine_soldier/vo/anticitizenone.wav
npc/combine_soldier/vo/antiseptic.wav
npc/combine_soldier/vo/apex.wav
npc/combine_soldier/vo/bearing.wav
npc/combine_soldier/vo/blade.wav
npc/combine_soldier/vo/block31mace.wav
npc/combine_soldier/vo/block64jet.wav
npc/combine_soldier/vo/bodypackholding.wav
npc/combine_soldier/vo/boomer.wav
npc/combine_soldier/vo/bouncerbouncer.wav
npc/combine_soldier/vo/callcontactparasitics.wav
npc/combine_soldier/vo/callcontacttarget1.wav
npc/combine_soldier/vo/callhotpoint.wav
npc/combine_soldier/vo/cleaned.wav
npc/combine_soldier/vo/closing.wav
npc/combine_soldier/vo/closing2.wav
npc/combine_soldier/vo/confirmsectornotsterile.wav
npc/combine_soldier/vo/contact.wav
npc/combine_soldier/vo/contactconfim.wav
npc/combine_soldier/vo/contactconfirmprosecuting.wav
npc/combine_soldier/vo/contained.wav
npc/combine_soldier/vo/containmentproceeding.wav
npc/combine_soldier/vo/copy.wav
npc/combine_soldier/vo/copythat.wav
npc/combine_soldier/vo/cover.wav
npc/combine_soldier/vo/coverhurt.wav
npc/combine_soldier/vo/coverme.wav
npc/combine_soldier/vo/dagger.wav
npc/combine_soldier/vo/dash.wav
npc/combine_soldier/vo/degrees.wav
npc/combine_soldier/vo/delivered.wav
npc/combine_soldier/vo/designatetargetas.wav
npc/combine_soldier/vo/displace.wav
npc/combine_soldier/vo/displace2.wav
npc/combine_soldier/vo/echo.wav
npc/combine_soldier/vo/eight.wav
npc/combine_soldier/vo/eighteen.wav
npc/combine_soldier/vo/eighty.wav
npc/combine_soldier/vo/eleven.wav
npc/combine_soldier/vo/engagedincleanup.wav
npc/combine_soldier/vo/engaging.wav
npc/combine_soldier/vo/executingfullresponse.wav
npc/combine_soldier/vo/extractoraway.wav
npc/combine_soldier/vo/extractorislive.wav
npc/combine_soldier/vo/fifteen.wav
npc/combine_soldier/vo/fifty.wav
npc/combine_soldier/vo/fist.wav
npc/combine_soldier/vo/five.wav
npc/combine_soldier/vo/fixsightlinesmovein.wav
npc/combine_soldier/vo/flaredown.wav
npc/combine_soldier/vo/flash.wav
npc/combine_soldier/vo/flatline.wav
npc/combine_soldier/vo/flush.wav
npc/combine_soldier/vo/four.wav
npc/combine_soldier/vo/fourteen.wav
npc/combine_soldier/vo/fourty.wav
npc/combine_soldier/vo/freeman3.wav
npc/combine_soldier/vo/fullactive.wav
npc/combine_soldier/vo/ghost.wav
npc/combine_soldier/vo/ghost2.wav
npc/combine_soldier/vo/goactiveintercept.wav
npc/combine_soldier/vo/gosharp.wav
npc/combine_soldier/vo/gosharpgosharp.wav
npc/combine_soldier/vo/grid.wav
npc/combine_soldier/vo/gridsundown46.wav
npc/combine_soldier/vo/hammer.wav
npc/combine_soldier/vo/hardenthatposition.wav
npc/combine_soldier/vo/hasnegativemovement.wav
npc/combine_soldier/vo/heavyresistance.wav
npc/combine_soldier/vo/helix.wav
npc/combine_soldier/vo/hunter.wav
npc/combine_soldier/vo/hurricane.wav
npc/combine_soldier/vo/ice.wav
npc/combine_soldier/vo/inbound.wav
npc/combine_soldier/vo/infected.wav
npc/combine_soldier/vo/ion.wav
npc/combine_soldier/vo/isatcode.wav
npc/combine_soldier/vo/isfieldpromoted.wav
npc/combine_soldier/vo/isfinalteamunitbackup.wav
npc/combine_soldier/vo/isholdingatcode.wav
npc/combine_soldier/vo/jet.wav
npc/combine_soldier/vo/judge.wav
npc/combine_soldier/vo/kilo.wav
npc/combine_soldier/vo/leader.wav
npc/combine_soldier/vo/lostcontact.wav
npc/combine_soldier/vo/mace.wav
npc/combine_soldier/vo/meters.wav
npc/combine_soldier/vo/motioncheckallradials.wav
npc/combine_soldier/vo/movein.wav
npc/combine_soldier/vo/necrotics.wav
npc/combine_soldier/vo/necroticsinbound.wav
npc/combine_soldier/vo/niner.wav
npc/combine_soldier/vo/nineteen.wav
npc/combine_soldier/vo/ninety.wav
npc/combine_soldier/vo/nomad.wav
npc/combine_soldier/vo/nova.wav
npc/combine_soldier/vo/noviscon.wav
npc/combine_soldier/vo/off1.wav
npc/combine_soldier/vo/off2.wav
npc/combine_soldier/vo/off3.wav
npc/combine_soldier/vo/on1.wav
npc/combine_soldier/vo/on2.wav
npc/combine_soldier/vo/one.wav
npc/combine_soldier/vo/onecontained.wav
npc/combine_soldier/vo/onedown.wav
npc/combine_soldier/vo/onedutyvacated.wav
npc/combine_soldier/vo/onehundred.wav
npc/combine_soldier/vo/outbreak.wav
npc/combine_soldier/vo/outbreakstatusiscode.wav
npc/combine_soldier/vo/overwatch.wav
npc/combine_soldier/vo/overwatchconfirmhvtcontained.wav
npc/combine_soldier/vo/overwatchreportspossiblehostiles.wav
npc/combine_soldier/vo/overwatchrequestreinforcement.wav
npc/combine_soldier/vo/overwatchrequestreserveactivation.wav
npc/combine_soldier/vo/overwatchrequestskyshield.wav
npc/combine_soldier/vo/overwatchrequestwinder.wav
npc/combine_soldier/vo/overwatchsectoroverrun.wav
npc/combine_soldier/vo/overwatchtarget1sterilized.wav
npc/combine_soldier/vo/overwatchtargetcontained.wav
npc/combine_soldier/vo/overwatchteamisdown.wav
npc/combine_soldier/vo/ovewatchorders3ccstimboost.wav
npc/combine_soldier/vo/payback.wav
npc/combine_soldier/vo/phantom.wav
npc/combine_soldier/vo/prepforcontact.wav
npc/combine_soldier/vo/priority1objective.wav
npc/combine_soldier/vo/prioritytwoescapee.wav
npc/combine_soldier/vo/prison_soldier_activatecentral.wav
npc/combine_soldier/vo/prison_soldier_boomersinbound.wav
npc/combine_soldier/vo/prison_soldier_bunker1.wav
npc/combine_soldier/vo/prison_soldier_bunker2.wav
npc/combine_soldier/vo/prison_soldier_bunker3.wav
npc/combine_soldier/vo/prison_soldier_containD8.wav
npc/combine_soldier/vo/prison_soldier_fallback_b4.wav
npc/combine_soldier/vo/prison_soldier_freeman_antlions.wav
npc/combine_soldier/vo/prison_soldier_fullbioticoverrun.wav
npc/combine_soldier/vo/prison_soldier_leader9dead.wav
npc/combine_soldier/vo/prison_soldier_negativecontainment.wav
npc/combine_soldier/vo/prison_soldier_prosecuteD7.wav
npc/combine_soldier/vo/prison_soldier_sundown3dead.wav
npc/combine_soldier/vo/prison_soldier_tohighpoints.wav
npc/combine_soldier/vo/prison_soldier_visceratorsA5.wav
npc/combine_soldier/vo/prosecuting.wav
npc/combine_soldier/vo/quicksand.wav
npc/combine_soldier/vo/range.wav
npc/combine_soldier/vo/ranger.wav
npc/combine_soldier/vo/razor.wav
npc/combine_soldier/vo/readycharges.wav
npc/combine_soldier/vo/readyextractors.wav
npc/combine_soldier/vo/readyweapons.wav
npc/combine_soldier/vo/readyweaponshostilesinbound.wav
npc/combine_soldier/vo/reaper.wav
npc/combine_soldier/vo/reportallpositionsclear.wav
npc/combine_soldier/vo/reportallradialsfree.wav
npc/combine_soldier/vo/reportingclear.wav
npc/combine_soldier/vo/requestmedical.wav
npc/combine_soldier/vo/requeststimdose.wav
npc/combine_soldier/vo/ripcord.wav
npc/combine_soldier/vo/ripcordripcord.wav
npc/combine_soldier/vo/savage.wav
npc/combine_soldier/vo/scar.wav
npc/combine_soldier/vo/sectionlockupdash4.wav
npc/combine_soldier/vo/sector.wav
npc/combine_soldier/vo/sectorisnotsecure.wav
npc/combine_soldier/vo/sectorissecurenovison.wav
npc/combine_soldier/vo/secure.wav
npc/combine_soldier/vo/seven.wav
npc/combine_soldier/vo/seventeen.wav
npc/combine_soldier/vo/seventy.wav
npc/combine_soldier/vo/shadow.wav
npc/combine_soldier/vo/sharpzone.wav
npc/combine_soldier/vo/sightlineisclear.wav
npc/combine_soldier/vo/six.wav
npc/combine_soldier/vo/sixteen.wav
npc/combine_soldier/vo/sixty.wav
npc/combine_soldier/vo/skyshieldreportslostcontact.wav
npc/combine_soldier/vo/slam.wav
npc/combine_soldier/vo/slash.wav
npc/combine_soldier/vo/spear.wav
npc/combine_soldier/vo/stab.wav
npc/combine_soldier/vo/stabilizationteamhassector.wav
npc/combine_soldier/vo/stabilizationteamholding.wav
npc/combine_soldier/vo/standingby].wav
npc/combine_soldier/vo/star.wav
npc/combine_soldier/vo/stayalert.wav
npc/combine_soldier/vo/stayalertreportsightlines.wav
npc/combine_soldier/vo/stinger.wav
npc/combine_soldier/vo/storm.wav
npc/combine_soldier/vo/striker.wav
npc/combine_soldier/vo/sundown.wav
npc/combine_soldier/vo/suppressing.wav
npc/combine_soldier/vo/swarmoutbreakinsector.wav
npc/combine_soldier/vo/sweeper.wav
npc/combine_soldier/vo/sweepingin.wav
npc/combine_soldier/vo/swift.wav
npc/combine_soldier/vo/sword.wav
npc/combine_soldier/vo/target.wav
npc/combine_soldier/vo/targetblackout.wav
npc/combine_soldier/vo/targetcompromisedmovein.wav
npc/combine_soldier/vo/targetcontactat.wav
npc/combine_soldier/vo/targetineffective.wav
npc/combine_soldier/vo/targetisat.wav
npc/combine_soldier/vo/targetmyradial.wav
npc/combine_soldier/vo/targetone.wav
npc/combine_soldier/vo/teamdeployedandscanning.wav
npc/combine_soldier/vo/ten.wav
npc/combine_soldier/vo/thatsitwrapitup.wav
npc/combine_soldier/vo/thirteen.wav
npc/combine_soldier/vo/thirty.wav
npc/combine_soldier/vo/three.wav
npc/combine_soldier/vo/threehundred.wav
npc/combine_soldier/vo/tracker.wav
npc/combine_soldier/vo/twelve.wav
npc/combine_soldier/vo/twenty.wav
npc/combine_soldier/vo/two.wav
npc/combine_soldier/vo/twohundred.wav
npc/combine_soldier/vo/uniform.wav
npc/combine_soldier/vo/unitisclosing.wav
npc/combine_soldier/vo/unitisinbound.wav
npc/combine_soldier/vo/unitismovingin.wav
npc/combine_soldier/vo/vamp.wav
npc/combine_soldier/vo/viscon.wav
npc/combine_soldier/vo/visualonexogens.wav
npc/combine_soldier/vo/weaponsoffsafeprepforcontact.wav
npc/combine_soldier/vo/weareinaninfestationzone.wav
npc/combine_soldier/vo/wehavefreeparasites.wav
npc/combine_soldier/vo/wehavenontaggedviromes.wav
npc/combine_soldier/vo/winder.wav
npc/combine_soldier/vo/zero.wav
npc/combine_soldier/vo/_comma.wav
npc/combine_soldier/vo/_period.wav

npc/metropolice/die1.wav
npc/metropolice/die2.wav
npc/metropolice/die3.wav
npc/metropolice/die4.wav
npc/metropolice/gear1.wav
npc/metropolice/gear2.wav
npc/metropolice/gear3.wav
npc/metropolice/gear4.wav
npc/metropolice/gear5.wav
npc/metropolice/gear6.wav
npc/metropolice/hiding02.wav
npc/metropolice/hiding03.wav
npc/metropolice/hiding04.wav
npc/metropolice/hiding05.wav
npc/metropolice/knockout2.wav
npc/metropolice/pain1.wav
npc/metropolice/pain2.wav
npc/metropolice/pain3.wav
npc/metropolice/pain4.wav
npc/metropolice/takedown.wav
npc/metropolice/vo npc/metropolice/vo/11-99officerneedsassistance.wav
npc/metropolice/vo/404zone.wav
npc/metropolice/vo/acquiringonvisual.wav
npc/metropolice/vo/administer.wav
npc/metropolice/vo/affirmative.wav
npc/metropolice/vo/affirmative2.wav
npc/metropolice/vo/airwatchsubjectis505.wav
npc/metropolice/vo/allrightyoucango.wav
npc/metropolice/vo/allunitsbol34sat.wav
npc/metropolice/vo/allunitscloseonsuspect.wav
npc/metropolice/vo/allunitscode2.wav
npc/metropolice/vo/allunitsmaintainthiscp.wav
npc/metropolice/vo/allunitsmovein.wav
npc/metropolice/vo/allunitsreportlocationsuspect.wav
npc/metropolice/vo/allunitsrespondcode3.wav
npc/metropolice/vo/amputate.wav
npc/metropolice/vo/anticitizen.wav
npc/metropolice/vo/anyonepickup647e.wav
npc/metropolice/vo/apply.wav
npc/metropolice/vo/assaultpointsecureadvance.wav
npc/metropolice/vo/atcheckpoint.wav
npc/metropolice/vo/backmeupImout.wav
npc/metropolice/vo/backup.wav
npc/metropolice/vo/block.wav
npc/metropolice/vo/blockisholdingcohesive.wav
npc/metropolice/vo/breakhiscover.wav
npc/metropolice/vo/bugs.wav
npc/metropolice/vo/bugsontheloose.wav
npc/metropolice/vo/canal.wav
npc/metropolice/vo/canalblock.wav
npc/metropolice/vo/catchthatbliponstabilization.wav
npc/metropolice/vo/cauterize.wav
npc/metropolice/vo/checkformiscount.wav
npc/metropolice/vo/chuckle.wav
npc/metropolice/vo/citizen.wav
npc/metropolice/vo/citizensummoned.wav
npc/metropolice/vo/classifyasdbthisblockready.wav
npc/metropolice/vo/clearandcode100.wav
npc/metropolice/vo/clearno647no10-107.wav
npc/metropolice/vo/code100.wav
npc/metropolice/vo/code7.wav
npc/metropolice/vo/condemnedzone.wav
npc/metropolice/vo/confirmadw.wav
npc/metropolice/vo/confirmpriority1sighted.wav
npc/metropolice/vo/contactwith243suspect.wav
npc/metropolice/vo/contactwithpriority2.wav
npc/metropolice/vo/control100percent.wav
npc/metropolice/vo/controlsection.wav
npc/metropolice/vo/converging.wav
npc/metropolice/vo/copy.wav
npc/metropolice/vo/covermegoingin.wav
npc/metropolice/vo/cpbolforthat243.wav
npc/metropolice/vo/cpiscompromised.wav
npc/metropolice/vo/cpisoverrunwehavenocontainment.wav
npc/metropolice/vo/cprequestsallunitsreportin.wav
npc/metropolice/vo/cpweneedtoestablishaperimeterat.wav
npc/metropolice/vo/criminaltrespass63.wav
npc/metropolice/vo/dbcountis.wav
npc/metropolice/vo/defender.wav
npc/metropolice/vo/deservicedarea.wav
npc/metropolice/vo/designatesuspectas.wav
npc/metropolice/vo/destroythatcover.wav
npc/metropolice/vo/dismountinghardpoint.wav
npc/metropolice/vo/dispatchIneed10-78.wav
npc/metropolice/vo/dispreportssuspectincursion.wav
npc/metropolice/vo/dispupdatingapb.wav
npc/metropolice/vo/distributionblock.wav
npc/metropolice/vo/document.wav
npc/metropolice/vo/dontmove.wav
npc/metropolice/vo/eight.wav
npc/metropolice/vo/eighteen.wav
npc/metropolice/vo/eighty.wav
npc/metropolice/vo/eleven.wav
npc/metropolice/vo/establishnewcp.wav
npc/metropolice/vo/examine.wav
npc/metropolice/vo/expired.wav
npc/metropolice/vo/externaljurisdiction.wav
npc/metropolice/vo/fifteen.wav
npc/metropolice/vo/fifty.wav
npc/metropolice/vo/finalverdictadministered.wav
npc/metropolice/vo/finalwarning.wav
npc/metropolice/vo/firetodislocateinterpose.wav
npc/metropolice/vo/firingtoexposetarget.wav
npc/metropolice/vo/firstwarningmove.wav
npc/metropolice/vo/five.wav
npc/metropolice/vo/four.wav
npc/metropolice/vo/fourteen.wav
npc/metropolice/vo/fourty.wav
npc/metropolice/vo/freeman.wav
npc/metropolice/vo/freenecrotics.wav
npc/metropolice/vo/get11-44inboundcleaningup.wav
npc/metropolice/vo/getdown.wav
npc/metropolice/vo/getoutofhere.wav
npc/metropolice/vo/goingtotakealook.wav
npc/metropolice/vo/gota10-107sendairwatch.wav
npc/metropolice/vo/gothimagainsuspect10-20at.wav
npc/metropolice/vo/gotoneaccomplicehere.wav
npc/metropolice/vo/gotsuspect1here.wav
npc/metropolice/vo/grenade.wav
npc/metropolice/vo/hardpointscanning.wav
npc/metropolice/vo/help.wav
npc/metropolice/vo/hero.wav
npc/metropolice/vo/hesgone148.wav
npc/metropolice/vo/hesrunning.wav
npc/metropolice/vo/hesupthere.wav
npc/metropolice/vo/hidinglastseenatrange.wav
npc/metropolice/vo/highpriorityregion.wav
npc/metropolice/vo/holdingon10-14duty.wav
npc/metropolice/vo/holdit.wav
npc/metropolice/vo/holditrightthere.wav
npc/metropolice/vo/holdthisposition.wav
npc/metropolice/vo/Ihave10-30my10-20responding.wav
npc/metropolice/vo/industrialzone.wav
npc/metropolice/vo/infection.wav
npc/metropolice/vo/infestedzone.wav
npc/metropolice/vo/inject.wav
npc/metropolice/vo/innoculate.wav
npc/metropolice/vo/inposition.wav
npc/metropolice/vo/inpositionathardpoint.wav
npc/metropolice/vo/inpositiononeready.wav
npc/metropolice/vo/intercede.wav
npc/metropolice/vo/interlock.wav
npc/metropolice/vo/investigate.wav
npc/metropolice/vo/investigating10-103.wav
npc/metropolice/vo/is10-108.wav
npc/metropolice/vo/is415b.wav
npc/metropolice/vo/Isaidmovealong.wav
npc/metropolice/vo/isathardpointreadytoprosecute.wav
npc/metropolice/vo/isclosingonsuspect.wav
npc/metropolice/vo/isdown.wav
npc/metropolice/vo/isgo.wav
npc/metropolice/vo/ismovingin.wav
npc/metropolice/vo/isolate.wav
npc/metropolice/vo/ispassive.wav
npc/metropolice/vo/isreadytogo.wav
npc/metropolice/vo/issuingmalcompliantcitation.wav
npc/metropolice/vo/Ivegot408hereatlocation.wav
npc/metropolice/vo/jury.wav
npc/metropolice/vo/keepmoving.wav
npc/metropolice/vo/king.wav
npc/metropolice/vo/level3civilprivacyviolator.wav
npc/metropolice/vo/line.wav
npc/metropolice/vo/localcptreportstatus.wav
npc/metropolice/vo/location.wav
npc/metropolice/vo/lock.wav
npc/metropolice/vo/lockyourposition.wav
npc/metropolice/vo/lookingfortrouble.wav
npc/metropolice/vo/lookout.wav
npc/metropolice/vo/lookoutrogueviscerator.wav
npc/metropolice/vo/looseparasitics.wav
npc/metropolice/vo/loyaltycheckfailure.wav
npc/metropolice/vo/malcompliant10107my1020.wav
npc/metropolice/vo/malignant.wav
npc/metropolice/vo/matchonapblikeness.wav
npc/metropolice/vo/meters.wav
npc/metropolice/vo/minorhitscontinuing.wav
npc/metropolice/vo/move.wav
npc/metropolice/vo/movealong.wav
npc/metropolice/vo/movealong3.wav
npc/metropolice/vo/movebackrightnow.wav
npc/metropolice/vo/moveit.wav
npc/metropolice/vo/moveit2.wav
npc/metropolice/vo/movetoarrestpositions.wav
npc/metropolice/vo/movingtocover.wav
npc/metropolice/vo/movingtohardpoint.wav
npc/metropolice/vo/movingtohardpoint2.wav
npc/metropolice/vo/necrotics.wav
npc/metropolice/vo/needanyhelpwiththisone.wav
npc/metropolice/vo/nine.wav
npc/metropolice/vo/nineteen.wav
npc/metropolice/vo/ninety.wav
npc/metropolice/vo/nocontact.wav
npc/metropolice/vo/non-taggedviromeshere.wav
npc/metropolice/vo/noncitizen.wav
npc/metropolice/vo/nonpatrolregion.wav
npc/metropolice/vo/novisualonupi.wav
npc/metropolice/vo/nowgetoutofhere.wav
npc/metropolice/vo/off1.wav
npc/metropolice/vo/off2.wav
npc/metropolice/vo/off3.wav
npc/metropolice/vo/off4.wav
npc/metropolice/vo/officerdowncode3tomy10-20.wav
npc/metropolice/vo/officerdownIam10-99.wav
npc/metropolice/vo/officerneedsassistance.wav
npc/metropolice/vo/officerneedshelp.wav
npc/metropolice/vo/officerunderfiretakingcover.wav
npc/metropolice/vo/on1.wav
npc/metropolice/vo/on2.wav
npc/metropolice/vo/one.wav
npc/metropolice/vo/onehundred.wav
npc/metropolice/vo/outbreak.wav
npc/metropolice/vo/outlandbioticinhere.wav
npc/metropolice/vo/outlandzone.wav
npc/metropolice/vo/pacifying.wav
npc/metropolice/vo/patrol.wav
npc/metropolice/vo/pickingupnoncorplexindy.wav
npc/metropolice/vo/pickupthecan1.wav
npc/metropolice/vo/pickupthecan2.wav
npc/metropolice/vo/pickupthecan3.wav
npc/metropolice/vo/positiontocontain.wav
npc/metropolice/vo/possible10-103alerttagunits.wav
npc/metropolice/vo/possible404here.wav
npc/metropolice/vo/possible647erequestairwatch.wav
npc/metropolice/vo/possiblelevel3civilprivacyviolator.wav
npc/metropolice/vo/preparefor1015.wav
npc/metropolice/vo/prepareforjudgement.wav
npc/metropolice/vo/preparingtojudge10-107.wav
npc/metropolice/vo/preserve.wav
npc/metropolice/vo/pressure.wav
npc/metropolice/vo/priority2anticitizenhere.wav
npc/metropolice/vo/proceedtocheckpoints.wav
npc/metropolice/vo/productionblock.wav
npc/metropolice/vo/prosecute.wav
npc/metropolice/vo/protectioncomplete.wav
npc/metropolice/vo/ptatlocationreport.wav
npc/metropolice/vo/ptgoagain.wav
npc/metropolice/vo/putitinthetrash1.wav
npc/metropolice/vo/putitinthetrash2.wav
npc/metropolice/vo/quick.wav
npc/metropolice/vo/readytoamputate.wav
npc/metropolice/vo/readytojudge.wav
npc/metropolice/vo/readytoprosecute.wav
npc/metropolice/vo/readytoprosecutefinalwarning.wav
npc/metropolice/vo/reinforcementteamscode3.wav
npc/metropolice/vo/reportsightingsaccomplices.wav
npc/metropolice/vo/repurposedarea.wav
npc/metropolice/vo/requestsecondaryviscerator.wav
npc/metropolice/vo/residentialblock.wav
npc/metropolice/vo/responding2.wav
npc/metropolice/vo/restrict.wav
npc/metropolice/vo/restrictedblock.wav
npc/metropolice/vo/rodgerthat.wav
npc/metropolice/vo/roller.wav
npc/metropolice/vo/runninglowonverdicts.wav
npc/metropolice/vo/sacrificecode1maintaincp.wav
npc/metropolice/vo/search.wav
npc/metropolice/vo/searchingforsuspect.wav
npc/metropolice/vo/secondwarning.wav
npc/metropolice/vo/sector.wav
npc/metropolice/vo/sentencedelivered.wav
npc/metropolice/vo/serve.wav
npc/metropolice/vo/seven.wav
npc/metropolice/vo/seventeen.wav
npc/metropolice/vo/seventy.wav
npc/metropolice/vo/shit.wav
npc/metropolice/vo/shotsfiredhostilemalignants.wav
npc/metropolice/vo/six.wav
npc/metropolice/vo/sixteen.wav
npc/metropolice/vo/sixty.wav
npc/metropolice/vo/sociocide.wav
npc/metropolice/vo/stabilizationjurisdiction.wav
npc/metropolice/vo/standardloyaltycheck.wav
npc/metropolice/vo/stationblock.wav
npc/metropolice/vo/sterilize.wav
npc/metropolice/vo/stick.wav
npc/metropolice/vo/stillgetting647e.wav
npc/metropolice/vo/stormsystem.wav
npc/metropolice/vo/subject.wav
npc/metropolice/vo/subjectis505.wav
npc/metropolice/vo/subjectisnowhighspeed.wav
npc/metropolice/vo/supsecthasmovednowto.wav
npc/metropolice/vo/suspect11-6my1020is.wav
npc/metropolice/vo/suspectinstormrunoff.wav
npc/metropolice/vo/suspectisbleeding.wav
npc/metropolice/vo/suspectlocationunknown.wav
npc/metropolice/vo/suspectusingrestrictedcanals.wav
npc/metropolice/vo/suspend.wav
npc/metropolice/vo/sweepingforsuspect.wav
npc/metropolice/vo/tag10-91d.wav
npc/metropolice/vo/tagonebug.wav
npc/metropolice/vo/tagonenecrotic.wav
npc/metropolice/vo/tagoneparasitic.wav
npc/metropolice/vo/takecover.wav
npc/metropolice/vo/tap.wav
npc/metropolice/vo/teaminpositionadvance.wav
npc/metropolice/vo/ten.wav
npc/metropolice/vo/ten2.wav
npc/metropolice/vo/ten4.wav
npc/metropolice/vo/ten8standingby.wav
npc/metropolice/vo/ten91dcountis.wav
npc/metropolice/vo/ten97.wav
npc/metropolice/vo/ten97suspectisgoa.wav
npc/metropolice/vo/tenzerovisceratorishunting.wav
npc/metropolice/vo/terminalrestrictionzone.wav
npc/metropolice/vo/thatsagrenade.wav
npc/metropolice/vo/therehegoeshesat.wav
npc/metropolice/vo/thereheis.wav
npc/metropolice/vo/thirteen.wav
npc/metropolice/vo/thirty.wav
npc/metropolice/vo/thisisyoursecondwarning.wav
npc/metropolice/vo/three.wav
npc/metropolice/vo/threehundred.wav
npc/metropolice/vo/transitblock.wav
npc/metropolice/vo/twelve.wav
npc/metropolice/vo/twenty.wav
npc/metropolice/vo/two.wav
npc/metropolice/vo/twohundred.wav
npc/metropolice/vo/union.wav
npc/metropolice/vo/unitis10-65.wav
npc/metropolice/vo/unitis10-8standingby.wav
npc/metropolice/vo/unitisonduty10-8.wav
npc/metropolice/vo/unitreportinwith10-25suspect.wav
npc/metropolice/vo/unlawfulentry603.wav
npc/metropolice/vo/upi.wav
npc/metropolice/vo/utlsuspect.wav
npc/metropolice/vo/utlthatsuspect.wav
npc/metropolice/vo/vacatecitizen.wav
npc/metropolice/vo/vice.wav
npc/metropolice/vo/victor.wav
npc/metropolice/vo/visceratordeployed.wav
npc/metropolice/vo/visceratorisoc.wav
npc/metropolice/vo/visceratorisoffgrid.wav
npc/metropolice/vo/wasteriver.wav
npc/metropolice/vo/watchit.wav
npc/metropolice/vo/wearesociostablethislocation.wav
npc/metropolice/vo/wegotadbherecancel10-102.wav
npc/metropolice/vo/wehavea10-108.wav
npc/metropolice/vo/workforceintake.wav
npc/metropolice/vo/xray.wav
npc/metropolice/vo/yellow.wav
npc/metropolice/vo/youknockeditover.wav
npc/metropolice/vo/youwantamalcomplianceverdict.wav
npc/metropolice/vo/zero.wav
npc/metropolice/vo/zone.wav
npc/metropolice/vo/_comma.wav
]]--